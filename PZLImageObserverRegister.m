//
//  PZLImageObserverRegister.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLImageObserverRegister.h"

@interface PZLImageObserverRegister()

@property (strong,readwrite)NSMutableArray<id<PZLImageChangeObserver>>* observers;

@end

@implementation PZLImageObserverRegister
static PZLImageObserverRegister* uniqueInstance  = nil;

+ (instancetype)instance{
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        uniqueInstance = [PZLImageObserverRegister new];
    });
    
    return  uniqueInstance;
}

- (instancetype)init{
    if (uniqueInstance){
        return uniqueInstance;
    }
    self = [super init];
    if(self){
        self.observers = [NSMutableArray new];
    }
    uniqueInstance = self;
    return uniqueInstance;
    
}

- (BOOL)registerObserver:(id<PZLImageChangeObserver>)observer {
    if ([observer conformsToProtocol:@protocol(PZLImageChangeObserver)]){
        [self.observers addObject:observer];
        return YES;
    }
    return NO;
}

- (void)notifyObservers{
  
    for (id<PZLImageChangeObserver> observer in self.observers){
        [observer updateImageList];
    }
}
@end
