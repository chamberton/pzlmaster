//
//  PZLImageObserverRegister.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLImageChangeObserver.h"

@interface PZLImageObserverRegister : NSObject

+ (instancetype)instance;
- (BOOL)registerObserver:(id<PZLImageChangeObserver>)observer;
- (void)notifyObservers;

@end

