//
//  PZLImageFactory.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/21.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLImageFactory.h"

@implementation PZLImageFactory

#pragma  mark - PZLImageFactoryPortal

+ (UIImage *)imageWithName:(NSString *)name isPlayingMode:(BOOL)playing {
    
    if(!name || name.length<1){
        return nil;
    }
    NSString* nameToUse = name;
    
    if(playing){
        nameToUse = [name stringByAppendingString:@"_playing"];
    }
    
    UIImage* image = [PZLImageFactory imageWithBaseImageName:nameToUse];
    
    if(!image){
        nameToUse = name;
        image = [PZLImageFactory imageWithBaseImageName:nameToUse];
    }
    
    if(!image){
        image =[UIImage imageNamed:@"Missing.png"];
    }
    
    return image;
}

+ (UIImage *)imageWithBaseImageName:(NSString *)imageName {
    
    NSString* nameToUse = imageName;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    UIImage* image = [UIImage imageWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:nameToUse]];
    
    if(!image){
        image = [UIImage imageNamed:nameToUse];
    }
    if(!image){
        image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", nameToUse]];
    }
    if(!image){
        image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpeg", nameToUse]];
    }
    if(!image){
        image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", nameToUse]];
    }
    
    return image;
}

@end
