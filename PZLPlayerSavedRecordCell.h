//
//  PZLPlayerSavedRecordCell.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/14.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SavedGames+CoreDataClass.h"

@interface PZLPlayerSavedRecordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
