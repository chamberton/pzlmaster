//
//  PZLUtilities.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/21.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLUtilities.h"
#import "PZLGameLogic.h"

enum JPEGQuality {
    
    lowest  = 0,
    low     = 1,
    medium  = 2,
    high    = 3,
    highest = 4
};
@interface JPEGandCompressLevel : NSObject
@property (nonatomic,strong) UIImage* image;
@property  CGFloat compressionLevel;
@end

@implementation PZLUtilities

+(UIImage *)resized:(UIImage *)image {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth  = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height- 200;// self.collectionView.bounds.size.height);
    CGSize size = CGSizeMake(screenHeight, screenWidth);
    UIImage* resizedImage = [PZLUtilities resizeImage:image targetSize:size];
    return resizedImage;
}

+(NSMutableArray *)getImagesFromImage:(UIImage *)resizedImage withRow:(NSInteger)rows withColumn:(NSInteger)columns {
    
    NSMutableArray *images = [NSMutableArray array];
    CGSize imageSize = resizedImage.size;
    CGFloat xPos = 0.0, yPos = 0.0;
    CGFloat width = imageSize.width/columns;
    CGFloat height = imageSize.height/rows;
    
    for (int y = 0; y < rows; y++) {
        xPos = 0.0;
        for (int x = 0; x < columns; x++) {
            CGRect rect = CGRectMake(xPos, yPos, width, height);
            CGImageRef cImage = CGImageCreateWithImageInRect([resizedImage CGImage],  rect);
            UIImage *dImage = [[UIImage alloc] initWithCGImage:cImage];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x*width, y*height, width, height)];
            [imageView setImage:dImage];
            [imageView.layer setBorderColor:[[UIColor blackColor] CGColor]];
            [imageView.layer setBorderWidth:1.0];
            [images addObject:dImage];
            xPos += width;
        }
        yPos += height;
        
    }
    return images;
}


+(UIImage*) resizeImage : (UIImage*)image targetSize: (CGSize) targetSize{
    
    if(image==nil){
        return nil;
    }
    
    CGSize imageSize = image.size;
    float widthRatio  = targetSize.width  / imageSize.width;
    float heightRatio = targetSize.height / imageSize.height;
    
    // Figure out what our orientation is, and use that to form the rectangle
    CGSize  newSize = CGSizeMake(imageSize.width * widthRatio, imageSize.height * heightRatio);
   
    
    // Creat the rect object  where to draw the image
    CGRect rect = CGRectMake(0, 0, newSize.width, newSize.height);
    
    // Carry out the actual resizing
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0);
    [image drawInRect:rect];
    UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

@end
