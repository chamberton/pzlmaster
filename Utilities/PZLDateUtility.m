//
//  PZLDateUtility.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/22.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLDateUtility.h"

static NSString* const FullTimeStampString=@"yyyy-MMM-dd HH:mm:ss Z";
static NSString* const FullLocalTimeString=@"yyyy-MMM-dd HH:mm:ss";
static NSString* const CommonLocalTimeString=@"yyyy-MMM-dd HH:mm";

#define PZLFormatArray FullTimeStampString,FullLocalTimeString,CommonLocalTimeString,@"Unknown_Default",nil

@implementation PZLDateUtility
static NSMutableArray<NSDateFormatter *>* arrayOfFormats;

+ (void)initialize {
    NSArray* formats= [[NSArray alloc] initWithObjects:PZLFormatArray];
    arrayOfFormats = [NSMutableArray arrayWithCapacity:formats.count];
    for (NSString* string in formats){
        NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
        [dateformater setDateFormat:string];
        [arrayOfFormats addObject:dateformater];
    }
}

+(NSDate *)setTimeToCommonFormat:(NSDate *)datDate{
    if( datDate == nil ) {
       return [NSDate date];
    }

    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:datDate];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(Format)format{
    if( date == nil ) {
        date = [NSDate date];
    }
    NSDateFormatter *dateformater = [arrayOfFormats objectAtIndex:format];
    return [dateformater stringFromDate:date];
}

+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(Format)format{
    if( dateString == nil ) {
        return [NSDate date];
    }
    NSDateFormatter *dateformater = [arrayOfFormats objectAtIndex:format];
    return [dateformater dateFromString:dateString];
    
}
@end
