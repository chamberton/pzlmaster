//
//  PZLUtilities.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/21.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZLUtilities : NSObject
+(NSMutableArray *)getImagesFromImage:(UIImage *)image withRow:(NSInteger)rows withColumn:(NSInteger)columns;
+(UIImage*) resized:(UIImage*) image;
@end
