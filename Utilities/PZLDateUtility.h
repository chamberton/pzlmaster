//
//  PZLDateUtility.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/22.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _Format {
    FullTimeStamp,   // yyyy-MMM-dd HH:mm:ss +Z
    FullLocalTime,   // yyyy-MMM-dd HH:mm:ss
    CommonLocalTime, // yyyy-MMM-dd HH:mm
} Format;

@interface PZLDateUtility : NSObject

+ (NSString *) stringFromDate:(NSDate *)date
                   withFormat:(Format)format;

+ (NSDate *) dateFromString:(NSString *)dateString
                 withFormat:(Format)format;

+ (NSDate *)setTimeToCommonFormat:(NSDate *)datDate;
@end
