//
//  PZLBestScoreDetailsTableViewController.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/24.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BestScore+CoreDataClass.h"
#import "PZLTableViewController.h"

@interface PZLBestScoreDetailsTableViewController : PZLTableViewController

@property (nonatomic,readwrite) BestScore* bestScore;

@end
