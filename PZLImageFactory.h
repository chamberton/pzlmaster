//
//  PZLImageFactory.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/21.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZLImageFactory : NSObject

+ (UIImage *)imageWithName:(NSString *)name isPlayingMode:(BOOL)playing;

@end
