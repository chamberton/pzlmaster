//
//  SavedGames+CoreDataProperties.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "SavedGames+CoreDataProperties.h"

@implementation SavedGames (CoreDataProperties)

+ (NSFetchRequest<SavedGames *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SavedGames"];
}

@dynamic dateStarted;
@dynamic dateStopped;
@dynamic finished;
@dynamic gameboard;
@dynamic gameLevel;
@dynamic gamePicture;
@dynamic gameScore;
@dynamic hints;
@dynamic isImage;
@dynamic numbeOfPieces;
@dynamic numberOfMoves;
@dynamic rating;
@dynamic soundEnabled;
@dynamic time;
@dynamic timerEnabled;
@dynamic usedHints;
@dynamic savedFor;
@dynamic startingCompletedPercentage;

@end
