//
//  Images+CoreDataProperties.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "Images+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Images (CoreDataProperties)

+ (NSFetchRequest<Images *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *unlockingPoints;

@end

NS_ASSUME_NONNULL_END
