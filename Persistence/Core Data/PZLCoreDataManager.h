//
//  PZLCoreDataManager.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/07.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "PZLAppDelegate.h"
#import "SavedGames+CoreDataProperties.h"
#import "BestScore+CoreDataProperties.h"
#import "PlayerList+CoreDataClass.h"
#import "Images+CoreDataClass.h"



static BOOL stateChanged = YES;

@interface PZLCoreDataManager : NSObject

+ (NSMutableArray<Images *>*)getImages;
+ (BOOL)addNewPlayer:(PlayerList *) player;
+ (BOOL)addImageName:(NSString *) name enableErrorNotification:(BOOL) enable;
+ (NSMutableArray *)getPlayers;
+ (BOOL)imageWithDataExist:(NSData *)data;
+ (NSMutableArray *)getImagesNames;
+ (BOOL)deletePlayerWithName:(NSString *)name;
+ (BOOL)updateBestGameForPlayerWithName:(NSString *)name withBestGames:(SavedGames *)gamesToDelete;
+ (BOOL)addGame:(SavedGames *)game forPlayer:(PlayerList *)player;
+ (BOOL)addBestScore:(BestScore *)game forPlayer:(PlayerList*) player;
+ (BOOL)updatePlayerWithName:(NSString*) name withSavedGames : (NSSet*) savedgames withBestGames : (NSSet*) bestgames;
+ (BOOL)updatePlayerWithName:(NSString*) name withSavedGames : (NSSet<SavedGames*>*) savedgames;
+ (BOOL)updateCompletedPointsForPlayer:(NSString*) name pointsToAdd:(NSUInteger) points;

@end
