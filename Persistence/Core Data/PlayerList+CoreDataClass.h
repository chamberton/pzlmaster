//
//  PlayerList+CoreDataClass.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BestScore, SavedGames;

NS_ASSUME_NONNULL_BEGIN

@interface PlayerList : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PlayerList+CoreDataProperties.h"
