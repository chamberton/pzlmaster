//
//  Images+CoreDataClass.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Images : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Images+CoreDataProperties.h"
