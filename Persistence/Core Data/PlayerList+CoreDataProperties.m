//
//  PlayerList+CoreDataProperties.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PlayerList+CoreDataProperties.h"

@implementation PlayerList (CoreDataProperties)

+ (NSFetchRequest<PlayerList *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PlayerList"];
}

@dynamic completedGamesPoints;
@dynamic continent;
@dynamic dateCreated;
@dynamic name;
@dynamic rating;
@dynamic bestgames;
@dynamic savedgames;

@end
