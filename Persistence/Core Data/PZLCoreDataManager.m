//
//  PZLCoreDataManager.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/07.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLCoreDataManager.h"
#import "Images+CoreDataClass.h"
#import "iToast.h"

#import "PlayerList+CoreDataClass.h"
#import "BestScore+CoreDataProperties.h"
#import "SavedGames+CoreDataProperties.h"
#import "Images+CoreDataClass.h"
#import "SavedGames+CoreDataProperties.h"
#import "BestScore+CoreDataProperties.h"
#import "Images+CoreDataProperties.h"
#import "PZLBestScoreFactory.h"
#import "PZLImageFactory.h"
#import "PZLCoreDataCache.h"
#import "NSDictionary+KeyForValue.h"

@interface PlayerWithCreationDate : NSManagedObject

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSDate*   dateCreated;
@property (nonatomic)  float  rating;
@property (nonatomic, retain) NSString* continent;

@end

@implementation PlayerWithCreationDate

@synthesize rating = _rating;
@synthesize name = _name;
@synthesize dateCreated = _dateCreated;
@synthesize continent = _continent;

- (instancetype)init {
    self= [super init];
    
    if(self){
        self.rating=0;
        self.name=NULL;
        self.dateCreated=NULL;
        self.continent=NULL;
    }
    
    return self;
}


@end

@implementation PZLCoreDataManager

+ (void)initialize {
    [self deletePlayerImageWithName:@"Adolf Hitler"];
}

+ (NSMutableArray *)getPlayers {
    
    if(![PZLCoreDataCache sharedInstance].playerArray){
        [PZLCoreDataCache sharedInstance].playerArray = [PZLCoreDataManager retriveSavedPlayers];
    }
    
    return [PZLCoreDataCache sharedInstance].playerArray;
}

+ (NSMutableArray *)getImagesNames {
    
    if(![PZLCoreDataCache sharedInstance].imageArray){
        [PZLCoreDataCache sharedInstance].imageArray = [PZLCoreDataManager assignListOfImages];
    }
    
    return [PZLCoreDataCache sharedInstance].imageArray;
}


+ (BOOL)imageWithDataExist:(NSData *)data {
    
    for (NSString* binaryImageData in [PZLCoreDataCache sharedInstance].imageArrayBinary){
        if([binaryImageData isEqual:data]){
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)checkifPlayerWithNameExist:(NSString *)name {
    
    for (PlayerList* object in [PZLCoreDataCache sharedInstance].playerArray){
        if([name isEqualToString:object.name]){
            return YES;
        }
    }
    
    return NO;
}


+ (BOOL)addImageName:(NSString*) name enableErrorNotification:(BOOL)enable {
    
    if ([[PZLCoreDataCache sharedInstance].imageArray containsObject:name]){
        
        if(enable){
            NSString* toastMessage=[[NSString alloc]initWithFormat:@"%@",NSLocalizedString(@"ImageWithSameNameExist", @"ImageWithSameNameExist") ];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
            });
        }
        
        return NO;
    }
    
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    Images *newImage = (Images *)[NSEntityDescription insertNewObjectForEntityForName:@"Images"inManagedObjectContext:moc];
    newImage.name =name;
    newImage.location = name;
    NSError *error;
    
    if (![moc save:&error]) {
        NSLog(@"Error saving : %@", [error localizedDescription]);
    }else{
        [PZLCoreDataCache sharedInstance].imageArray = [PZLCoreDataManager assignListOfImages];
    }
    
    return YES;
}

+ (BOOL)addNewPlayer:(PlayerList *)player {
    if ([self checkifPlayerWithNameExist:player.name]){
        NSString* toastMessage=[[NSString alloc]initWithFormat:@"%@",NSLocalizedString(@"PlayerWithSameNameExist", @"PlayerWithSameNameExist") ];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
        });
        
        return NO;
    }
    
    if(player){
        NSError *error;
        PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *moc = ad.managedObjectContext;
        
        if (![moc save:&error]) {
            NSLog(@"Error saving : %@", [error localizedDescription]);
            return  YES;
        }else{
            [PZLCoreDataCache sharedInstance].playerArray = [PZLCoreDataManager retriveSavedPlayers];
        }
    }
    
    return YES;
}


+ (BOOL)addBestScore:(BestScore *)game forPlayer:(PlayerList *)player {
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    game.bestFor = player;
    NSError *error;
    
    if (![moc save:&error]) {
        NSLog(@"Error saving : %@", [error localizedDescription]);
        return  YES;
    }
    
    return NO;
}

+ (BOOL)addGame:(SavedGames *)game forPlayer:(PlayerList *)player {
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    game.savedFor = player;
    NSError *error;
    
    if (![moc save:&error]){
        NSLog(@"Error saving : %@", [error localizedDescription]);
        return  YES;
    }
    
    return NO;
}

+ (void)deletePlayerImageWithName:(NSString *)name {
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    NSFetchRequest * request= [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Images" inManagedObjectContext:moc]];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name = %@",name];
    [request setPredicate:predicate];
    NSError *error;
    BOOL failedDeletion  = NO;
    NSArray *entitiesArray = [moc executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"%@: Error fetching context: %@", [self class], [error localizedDescription]);
        NSLog(@"entitiesArray: %@",entitiesArray);
        failedDeletion=YES;
    }
    else{
        for(NSManagedObject *entity in entitiesArray) {
            [moc deleteObject:entity];
        }
        
        if (![moc save:&error]) {
            NSLog(@"Error saving : %@", [error localizedDescription]);
            failedDeletion=YES;
        }
    }
}


+ (BOOL)deletePlayerWithName:(NSString *)name{
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    if ([self checkifPlayerWithNameExist:name]){
        NSFetchRequest * request= [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PlayerList" inManagedObjectContext:moc]];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name = %@",name];
        [request setPredicate:predicate];
        NSError *error = nil;
        BOOL failedDeletion  = NO;
        NSArray *entitiesArray = [moc executeFetchRequest:request error:&error];
        
        if (error) {
            NSLog(@"%@: Error fetching context: %@", [self class], [error localizedDescription]);
            NSLog(@"entitiesArray: %@",entitiesArray);
            failedDeletion=YES;
        }
        else{
            for(NSManagedObject *entity in entitiesArray) {
                [moc deleteObject:entity];
            }
            
            if ( ![moc save:&error]) {
                NSLog(@"Error saving : %@", [error localizedDescription]);
                failedDeletion=YES;
            }
        }
        NSMutableString* toastMessage=NULL;
        if(failedDeletion){
            toastMessage= [NSMutableString stringWithFormat:@"%@",NSLocalizedString(@"FailedTodelete", @"PlayerDeleteSuccessfully") ];
        }else{
            
            stateChanged = YES;
            
            toastMessage= [NSMutableString stringWithFormat:@"%@",NSLocalizedString(@"PlayerDeleteSuccessfully", @"PlayerDeleteSuccessfully") ];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
            [PZLCoreDataCache sharedInstance].playerArray = [PZLCoreDataManager retriveSavedPlayers];
        });
        return !failedDeletion;
    }
    return NO;
    
}

+ (BOOL)updateBestGameForPlayerWithName:(NSString*)name withBestGames:(SavedGames *)gamesToDelete {
    
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    if([gamesToDelete.gameScore integerValue]==0){
        return NO;
    }
    if ([self checkifPlayerWithNameExist:name]){
        
        NSMutableArray* array = [NSMutableArray array];
        NSFetchRequest * request= [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PlayerList" inManagedObjectContext:moc]];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name = %@",name];
        [request setPredicate:predicate];
        NSError *error = nil;
        BOOL failedUpdate  = NO;
        NSArray *entitiesArray = [moc executeFetchRequest:request error:&error];
        
        if (error) {
            NSLog(@"%@: Error fetching context: %@", [self class], [error localizedDescription]);
            NSLog(@"entitiesArray: %@",entitiesArray);
            failedUpdate=YES;
        }
        else{
            for(PlayerList *entity in entitiesArray) {
                array = [NSMutableArray arrayWithArray:entity.bestgames.allObjects];
                BestScore* bestScore = [PZLBestScoreFactory buidBestGame:gamesToDelete];
                bestScore.playername = name;
                bestScore.bestFor = entity;
                [array addObject:bestScore];
                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"score" ascending:NO];
                array = [NSMutableArray arrayWithArray:[array sortedArrayUsingDescriptors:@[sort]]];
                while(array.count>10){
                    [array removeObjectAtIndex:array.count-1];
                }
                entity.bestgames = [NSSet setWithArray:array];
            }
            
            if ( ![moc save:&error]) {
                NSLog(@"Error saving : %@", [error localizedDescription]);
                failedUpdate=YES;
            }
        }
        return !failedUpdate;
    }
    return NO;
    
}

+ (BOOL)updatePlayerWithName:(NSString*) name withSavedGames : (NSSet<SavedGames *>*) gamesToDelete{
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    if ([self checkifPlayerWithNameExist:name]){
        
        NSMutableSet* set = [NSMutableSet set];
        NSFetchRequest * request= [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PlayerList" inManagedObjectContext:moc]];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name = %@",name];
        [request setPredicate:predicate];
        NSError *error = nil;
        BOOL failedUpdate  = NO;
        NSArray *entitiesArray = [moc executeFetchRequest:request error:&error];
        
        if (error) {
            NSLog(@"%@: Error fetching context: %@", [self class], [error localizedDescription]);
            NSLog(@"entitiesArray: %@",entitiesArray);
            failedUpdate=YES;
        }
        else{
            for(PlayerList *entity in entitiesArray) {
                set = [NSMutableSet setWithSet:entity.savedgames];
                [set minusSet:gamesToDelete];
                entity.savedgames = set;
            }
            
            if ( ![moc save:&error]) {
                NSLog(@"Error saving : %@", [error localizedDescription]);
                failedUpdate=YES;
            }
        }
        return !failedUpdate;
    }
    return NO;
    
}


+ (BOOL)updatePlayerWithName:(NSString *)name withSavedGames:(NSSet *)savedgames  withBestGames:(NSSet *)bestgames{
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    
    if ([self checkifPlayerWithNameExist:name]){
        NSFetchRequest * request= [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PlayerList" inManagedObjectContext:moc]];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name = %@",name];
        [request setPredicate:predicate];
        NSError *error = nil;
        BOOL failedUpdate  = NO;
        NSArray *entitiesArray = [moc executeFetchRequest:request error:&error];
        
        if (error) {
            NSLog(@"%@: Error fetching context: %@", [self class], [error localizedDescription]);
            NSLog(@"entitiesArray: %@",entitiesArray);
            failedUpdate=YES;
        }
        else{
            for(PlayerList *entity in entitiesArray) {
                if(entity.savedgames==nil)
                    entity.savedgames =[NSSet set];
                for (SavedGames* game in savedgames ){
                    [entity.savedgames setByAddingObject:game];
                }
            }
            for(PlayerList *entity in entitiesArray) {
                if(entity.bestgames==nil)
                    entity.bestgames =[NSSet set];
                
                for (BestScore* game in bestgames ){
                    [entity.bestgames setByAddingObject:game];
                }
            }
            
            if ( ![moc save:&error]) {
                // Something's gone seriously wrong
                NSLog(@"Error saving : %@", [error localizedDescription]);
                failedUpdate=YES;
            }
        }
        return !failedUpdate;
    }
    return NO;
    
}

#define REQUIRED_POINTS_TO_NEXT_LEVEL 5000

+ (BOOL)updateCompletedPointsForPlayer:(NSString *)name pointsToAdd:(NSUInteger)points {
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    
    if ([self checkifPlayerWithNameExist:name]){
        NSFetchRequest * request= [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"PlayerList" inManagedObjectContext:moc]];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name = %@",name];
        [request setPredicate:predicate];
        NSError *error = nil;
        BOOL failedUpdate  = NO;
        NSArray *entitiesArray = [moc executeFetchRequest:request error:&error];
        
        if (error) {
            NSLog(@"%@: Error fetching context: %@", [self class], [error localizedDescription]);
            NSLog(@"entitiesArray: %@",entitiesArray);
            failedUpdate=YES;
        }
        
        else{
            for(PlayerList *entity in entitiesArray) {
                NSNumber* newNumber = [NSNumber numberWithUnsignedInteger:points + [entity.completedGamesPoints unsignedIntegerValue]];
                entity.completedGamesPoints = newNumber;
                if(entity.completedGamesPoints.intValue>REQUIRED_POINTS_TO_NEXT_LEVEL){
                    unsigned long newRating = 1 + [entity.rating unsignedIntegerValue];
                    if (newRating>MAX_RATING){
                        newRating=MAX_RATING;
                    }
                    entity.rating = [NSNumber numberWithUnsignedInteger:newRating];
                    entity.completedGamesPoints = [NSNumber numberWithUnsignedInteger:0];
                }
            }
            
            if ( ![moc save:&error]) {
                failedUpdate=YES;
            }
        }
        return !failedUpdate;
    }
    return NO;
    
}

+ (NSMutableArray *)retriveSavedPlayers {
    
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PlayerList" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    NSError *error;
    NSMutableArray* players = [[moc executeFetchRequest:request error:&error] mutableCopy];
    
    if (!players) {
        NSLog(@"Failed to load colors from disk");
        return  NULL;
    }
    return players;
}

+ (NSMutableArray *)assignListOfImages {
   
    NSMutableArray* images = [self getImages];
    if (!images) {
        NSLog(@"Failed to load images list from disk");
        return  NULL;
    }
    NSMutableArray* names = [[NSMutableArray alloc] initWithCapacity:[images count]];
    for (Images* obj in images){
        [names addObject:obj.name];
    }
    
    return [self localizedSort:names];
}

+ (NSMutableArray<Images *> *)getImages {
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Images" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error;
    NSMutableArray* images = [[moc executeFetchRequest:request error:&error] mutableCopy];
    
    if (!images) {
        NSLog(@"Failed to load images list from disk");
        return  NULL;
    }
    
    return images;
}

+ (NSMutableArray*)localizedSort:(NSArray*)arrayOfImages{
    NSMutableDictionary<NSString*, NSString*>* dictionary = [NSMutableDictionary new];
  
    for (NSString* imageCoreDataObject in arrayOfImages){
        dictionary[imageCoreDataObject]=
        NSLocalizedStringFromTable(imageCoreDataObject, @"ImageNames", nil);
      
    }
    NSArray *sortedArrayOfLocalizedNames = [[dictionary allValues] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    NSMutableArray<Images*>* sortedImages= [NSMutableArray array];
    
    for (NSString* localizedName in sortedArrayOfLocalizedNames){
        [sortedImages addObject:[dictionary keyForValue:localizedName]];
    }
    return sortedImages;
    
}

@end
