//
//  SavedGames+CoreDataClass.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PlayerList;

NS_ASSUME_NONNULL_BEGIN

@interface SavedGames : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SavedGames+CoreDataProperties.h"
