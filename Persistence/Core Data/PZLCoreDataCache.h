//
//  PZLCoreDataCache.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZLCoreDataCache : NSObject

@property (nonatomic,readwrite,strong) NSMutableArray* playerArray;
@property (nonatomic,readwrite,strong) NSMutableArray* imageArray;
@property (nonatomic,readwrite,strong) NSMutableArray<NSData *>* imageArrayBinary;

+ (PZLCoreDataCache *)sharedInstance;

@end
