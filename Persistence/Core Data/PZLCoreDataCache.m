//
//  PZLCoreDataCache.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLImageFactory.h"
#import "PZLCoreDataCache.h"

static PZLCoreDataCache* sharedCache = nil;
static dispatch_queue_t binaryCreationQueue = nil;
@implementation PZLCoreDataCache

@synthesize imageArray = _imageArray;

+ (PZLCoreDataCache *)sharedInstance {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedCache = [[PZLCoreDataCache alloc] init];
    });
    return sharedCache;
}

- (instancetype)init{
    if(sharedCache){
        return sharedCache;
    }
    
    self = [super init];
    
    sharedCache = self;
    return self;
}

- (void)setImageArray:(NSMutableArray *)imageArray {
    _imageArray  = imageArray;
    if(!binaryCreationQueue){
        binaryCreationQueue = dispatch_queue_create( "createBinary", DISPATCH_QUEUE_SERIAL );
    }
    dispatch_async(binaryCreationQueue, ^{
        [self setBinaryArray];
    });
}

- (void)setBinaryArray {
    self.imageArrayBinary = [NSMutableArray arrayWithCapacity: self.imageArray.count];
    
    for (NSString* obj in  [PZLCoreDataCache sharedInstance].imageArray){
        UIImage* image = [PZLImageFactory imageWithName:obj isPlayingMode:NO];
        if(image){
            [[PZLCoreDataCache sharedInstance].imageArrayBinary addObject:UIImagePNGRepresentation(image)];
        }
    }
    
}

@end
