//
//  Images+CoreDataProperties.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "Images+CoreDataProperties.h"

@implementation Images (CoreDataProperties)

+ (NSFetchRequest<Images *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Images"];
}

@dynamic location;
@dynamic name;
@dynamic unlockingPoints;

@end
