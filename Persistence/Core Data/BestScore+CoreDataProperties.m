//
//  BestScore+CoreDataProperties.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "BestScore+CoreDataProperties.h"

@implementation BestScore (CoreDataProperties)

+ (NSFetchRequest<BestScore *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BestScore"];
}

@dynamic boardName;
@dynamic duration;
@dynamic level;
@dynamic numberOfTiles;
@dynamic numberOfMoves;
@dynamic playDate;
@dynamic playername;
@dynamic score;
@dynamic usedHints;
@dynamic bestFor;

@end
