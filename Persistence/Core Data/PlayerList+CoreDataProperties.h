//
//  PlayerList+CoreDataProperties.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PlayerList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PlayerList (CoreDataProperties)

+ (NSFetchRequest<PlayerList *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *completedGamesPoints;
@property (nullable, nonatomic, copy) NSNumber *continent;
@property (nullable, nonatomic, copy) NSDate *dateCreated;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *rating;
@property (nullable, nonatomic, retain) NSSet<BestScore *> *bestgames;
@property (nullable, nonatomic, retain) NSSet<SavedGames *> *savedgames;

@end

@interface PlayerList (CoreDataGeneratedAccessors)

- (void)addBestgamesObject:(BestScore *)value;
- (void)removeBestgamesObject:(BestScore *)value;
- (void)addBestgames:(NSSet<BestScore *> *)values;
- (void)removeBestgames:(NSSet<BestScore *> *)values;

- (void)addSavedgamesObject:(SavedGames *)value;
- (void)removeSavedgamesObject:(SavedGames *)value;
- (void)addSavedgames:(NSSet<SavedGames *> *)values;
- (void)removeSavedgames:(NSSet<SavedGames *> *)values;

@end

NS_ASSUME_NONNULL_END
