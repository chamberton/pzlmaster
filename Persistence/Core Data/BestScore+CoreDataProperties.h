//
//  BestScore+CoreDataProperties.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "BestScore+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BestScore (CoreDataProperties)

+ (NSFetchRequest<BestScore *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *boardName;
@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSNumber *level;
@property (nullable, nonatomic, copy) NSNumber *numberOfTiles;
@property (nullable, nonatomic, copy) NSNumber *numberOfMoves;
@property (nullable, nonatomic, copy) NSDate *playDate;
@property (nullable, nonatomic, copy) NSString *playername;
@property (nullable, nonatomic, copy) NSNumber *score;
@property (nullable, nonatomic, copy) NSNumber *usedHints;
@property (nullable, nonatomic, retain) PlayerList *bestFor;

@end

NS_ASSUME_NONNULL_END
