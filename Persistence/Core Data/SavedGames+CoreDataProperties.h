//
//  SavedGames+CoreDataProperties.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/12.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "SavedGames+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SavedGames (CoreDataProperties)

+ (NSFetchRequest<SavedGames *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateStarted;
@property (nullable, nonatomic, copy) NSDate *dateStopped;
@property (nullable, nonatomic, copy) NSNumber *finished;
@property (nullable, nonatomic, retain) NSData *gameboard;
@property (nullable, nonatomic, copy) NSNumber *gameLevel;
@property (nullable, nonatomic, copy) NSString *gamePicture;
@property (nullable, nonatomic, copy) NSNumber *gameScore;
@property (nullable, nonatomic, copy) NSNumber *startingCompletedPercentage;
@property (nullable, nonatomic, copy) NSNumber *hints;
@property (nullable, nonatomic, copy) NSNumber *isImage;
@property (nullable, nonatomic, copy) NSNumber *numbeOfPieces;
@property (nullable, nonatomic, copy) NSNumber *numberOfMoves;
@property (nullable, nonatomic, copy) NSNumber *rating;
@property (nullable, nonatomic, copy) NSNumber *soundEnabled;
@property (nullable, nonatomic, copy) NSNumber *time;
@property (nullable, nonatomic, copy) NSNumber *timerEnabled;
@property (nullable, nonatomic, copy) NSNumber *usedHints;
@property (nullable, nonatomic, retain) PlayerList *savedFor;

@end

NS_ASSUME_NONNULL_END
