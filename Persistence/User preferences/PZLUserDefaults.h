//
//  PZLUserDefaults.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/20.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZLUserDefaults : NSObject
+(void) storeInteger:(NSUInteger) integer
              forKey: (NSString*) key;

+(NSUInteger) getIntegerWithKey : (NSString*) key;


+(void) storeString:(NSString*) string
             forKey: (NSString*) key;

+(NSString*) stringForKey : (NSString*) key;

+(void) storeBool:(BOOL) boolean
           forKey: (NSString*) key;

+(BOOL) getBoolWithKey : (NSString*) key;
@end
