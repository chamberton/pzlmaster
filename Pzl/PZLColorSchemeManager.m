//
//  PZLColorSchemeManager.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/11.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLColorSchemeManager.h"

@implementation PZLColorSchemeManager

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
    green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
    blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
    alpha:1.0]

+ (void)load {
    MainTexst = [NSArray arrayWithObjects:defaultSchemeMainText,darkSchemeMainText,metallicSchemeMainText, nil];
    SubTexts = [NSArray arrayWithObjects:defaultSchemeSubText,darkSchemeSubText, metallicSchemeSubText, nil];
    Backgrounds = [NSArray arrayWithObjects: defaultSchemeBackground,darkSchemeBackground,metallicSchemeBackground, nil];
}

+ (void)initialize {
    defaultSchemeMainText = [UIColor colorWithRed:168.f/255 green:64.f/255 blue:0.f/255 alpha:1];
    defaultpickerColor = [UIColor colorWithRed:168.f/255 green:64.f/255 blue:0.f/255 alpha:1];
    defaultBorderColor = [UIColor blackColor];
    defaultSchemeSubText = [UIColor colorWithRed:168.f/255 green:64.f/255 blue:0.f/255 alpha:1];
}

+ (UIColor *)getBorderColor {
    return defaultBorderColor;
}

+ (UIColor *)getPickerColor {
    return defaultpickerColor;
}

+ (UIColor *)getMainTextColor {
    return UIColorFromRGB(0xFF0800);
}

+ (UIColor *)getMainSubtextColor {
    return defaultSchemeSubText;
}

+ (UIColor *)getMainBackgroundColor {
    return defaultSchemeBackground;
}

+ (UIColor *)getDateColor {
    return  [UIColor blackColor];
}

@end
