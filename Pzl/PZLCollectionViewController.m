//
//  PZLCollectionViewController.m
//  CollectionViewTutorial
//
//  Created by Bryan Hansen on 11/3/12.
//  Copyright (c) 2012 Bryan Hansen. All rights reserved.
//
#import "iToast.h"
#import "PZLCollectionViewController.h"
#import "PZLPhotoAlbumLayout.h"
#import "PZLAlbumPhotoCell.h"
#import "PZLAlbum.h"
#import "PZLPhoto.h"
#import "PZLAlbumTitleReusableView.h"
#import "PZLColorSchemeManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PZLUserDefaults.h"
#import "DraggableViewController.h"
#import "Images+CoreDataClass.h"
#import "TypesDefinition.h"
#import "PZLGameLogic.h"
#import "PZLImageFactory.h"
#import <objc/runtime.h>

static NSString * const PhotoCellIdentifier = @"PhotoCell";
static NSString * const AlbumTitleIdentifier = @"AlbumTitle";
static NSMutableArray* _arrayWithImagesGlobal;
static  NSMutableArray<Images*>* _RawImagesGlobal;
static  NSString *_documentsDirectoryGlobal;
static dispatch_queue_t _TasksQueue ;
static UIImage*  black;
static UIImage*  white;

@interface UIImage(AssociatedObject)
@property (nonatomic,strong) id locked;
@property (nonatomic,strong) id unlockingPoints;
@property (nonatomic,strong) id continent;
@end

@implementation  UIImage(AssociatedObject)

@dynamic locked;
@dynamic unlockingPoints;
const static char* kLockedKey = "locked";
const static char* kUnlockingPointKey = "unlockingPoints";
const static char* kContinent = "continent";
const static char* kMissingKey = "missing";

- (void) setLocked:(id)object {
    objc_setAssociatedObject(self, kLockedKey,object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id) locked {
    return objc_getAssociatedObject(self, kLockedKey);
}

- (void) setMissing:(id)object {
    objc_setAssociatedObject(self, kMissingKey,object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id) missing {
    return objc_getAssociatedObject(self, kMissingKey);
}

- (void) setUnlockingPoints:(id)object {
    objc_setAssociatedObject(self, kUnlockingPointKey,object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id) unlockingPoints {
    return objc_getAssociatedObject(self, kUnlockingPointKey);
}

- (void) setContinent:(id)object {
    objc_setAssociatedObject(self, kContinent,object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id) continent {
    return objc_getAssociatedObject(self, kContinent);
}

@end

@interface PZLCollectionViewController ()

@property (nonatomic, strong) NSMutableArray *albums;
@property (nonatomic, weak)   IBOutlet BHPhotoAlbumLayout *photoAlbumLayout;
@property (nonatomic, strong) NSMutableArray* arrayWithImages;
@property (nonatomic, strong) NSMutableArray<UIImage*>* RawImages;
@property (nonatomic, strong) NSMutableArray* arrayOfSelectedWithImages;
@property (nonatomic, strong) NSString *documentsDirectory;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progessIndicator;

@property (nonatomic) BOOL errorSaving;

@end

@implementation PZLCollectionViewController

#pragma mark - Lifecycle

+ (void)load {
    black= [UIImage imageNamed:@"check_black"];
    white= [UIImage imageNamed:@"check_white"];
    _TasksQueue = dispatch_queue_create("updatingImageQueue", NULL);
    _documentsDirectoryGlobal = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    _arrayWithImagesGlobal =[NSMutableArray arrayWithCapacity:0];
    _RawImagesGlobal =[NSMutableArray arrayWithCapacity:0];
    
}

- (void)set {
    
    if(stateChanged){
        _arrayWithImagesGlobal=[NSMutableArray arrayWithArray:[PZLCoreDataManager getImages]];
        int index  =0,currentIndex=0;
    check:
        index=0;
        

        for (Images* imageCoreDataObject in _arrayWithImagesGlobal){
            NSString* obj = imageCoreDataObject.name;
            if(index<currentIndex){
                index++;
                continue;
            }
            index++;
            currentIndex++;
            
            UIImage* image = [PZLImageFactory imageWithName:obj isPlayingMode:NO];;
            
            if(image)
                [_RawImagesGlobal addObject:imageCoreDataObject];
            else{
                currentIndex--;
                [_arrayWithImagesGlobal removeObject:imageCoreDataObject];
                goto  check;
            }
            
        }
        stateChanged = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem.enabled  = NO;
    black= [UIImage imageNamed:@"check_black"];
    white= [UIImage imageNamed:@"check_white"];
    _TasksQueue = dispatch_queue_create("updatingImageQueue", NULL);
    _documentsDirectoryGlobal = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    _arrayOfSelectedWithImages = [NSMutableArray arrayWithCapacity:0];
    UIImage *patternImage = [UIImage imageNamed:@"WallPaper"];
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:patternImage];
    
    [self.collectionView registerClass:[BHAlbumPhotoCell class]
            forCellWithReuseIdentifier:PhotoCellIdentifier];
    [self.collectionView registerClass:[BHAlbumTitleReusableView class]
            forSupplementaryViewOfKind:BHPhotoAlbumLayoutAlbumTitleKind
                   withReuseIdentifier:AlbumTitleIdentifier];
    
    self.progessIndicator =   [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.progessIndicator.alpha = 1.0;
    self.progessIndicator.hidesWhenStopped = NO;
    self.progessIndicator.frame =CGRectMake(0,0,50,50);
    self.progessIndicator.center= self.view.center;
    self.progessIndicator.transform = CGAffineTransformMakeScale(2,2);
    self.progessIndicator.layer.cornerRadius = 15;
    self.progessIndicator.layer.borderWidth = 2;
    self.progessIndicator.layer.borderColor = [UIColor brownColor].CGColor;
    [self.view addSubview:self.progessIndicator];
    [self.progessIndicator startAnimating];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self set];
        [self buildUIImages];
        [self.progessIndicator stopAnimating];
        self.progessIndicator.hidden = TRUE;
        [self.collectionView reloadData];
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - View Rotation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
            self.photoAlbumLayout.numberOfColumns = 5;
        }else{
            self.photoAlbumLayout.numberOfColumns = 3;
        }
        CGFloat sideInset = [UIScreen mainScreen].preferredMode.size.width == 1136.0f ?
        45.0f : 25.0f;
        
        self.photoAlbumLayout.itemInsets = UIEdgeInsetsMake(22.0f, sideInset, 13.0f, sideInset);
        
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
            self.photoAlbumLayout.numberOfColumns = 4;
        }else{
            self.photoAlbumLayout.numberOfColumns = 2;
        }
        self.photoAlbumLayout.itemInsets = UIEdgeInsetsMake(22.0f, 22.0f, 13.0f, 22.0f);
    }
    
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if( !_mutipleSelectionAllowed)
    {
        [self markSetLockedItems];
    }
    return _RawImages.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return NUMBER_OF_ITEMS_IN_SECTION;
}


- (void) markSetLockedItems {
    PlayerList *player =[PZLGameLogic getPlayer];
    NSInteger continent;
    NSInteger rating  = [player.rating integerValue];
    NSInteger imageContinent;
    NSInteger unlockingPoints;
    
    for (int i=0; i<_RawImages.count; i++){
        NSInteger sum ;
        imageContinent = [_RawImages[i].continent integerValue];
        _RawImages[i].locked=[NSNumber numberWithBool:NO];
        continent = [player.continent integerValue];
        unlockingPoints = [_RawImages[i].unlockingPoints integerValue];
        
        if (imageContinent!=0 || unlockingPoints!=0){
            if (continent<imageContinent){
                sum = continent + rating;
            }else{
                continent -=[PZLGameLogic numberOfContinentItems];
                sum = continent + rating;
            }
            _RawImages[i].locked = [NSNumber numberWithBool:sum<(imageContinent-1)];
        }else{
            BOOL unlocked =(rating*10)>unlockingPoints || unlockingPoints==0;
            if (rating==MAX_RATING){
                unlocked=YES;
            }
            _RawImages[i].locked = [NSNumber numberWithBool:!unlocked];
        }
    }
}

- (IBAction)goToGame:(id)sender {
    if(_arrayOfSelectedWithImages.count<1){
        NSString* toastMessage=[[NSString alloc]initWithFormat:@"%@",NSLocalizedString(@"YouMustSelectAPicture", @"YouMustSelectAPicture") ];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
        });
        
        return;
    }
    NSString* gamePicture =[_arrayWithImages objectAtIndex:[[_arrayOfSelectedWithImages firstObject] integerValue] ];
    NSNumber* hints = [NSNumber numberWithInteger:[PZLUserDefaults getIntegerWithKey:CURRENT_HINTS]];
    NSNumber* level = [NSNumber numberWithInteger:[PZLUserDefaults getIntegerWithKey:CURRENT_LEVEL]];
    NSNumber* pieces =[NSNumber numberWithInteger:[PZLUserDefaults getIntegerWithKey:CURRENT_BOARD]];
    
    BOOL soundEnabled = [PZLUserDefaults getBoolWithKey:SOUNDS_ENABLED];
    BOOL isImage = !([gamePicture isEqualToString:@"letters.jpg"]  ||  [gamePicture isEqualToString:@"numbers.jpg"] );
    BOOL timerON = [PZLUserDefaults getBoolWithKey:TIMER_ENABLED];
    
    if([DraggableViewController setFromNull:isImage forlevel:level withPicture:gamePicture  numberOfHints:hints  numberOfPieces:pieces.intValue enableTmer:timerON enableSound:soundEnabled]){
        [self performSegueWithIdentifier:@"startGame" sender:sender];
    }
}

- (IBAction)savePicture:(id)sender {
    if (!_mutipleSelectionAllowed)
        return;
    const NSUInteger count  = [_arrayOfSelectedWithImages count];
    
    NSString* title = [NSString stringWithFormat:@"%@ %lu %@",NSLocalizedString(@"Save", @"Save"),(unsigned long)count,NSLocalizedString(@"selected images", @"selected images")];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",@"Ok") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                         {
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             dispatch_async(_TasksQueue, ^{
                                 ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                                 
                                 for (int i=0; i< [_arrayOfSelectedWithImages count]; i++){
                                     NSNumber *a= (NSNumber*)[_arrayOfSelectedWithImages objectAtIndex:(NSInteger)i];
                                     UIImage* image = (UIImage*)[_RawImages  objectAtIndex: [a integerValue]];
                                     
                                     [library writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
                                         if (!error) {
                                             _errorSaving = NO;
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 NSString* toastMessage=[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"ImageSavedToPhoto", @"ImageSavedToPhoto"),i];
                                                 [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:0.f] show:iToastTypeInfo];
                                                 [self.collectionView reloadData];
                                                 
                                                 
                                             });
                                         } else {
                                             _errorSaving = YES;
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 NSString* toastMessage=[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"CouldNoSaveImage", @"CouldNoSaveImage"),i ];
                                                 [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
                                                 [self.collectionView reloadData];
                                             });
                                             
                                             
                                         }
                                     }];
                                     
                                 }
                             });
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BHAlbumPhotoCell* ownerCell =(BHAlbumPhotoCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    if([ownerCell.imageView.image.locked boolValue] || [ownerCell.imageView.image.missing boolValue]){
        return;
    }
    if (!_mutipleSelectionAllowed)
    {
        if(_arrayOfSelectedWithImages.count>0){
            NSNumber* oldSection  = [_arrayOfSelectedWithImages firstObject];
            NSIndexPath* path  =[NSIndexPath indexPathForRow:0 inSection:[oldSection integerValue]] ;
            BHAlbumPhotoCell* oldSelectedCell =(BHAlbumPhotoCell*)[self.collectionView cellForItemAtIndexPath:path];
            oldSelectedCell.checkView.image = white;
            oldSelectedCell.backgroundColor = [ UIColor clearColor];
            oldSelectedCell.layer.shadowColor = [UIColor clearColor].CGColor;
            [self changeSelectState:oldSelectedCell];
            [_arrayOfSelectedWithImages removeAllObjects];
            
            if ([path isEqual:indexPath]){
                [self activateRightButton:NO];
                return;
            }
        }
    }
    if(ownerCell.selectedForDeletion){
        [_arrayOfSelectedWithImages removeObject:[NSNumber numberWithInt:(int)indexPath.section]];
        
    }else{
        if (![_arrayOfSelectedWithImages containsObject: [NSNumber numberWithInt:(int)indexPath.section]]){
            [_arrayOfSelectedWithImages addObject: [NSNumber numberWithInt:(int)indexPath.section]];
        }
    }
    
    [self changeSelectState:ownerCell];
    [self activateRightButton:_arrayOfSelectedWithImages.count>0];
}

-(void)activateRightButton:(BOOL)selectionExist {
    self.navigationItem.rightBarButtonItem.enabled = selectionExist;
}

- (void)changeSelectState :(BHAlbumPhotoCell*) ownerCell {
    
    if(ownerCell.selectedForDeletion){
        ownerCell.checkView.image = white;
        ownerCell.selectedForDeletion=NO;
        ownerCell.backgroundColor = [ UIColor clearColor];
        ownerCell.layer.shadowColor = [UIColor clearColor].CGColor;
        ownerCell.layer.shadowOpacity = 0.0f;
        
    }else{
        ownerCell.checkView.image = black;
        ownerCell.backgroundColor = [ UIColor blackColor];
        ownerCell.layer.shadowColor = [UIColor blackColor].CGColor;
        ownerCell.layer.shadowRadius = 3.0f;
        ownerCell.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        ownerCell.layer.shadowOpacity = 1.0f;
        ownerCell.selectedForDeletion=YES;
    }
}

extern BOOL shouldAppear;

- (NSURL*)applicationDataDirectory {
    NSFileManager* sharedFM = [NSFileManager defaultManager];
    NSArray* possibleURLs = [sharedFM URLsForDirectory:NSApplicationSupportDirectory
                                             inDomains:NSUserDomainMask];
    NSURL* appSupportDir = nil;
    NSURL* appDirectory = nil;
    
    if ([possibleURLs count] >= 1) {
        appSupportDir = [possibleURLs objectAtIndex:0];
    }
    
    if (appSupportDir) {
        NSString* appBundleID = [[NSBundle mainBundle] bundleIdentifier];
        appDirectory = [appSupportDir URLByAppendingPathComponent:appBundleID];
    }
    
    return appDirectory;
}



- (void)buildUIImages {
    [self dismissViewControllerAnimated:YES completion:nil];
    _documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    _arrayWithImages=[NSMutableArray arrayWithArray: [PZLCoreDataManager getImagesNames]];
    _RawImages =[NSMutableArray arrayWithCapacity:0];
    int index  =0,currentIndex=0;
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Images" withExtension:@"plist"];
    NSDictionary *arrayOfImagesDescription = [[NSDictionary alloc ] initWithContentsOfURL:url];
    
check:
    index=0;
    for (NSString* obj in _arrayWithImages){
        if (index<currentIndex){
            index++;
            continue;
        }
        index++;
        currentIndex++;
        UIImage* image = [PZLImageFactory imageWithName:obj isPlayingMode:NO];
        
        if(image){
            NSDictionary* imageInfo = arrayOfImagesDescription[obj];
            if (!imageInfo){
                image.unlockingPoints = [NSNumber numberWithInteger:0];
                image.continent = [NSNumber numberWithInteger:0];
            }else{
                image.unlockingPoints = imageInfo[@"UnlockingPoints"];
                image.continent = imageInfo[@"Continent"];
            }
            [_RawImages addObject:image];
        }else{
            currentIndex--;
            [_arrayWithImages removeObject:obj];
            goto check;
        }
    }
    stateChanged=NO;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BHAlbumPhotoCell *photoCell =[collectionView dequeueReusableCellWithReuseIdentifier:PhotoCellIdentifier forIndexPath:indexPath];
    
    if(indexPath.row==0){
        if([_arrayWithImages count]!=[_RawImages count]){
            [self buildUIImages];
        }
        UIImage* image = (UIImage*)[_RawImages objectAtIndex:indexPath.section];
        if([image.locked boolValue]){
            photoCell.imageView.image =[UIImage imageNamed:@"Locked"];
            photoCell.imageView.image.locked=[NSNumber numberWithBool:YES];
        }else{
            photoCell.imageView.image = image;
        }
        
        
        if (![_arrayOfSelectedWithImages containsObject:[NSNumber numberWithInt:(int)indexPath.section]]){
            if ([image.locked boolValue] || [image.missing boolValue]){
                photoCell.checkView.image = nil;
            }else{
                photoCell.checkView.image = white;
            }
            photoCell.selected=NO;
            photoCell.backgroundColor = [ UIColor clearColor];
            photoCell.layer.shadowColor = [UIColor clearColor].CGColor;
        }else{
            if ([image.locked boolValue] || [image.missing boolValue]){
                photoCell.checkView.image = nil;
            }else{
                photoCell.checkView.image = black;
            }
            photoCell.backgroundColor = [ UIColor blackColor];
            photoCell.layer.shadowColor = [UIColor blackColor].CGColor;
            photoCell.layer.shadowRadius = 3.0f;
            photoCell.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
            photoCell.layer.shadowOpacity = 1.0f;
            photoCell.selected=YES;
        }
    }
    
    return photoCell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    BHAlbumTitleReusableView *titleView =
    [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                       withReuseIdentifier:AlbumTitleIdentifier
                                              forIndexPath:indexPath];
    
    
    if([titleView.titleLabel.text isEqualToString:@"letters.jpg"]){
        titleView.titleLabel.text = @"Letters";
    }
    else if([titleView.titleLabel.text isEqualToString:@"numbers.jpg"]){
        titleView.titleLabel.text = @"Numbers";
    }else{
        titleView.titleLabel.text = NSLocalizedStringFromTable([_arrayWithImages objectAtIndex:indexPath.section], @"ImageNames", nil);
    }
    titleView.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleView.titleLabel.font = [UIFont fontWithName:@"Noteworthy-Bold" size:16];
    [ titleView.titleLabel setTextColor:[PZLColorSchemeManager getMainTextColor]];
    
    return titleView;
}

- (void)dealloc {
    [self.view willRemoveSubview:self.progessIndicator];
    self.progessIndicator = nil;
}
@end
