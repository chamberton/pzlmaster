//
//  PlayersViewController.m
//  
//
//  Created by Serge Mbamba on 2016/06/27.
//
//
#import "PZLPlayerDetailsViewController.h"
#import "PZLPlayersViewController.h"
#import "PZLPlayerRecordColletionViewController.h"
#import "PZLColorSchemeManager.h"
#import "SavedGames+CoreDataProperties.h"
#import "PZLCoreDataManager.h"

static int _lastindex = 0;

@implementation PlayersViewController{
     PlayerList* _currentPlayer;
}

@synthesize players;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.allowsSelection = NO;
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];
    players =[PZLCoreDataManager getPlayers];
    self.tableView.separatorColor = [UIColor clearColor];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshPlayer:)
                                                 name:@"refreshPlayerList"
                                               object:nil];
}

- (void)refreshPlayer:(NSNotification *)note {
    
    if([note.name isEqualToString:@"refreshPlayerList"]){
        players = [PZLCoreDataManager getPlayers];
        _currentPlayer =(self.players)[_lastindex];
        
         dispatch_async(dispatch_get_main_queue(), ^{
             [self.tableView reloadData];
             [[NSNotificationCenter defaultCenter]
              postNotificationName:@"refreshPlayerDetails"
              object:self];
         });
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    self.players = [NSMutableArray arrayWithArray:[self.players sortedArrayUsingDescriptors:@[sort]]];
    return [self.players count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((self.players)[indexPath.row]) {
        _currentPlayer =(self.players)[indexPath.row];
        _lastindex = (int)indexPath.row;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell"];
    cell.backgroundColor = [UIColor clearColor];
    PlayerList *player = (PlayerList*)(self.players)[indexPath.row];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:100];
    nameLabel.text = player.name;
    [nameLabel setTextColor:[PZLColorSchemeManager getMainTextColor]];
    
    UILabel *gameLabel = (UILabel *)[cell viewWithTag:101];
    gameLabel.text = [NSArray arrayWithObjects:ContinentNameArray][[player.continent unsignedIntegerValue]];

    UIImageView *ratingImageView = (UIImageView *)[cell viewWithTag:102];
    [ratingImageView setImage:[self imageForRating:[player.rating intValue]>5?5:[player.rating intValue]]];
    UIImage * image = [ UIImage imageNamed:@"arrow-disclosure" ] ;
    UIControl * c = [ [ UIControl alloc ] initWithFrame:(CGRect){ CGPointMake(ratingImageView.frame.origin.x+ratingImageView.frame.size.width, ratingImageView.frame.origin.y), image.size } ] ;
    
    c.layer.contents = (id)image.CGImage ;
    c.tag = indexPath.row;
    cell.tag = indexPath.row;
    [c addTarget:self action:@selector(accessoryButtonTapped:event:)  forControlEvents:UIControlEventTouchUpInside];
    cell.accessoryView = c ;
   
    return cell;
}

- (void)accessoryButtonTapped:(id)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
    
    if (indexPath != nil){
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [self performSegueWithIdentifier:@"PlayerRecord" sender:cell];
    }
}

- (UIImage *)imageForRating:(int)rating {
    
    switch (rating) {
        case 1: return [UIImage imageNamed:@"1StarSmall"];
        case 2: return [UIImage imageNamed:@"2StarsSmall"];
        case 3: return [UIImage imageNamed:@"3StarsSmall"];
        case 4: return [UIImage imageNamed:@"4StarsSmall"];
        case 5: return [UIImage imageNamed:@"5StarsSmall"];
    }
    return nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"AddPlayer"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        PlayerDetailsViewController *playerDetailsViewController = [navigationController viewControllers][0];
        playerDetailsViewController.delegate = self;
    }else if ([segue.identifier isEqualToString:@"PlayerRecord"]) {
        UITableViewCell *cell = (UITableViewCell*) sender;
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:100];
        
        int currentIndex = 0;
        for( PlayerList *player in self.players){
            
            if([player.name isEqualToString:nameLabel.text]){
                break;
            }
            currentIndex++;
        }
        
        PlayerList *player = (self.players)[currentIndex];
        [PZLPlayerRecordColletionViewController setSavedGames:player.savedgames];
        [PZLPlayerRecordColletionViewController setBestGames:player.bestgames];
        [PZLPlayerRecordColletionViewController setCurrentPlayer:player];
        [PZLPlayerRecordColletionViewController setPlayerIndex:currentIndex];
        [segue.destinationViewController setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Records",@"Records"),nameLabel.text]];
    }
}

#pragma mark - PlayerDetailsViewControllerDelegate

- (void)playerDetailsViewControllerDidCancel:(id)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)playerDetailsViewController:(id)controller didAddPlayer:(id)player {
    
    if([PZLCoreDataManager addNewPlayer:player]){
        [self.players addObject:player];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.players count] - 1) inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.tableView reloadData];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle: NSLocalizedString(@"Deletion Confirmation", @"Deletion Confirmation")
                                      message:[NSString stringWithFormat:@"%@ %@?", NSLocalizedString(@"Permanently delete",@"Permanently delete"),((PlayerList*)self.players[indexPath.row]).name]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Ok",@"Ok")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if([PZLCoreDataManager deletePlayerWithName:((PlayerList*)self.players[indexPath.row]).name]){
                                     [self.players removeObjectAtIndex:(NSUInteger)indexPath.row];
                                     [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                 }
                                 [alert dismissViewControllerAnimated:YES completion:nil];

                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [self.tableView reloadData];
                                     
                                 }];
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
