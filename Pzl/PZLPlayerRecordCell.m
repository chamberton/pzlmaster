//
//  PZLPlayerRecordCell.m
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import "PZLPlayerRecordCell.h"

@implementation PZLPlayerRecordCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.layer.shadowOpacity = 0.5f;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.layer.shouldRasterize = YES;
        self.backgroundColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        _label =[[UILabel alloc] initWithFrame:self.bounds];
        [self.contentView addSubview:_imageView];
        [self.contentView addSubview:_label];
        
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _imageView.image = nil;
    _label.text=nil;
    _label.backgroundColor = [UIColor clearColor];
     self.backgroundColor  = [UIColor clearColor];
}

@end
