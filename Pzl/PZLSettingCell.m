//
//  SettingCell.m
//  
//
//  Created by Serge Mbamba on 2016/06/30.
//
//

#import "PZLSettingCell.h"

@implementation PZLSettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
