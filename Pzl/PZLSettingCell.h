//
//  SettingCell.h
//  
//
//  Created by Serge Mbamba on 2016/06/30.
//
//

#import <UIKit/UIKit.h>

@interface PZLSettingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *Label;
@property (weak, nonatomic) IBOutlet UIImageView *Icon;
@property (weak, nonatomic) IBOutlet UIPickerView *Picker;
@property (weak, nonatomic) IBOutlet UISwitch *Switch;

@end
