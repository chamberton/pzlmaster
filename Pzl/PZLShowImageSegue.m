//
//  PZLShowImageSegue.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/22.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLShowImageSegue.h"

@implementation PZLShowImageSegue

- (void)perform {
    UIViewController * sourceViewController  =  self.sourceViewController;
    UIViewController* destinationViewController  =  self.destinationViewController;
    [sourceViewController.view addSubview:destinationViewController.view];
    destinationViewController.view.frame = sourceViewController.view.window.frame;
    destinationViewController.view.transform = CGAffineTransformMakeTranslation(0, -sourceViewController.view.frame.size.height);
    
    [UIView animateWithDuration:0.75 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
         destinationViewController.view.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        [sourceViewController addChildViewController: destinationViewController];
    }];
}

@end
