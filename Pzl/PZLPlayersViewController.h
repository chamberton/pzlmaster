//
//  PlayersViewController.h
//  
//
//  Created by Serge Mbamba on 2016/06/27.
//
//


#import <UIKit/UIKit.h>
#import "PZLPlayerDetailsViewController.h"
#import "GamePickerViewControllerDelegate.h"

@interface PlayersViewController : UITableViewController <PlayerDetailsViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *players;

@end
