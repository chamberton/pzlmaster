//
//  ShowPictureInGameVC.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/22.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "ShowPictureInGameVC.h"
#import "SoundManager.h"

@implementation ShowPictureInGameVC{
    NSString* soundFile;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.soundEnabled){
        soundFile = @"showImage.wav";
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SoundManager sharedManager] playMusic:soundFile looping:YES];
        });
    }
    
    _imageView.image = _imageToShow;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(dismiss:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    [_imageView addGestureRecognizer:tapGesture1];
    

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)dismiss:(id)sender {
    [[SoundManager sharedManager] stopMusic];
    [self.view removeFromSuperview];
}

@end
