//
//  PZLplayerRecordColletionView.h
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#ifndef _PZLplayerRecordColletionView_h_
#define _PZLplayerRecordColletionView_h_


#import <UIKit/UIKit.h>
#import "PZLPlayerRecordLayoutManager.h"
#import "SavedGames+CoreDataProperties.h"
#import "BestScore+CoreDataProperties.h"

@interface PZLPlayerRecordColletionViewController : UICollectionViewController <UICollectionViewDataSource,UICollectionViewDelegate, UIGestureRecognizerDelegate,UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet PZLPlayerRecordLayoutManager *layoutManager;

+(void) setSavedGames: (NSSet*) games;
+(void) setBestGames : (NSSet*) games;
+(void) setCurrentPlayer: (id) player;
+(void) saveGame : (id)game;
+(void) saveBestScore : (id)game;
+(void) setPlayerIndex:(int) index;
+(void) update;

@end

#endif
