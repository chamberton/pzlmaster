//
//  PZLSettingsViewController.h
//  
//
//  Created by Serge Mbamba on 2016/06/30.
//
//

#import <UIKit/UIKit.h>
#import "PZLGameLogic.h"
#import "PZLTableViewController.h"

@interface PZLSettingsViewController : PZLTableViewController<UITableViewDelegate, UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>

@end
