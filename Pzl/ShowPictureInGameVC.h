//
//  ShowPictureInGameVC.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/22.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowPictureInGameVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImage *imageToShow;
@property (readwrite) BOOL soundEnabled;

@end
