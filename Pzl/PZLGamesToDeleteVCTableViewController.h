//
//  PZLGamesToDeleteVCTableViewController.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/24.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SavedGames+CoreDataProperties.h"
#import "PZLTableViewController.h"

@interface PZLGamesToDeleteVCTableViewController : PZLTableViewController

@property (nonatomic,strong,readwrite) NSMutableArray<SavedGames *> * games;
@property (nonatomic,strong,readwrite) NSString *player;

@end
