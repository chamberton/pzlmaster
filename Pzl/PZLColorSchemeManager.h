//
//  PZLColorSchemeManager.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/11.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// Sheme 1
static UIColor *defaultSchemeMainText;
static UIColor *defaultSchemeSubText;
static UIColor *defaultSchemeBackground;
static UIColor *defaultpickerColor;
static UIColor *defaultBorderColor;

// Scheme 2

static UIColor *darkSchemeMainText;
static UIColor *darkSchemeSubText;
static UIColor *darkSchemeBackground;

// Scheme 3
static UIColor *metallicSchemeMainText;
static UIColor *metallicSchemeSubText;
static UIColor *metallicSchemeBackground;


static NSArray *MainTexst;
static NSArray *SubTexts;
static NSArray *Backgrounds;

static UIColor *currentSchemeMainText;
static UIColor *currentSchemeSubText;
static UIColor *currentSchemeBackground;

@interface PZLColorSchemeManager : NSObject


+(UIColor *)getBorderColor;
+(UIColor *)getDateColor;
+(UIColor *)getPickerColor;
+(UIColor *)getMainTextColor;
+(UIColor *)getMainSubtextColor;
+(UIColor *)getMainBackgroundColor;

@end
