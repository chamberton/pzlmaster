//
//  PZLPlayerRecordCell.h
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//
#ifndef _PZLPlayerRecordCell_h_
#define _PZLPlayerRecordCell_h_

#import <UIKit/UIKit.h>
#import "PZLColorSchemeManager.h"

static NSString * const PZLPlayerCellIdentifier = @"PZLPlayerRecordCell";

@interface PZLPlayerRecordCell : UICollectionViewCell
@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *label;

@end

#endif
