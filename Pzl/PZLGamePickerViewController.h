//
//  GamePickerViewController.h
//  
//
//  Created by Serge Mbamba on 2016/06/28.
//
//

#import <UIKit/UIKit.h>
#import "GamePickerViewControllerDelegate.h"
#import "PZLGameLogic.h"
#import "PZLTableViewController.h"

@class GamePickerViewController;

@interface GamePickerViewController : PZLTableViewController

@property (nonatomic, weak) id <GamePickerViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *game;

@end
