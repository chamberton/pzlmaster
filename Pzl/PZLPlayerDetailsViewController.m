//
//  PlayerDetailsViewController.m
//
//
//  Created by Serge Mbamba on 2016/06/28.
//
//
#import "PZLCoreDataManager.h"
#import "PZLAppDelegate.h"
#import "iToast.h"
#import "PZLPlayerDetailsViewController.h"
#import "PZLPlayerRecordColletionViewController.h"
#import "PZLColorSchemeManager.h"
@interface PlayerDetailsViewController()<UITableViewDataSource>
@end


@implementation PlayerDetailsViewController {
    NSString *_game;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        _game = NSLocalizedString(@"Not provided", @"Not provided");
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.detailText.text = _game;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView* header =(UITableViewHeaderFooterView *)  view;
    header.textLabel.textColor = [PZLColorSchemeManager getMainTextColor];
    header.textLabel.font =  [UIFont fontWithName:@"Noteworthy-Bold" size:16];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.detailTextLabel.textColor = [PZLColorSchemeManager getMainSubtextColor];
    cell.detailTextLabel.font =  [UIFont fontWithName:@"Noteworthy-Bold" size:14];
    cell.textLabel.font =  [UIFont fontWithName:@"Noteworthy-Bold" size:16];
    cell.textLabel.textColor =  [PZLColorSchemeManager getMainTextColor];
    cell.opaque = NO;
    cell.backgroundColor = [UIColor clearColor];
    if(cell.accessoryType!=UITableViewCellAccessoryNone){
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow-disclosure"]];
    }
}

- (void)gamePickerViewController:(GamePickerViewController *)controller didSelectGame:(NSString *)game {
    _game = game;
    self.detailText.text = _game;
    self.detailText.textColor = [PZLColorSchemeManager getMainSubtextColor];
    self.detailText.font =  [UIFont fontWithName:@"Noteworthy-Bold" size:14];
    [self presentAdd];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        [self.playerName becomeFirstResponder];
    }
}


- (IBAction)cancel:(id)sender {
    [self.delegate playerDetailsViewControllerDidCancel:self];
}

- (IBAction)done:(id)sender {
    NSString* trimmedString =[self.playerName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (trimmedString!=nil && [trimmedString length]>0) {
        PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *moc = ad.managedObjectContext;
        PlayerList *player = (PlayerList *)[NSEntityDescription insertNewObjectForEntityForName:@"PlayerList"inManagedObjectContext:moc];
        player.name = self.playerName.text;
        player.continent =[NSNumber numberWithUnsignedInteger:[[NSArray arrayWithObjects:ContinentNameArray] indexOfObject:self.detailText.text]];
        player.rating = [NSNumber numberWithInt:0];
        player.completedGamesPoints = [NSNumber numberWithInt:0];
        player.dateCreated = [NSDate date];
        [self.delegate playerDetailsViewController:self didAddPlayer:player];
    }else{
        
        NSString* toastMessage=[[NSString alloc]initWithFormat:@"%@",NSLocalizedString(@"Empty_Name", @"Empty Name") ];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
        });
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PickGame"]) {
        GamePickerViewController *gamePickerViewController = segue.destinationViewController;
        gamePickerViewController.delegate = self;
        gamePickerViewController.game = _game;
    }
}

@end
