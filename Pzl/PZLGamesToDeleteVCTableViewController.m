//
//  PZLGamesToDeleteVCTableViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/24.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLGamesToDeleteVCTableViewController.h"
#import "PZLCoreDataManager.h"
#import "PZLPlayerRecordColletionViewController.h"
#import "PZLColorSchemeManager.h"

@interface PZLGamesToDeleteVCTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteButton;

@end

@implementation PZLGamesToDeleteVCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorColor =[UIColor clearColor];
    [self.tableView setEditing: YES animated: NO];
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.tableView.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"WallPaper"]];
    self.deleteButton.enabled = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentAdd];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _games.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.deleteButton.enabled = tableView.indexPathsForSelectedRows.count>0;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    self.deleteButton.enabled = tableView.indexPathsForSelectedRows.count>0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellToDelete" forIndexPath:indexPath];
    cell.textLabel.text = _games[indexPath.row].gamePicture;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",_games[indexPath.row].dateStopped];
    cell.backgroundView.backgroundColor =[UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [PZLColorSchemeManager getMainTextColor];
    cell.detailTextLabel.textColor  =[PZLColorSchemeManager getMainSubtextColor];
    cell.textLabel.font  = [UIFont fontWithName:@"Noteworthy-Bold" size:18];
    cell.detailTextLabel.font  = [UIFont fontWithName:@"Noteworthy-Bold" size:14];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSSet<SavedGames*>* gamesTodelete= [NSSet setWithObjects:[self.games objectAtIndex:indexPath.row], nil];
        if ( [PZLCoreDataManager updatePlayerWithName:self.player withSavedGames:gamesTodelete]){
            [PZLPlayerRecordColletionViewController update];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

- (IBAction)deleteSeleted:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle: NSLocalizedString(@"Deletion Confirmation", @"Deletion Confirmation")
                                  message:[NSString stringWithFormat:@"%@?", NSLocalizedString(@"Delete selected",@"Delete selected")]
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"Ok",@"Ok")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSArray<NSIndexPath *>  *selectedIndices = self.tableView.indexPathsForSelectedRows;
                             NSMutableArray<SavedGames*>* gamesTodelete = [NSMutableArray array];
                             for (NSIndexPath* indexPath in selectedIndices){
                                 [gamesTodelete addObject:[self.games objectAtIndex:indexPath.row]];
                             }
                             if ( [PZLCoreDataManager updatePlayerWithName:self.player withSavedGames:[NSSet setWithArray:gamesTodelete]]){
                                 [PZLPlayerRecordColletionViewController update];
                                 [self.games removeObjectsInArray:gamesTodelete];
                                 [self.tableView reloadData];
                                 [self presentAdd];
                             }
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self.tableView reloadData];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
