//
//  PZLPlayerRecordLayoutManager.m
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import "PZLPlayerRecordLayoutManager.h"

static NSString * const Kind = @"RecordCell";
static BOOL configureToTwoColum = NO;
static CGFloat ylevel =0;

@implementation PZLPlayerRecordLayoutManager

#pragma mark - Lifecycle

- (id)init {
    self = [super init];
    if (self) {
        [self setup];
        _oldrow=0;
        ylevel = 0;
               self.numberOfColumns = 1;
    }
     CGRect screenDimensions = [[UIScreen mainScreen] bounds];
    
    if (screenDimensions.size.height>400){
         _height=100.0f;
    }else{
         _height=75.0f;
        
    }
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        _height = ([UIScreen mainScreen].bounds.size.height - 250)/3;
    }
    _oldheight=_height;
    _oldSection=0;
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        [self setup];
         _oldrow=0;
         ylevel = 0;
        CGRect screenDimensions = [[UIScreen mainScreen] bounds];
        if (screenDimensions.size.height>600){
            _height=100.0f;
        }else{
            _height=70.0f;
            
        }
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            _height = ([UIScreen mainScreen].bounds.size.height - 250)/3;
        }
        _oldheight=_height;
    }
    
    return self;
}

- (CATransform3D)transformForAlbumPhotoAtIndex:(NSIndexPath *)indexPath {
    NSInteger offset = (indexPath.section * RotationStride + indexPath.item);
    
    return [self.rotations[offset % RotationCount] CATransform3DValue];
}

- (void)setup {
    configureToTwoColum = NO;
    ylevel =0;
    NSMutableArray *rotations = [NSMutableArray arrayWithCapacity:RotationCount];
    CGFloat percentage = 0.0f;
    
    for (NSInteger i = 0; i < RotationCount; i++) {
        CGFloat newPercentage = 0.0f;
        
        do {
            newPercentage = ((CGFloat)(arc4random() % 220) - 110) * 0.0001f;
        } while (fabs(percentage - newPercentage) < 0.006);
        
        percentage = newPercentage;
        CGFloat angle = 2 * M_PI * (1.0f + percentage);
        CATransform3D transform = CATransform3DMakeRotation(angle, 0.0f, 0.0f, 1.0f);
        [rotations addObject:[NSValue valueWithCATransform3D:transform]];
    }
    
    self.rotations = rotations;
}

#pragma mark - Properties Accessors

- (void)setItemInsets:(UIEdgeInsets)itemInsets {
    if (UIEdgeInsetsEqualToEdgeInsets(_itemInsets, itemInsets))
        return;
    
    _itemInsets = itemInsets;
    [self invalidateLayout];
}

- (void)setItemSize:(CGSize)itemSize {
    if (CGSizeEqualToSize(_itemSize, itemSize))
        return;
    
    _itemSize = itemSize;
    [self invalidateLayout];
}

- (void)setInterItemSpacingY:(CGFloat)interItemSpacingY {
    if (_interItemSpacingY == interItemSpacingY) return;
    
    _interItemSpacingY = interItemSpacingY;
    [self invalidateLayout];
}

- (void)setNumberOfColumns:(NSInteger)numberOfColumns {
    
    if (_numberOfColumns == numberOfColumns)
        return;
    
    _numberOfColumns = numberOfColumns;
    [self invalidateLayout];
}

#pragma mark - Layout

- (CGRect)frameAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section>4) {
        if(!configureToTwoColum){
        self.itemSize   = CGSizeMake(130.0f,_height);
        self.interItemSpacingY = 24.0f;
        self.oldNumberOfColumns= self.numberOfColumns;
        self.numberOfColumns = 2;
        configureToTwoColum = YES;
        }else{
            self.oldNumberOfColumns= self.numberOfColumns;
        }

    }else{
        configureToTwoColum = NO;
    }
    _oldheight=_height;
    CGRect screenDimensions = [[UIScreen mainScreen] bounds];
    if (screenDimensions.size.height>600){
        _height=125.0f;
    }else{
        _height=100.0f;
    }
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        _height = ([UIScreen mainScreen].bounds.size.height - 250)/3;
    }
    if ((indexPath.section%2==0 || indexPath.section==5) && indexPath.section!=6){
         _height   = 25.0f;
        [self setInterItemSpacingY:5.f];
        
    }
    NSInteger increment = self.numberOfColumns==self.oldNumberOfColumns?0:1;
    NSInteger row       = (NSInteger)ceil((double)indexPath.section / (double) self.numberOfColumns) + increment;
    if(row<self.oldrow){
        row=self.oldrow;
    }else{
        self.oldrow =row;
    }
    NSInteger column = indexPath.section % self.numberOfColumns;
    CGFloat spacingX = 25;
    self.numberOfColumns = indexPath.section<4?1:2;
    
    if (self.numberOfColumns==1) {
        self.itemSize   = CGSizeMake(self.collectionView.bounds.size.width-35,_height);
    }else{
        self.itemSize   = CGSizeMake((self.collectionView.bounds.size.width-35)/self.numberOfColumns-spacingX *(self.numberOfColumns-1)/self.numberOfColumns,_height);
    }

    if (self.numberOfColumns > 1) {
       spacingX = spacingX / (self.numberOfColumns - 1);
    }
    
    CGFloat originX = ceilf(self.itemInsets.left + (self.itemSize.width + spacingX) * column);
    
    CGFloat originY = floor(self.itemInsets.top +
                            (self.itemSize.height + self.interItemSpacingY) * row)-20;
    if (indexPath.section==0) {
        ylevel=originY;
    }else {
        if((indexPath.section!=_oldSection && (self.numberOfColumns!=self.oldNumberOfColumns || self.numberOfColumns==1))){
            originY = ylevel+_oldheight+10;
            ylevel=originY;
        }else{
            originY=ylevel;
        }
    }
    _oldSection  = indexPath.section;
    float lastShift=(indexPath.section==6 || indexPath.section==7)?35:0;
    CGRect frame = CGRectMake(originX, originY+lastShift, self.itemSize.width, self.itemSize.height);
    
    return frame;
}

- (void)prepareLayout {
    self.oldheight =0;
    self.height=100;
    self.oldheight=100;
    self.oldrow=0;
    self.numberOfColumns=0;
    self.oldNumberOfColumns=0;
    ylevel=0;
    self.itemInsets = UIEdgeInsetsMake(22.0f, 22.0f, 13.0f, 22.0f);
    self.itemSize   = CGSizeMake(250.0f,_height);
    self.interItemSpacingY = 24.0f;
    self.numberOfColumns = 1;
    self.oldNumberOfColumns= self.numberOfColumns;
    NSMutableDictionary *newLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView numberOfSections];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    for (NSInteger section = 0; section < sectionCount; section++) {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++) {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            
            UICollectionViewLayoutAttributes *itemAttributes =
            [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self frameAtIndexPath:indexPath];
            if(item%2!=0){
            itemAttributes.transform3D = [self transformForAlbumPhotoAtIndex:indexPath];
            }
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    newLayoutInfo[Kind] = cellLayoutInfo;
    self.layoutInfo = newLayoutInfo;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier,
                                                         NSDictionary *elementsInfo,
                                                         BOOL *stop) {
        [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                          UICollectionViewLayoutAttributes *attributes,
                                                          BOOL *innerStop) {
            if (CGRectIntersectsRect(rect, attributes.frame)) {
                [allAttributes addObject:attributes];
            }
        }];
    }];
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.layoutInfo[Kind][indexPath];
}

- (CGSize)collectionViewContentSize {
    
    NSInteger rowCount = [self.collectionView numberOfSections] / self.numberOfColumns;

    if ([self.collectionView numberOfSections] % self.numberOfColumns){
     rowCount++;
    }
    
    CGFloat height = self.itemInsets.top +
    rowCount * self.itemSize.height + (rowCount - 1) * self.interItemSpacingY +
    self.itemInsets.bottom;
    
    return CGSizeMake(self.collectionView.bounds.size.width, height);
}

@end
