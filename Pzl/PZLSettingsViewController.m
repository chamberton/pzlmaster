//
//  PZLSettingsViewController.m
//
//
//  Created by Serge Mbamba on 2016/06/30.
//
//

#import "PZLSettingsViewController.h"
#import "PZLSettingCell.h"
#import "PZLColorSchemeManager.h"
#import "PZLUserDefaults.h"
#import "TypesDefinition.h"
#import "PZLGameLogic.h"

@interface PZLSettingsViewController()

@end

NSArray* _arrayOfDifficultyLevels ;
BOOL justAppeared;


enum settingsObject{
    Sounds=0,
    Difficulty,
    ShowTimer,
    EnableHints,
    NumberOfHints,
    Gameboard
};

enum settingsTag{
    difficultyTag=1,
    hintsTag=4,
    boardSizeTag=5
};

@implementation PZLSettingsViewController{
    NSArray* _arrayOfDifficultyLevelsValues;
    NSArray* _arrayOfNumberOfHints;
    NSArray* _arrayOfBoardSizes;
    NSTimer*  _enabledKeysChecker;
    BOOL _isON ;
}

+ (void) load {
    _arrayOfDifficultyLevels = [NSArray arrayWithObjects:levelElements];
}

static int difficulties[]  = { Beginner,
    Intermediate,
    Advanced,
    Expert
};

- (void)viewDidLoad {
    _isON = YES;
    justAppeared = YES;
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];
    
    _arrayOfDifficultyLevelsValues = @[[NSNumber numberWithInt:Beginner],
                                       [NSNumber numberWithInt:Intermediate],
                                       [NSNumber numberWithInt:Advanced],
                                       [NSNumber numberWithInt:Expert]];
    
    _arrayOfNumberOfHints= @[@"0",
                             @"20",
                             @"40",
                             @"60",
                             @"80",
                             NSLocalizedString(@"Unlimited",@"Unlimited")];
    
    _arrayOfBoardSizes= [PZLGameLogic boardSizes];
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:YES];
    [_enabledKeysChecker invalidate];
    [self setGameValues];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if(_enabledKeysChecker && !_enabledKeysChecker.valid){
        _enabledKeysChecker = nil;
        _enabledKeysChecker = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector: @selector(CheckEnabledKeys:) userInfo:nil repeats:YES];
    }
    
}
- (IBAction)selectPicture:(id)sender {
    
    [self performSegueWithIdentifier:@"showGameSelection" sender:sender];
    
}
 BOOL shouldAppear;
-(void) setGameValues{
    shouldAppear = YES;
    NSUInteger value  =0;
    for (int i=0; i<NUMBER_OF_ROWS_IN_SECTION; i++){
        NSIndexPath* path =  [NSIndexPath indexPathForRow:i inSection:0];
        PZLSettingCell* cell =[self.tableView cellForRowAtIndexPath:path];
        NSUInteger index = [cell.Picker selectedRowInComponent:0] ;
        switch (i) {
            case Sounds:
                [PZLUserDefaults storeBool:cell.Switch.on forKey:SOUNDS_ENABLED];
                break;
            case EnableHints:
                [PZLUserDefaults storeBool:cell.Switch.on forKey:HINTS_ENABLED];
                break;
            case ShowTimer:
                [PZLUserDefaults storeBool:cell.Switch.on forKey:TIMER_ENABLED];
                break;
            case NumberOfHints:
                if (index==5){
                    value = USHRT_MAX;
                }else{
                    value = [_arrayOfNumberOfHints[index] integerValue];
                }
                [PZLUserDefaults storeInteger:value forKey:CURRENT_HINTS];
                break;
            case Difficulty:
                value = [_arrayOfDifficultyLevelsValues[index] integerValue];
                [PZLUserDefaults storeInteger:difficulties[value] forKey:CURRENT_LEVEL];
                break;
            case Gameboard:
                value = [_arrayOfBoardSizes[index] integerValue];
                [PZLUserDefaults storeInteger:value forKey:CURRENT_BOARD];
                break;
            default:
                break;
        }
        
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:NO];
    [self setGameValues];
}

-(void) viewWillLayoutSubviews{
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return NUMBER_OF_ROWS_IN_SECTION;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)CheckEnabledKeys:(NSTimer *)timer{
    
    if (_enabledKeysChecker==timer) {
        BOOL actualState = _isON;
        BOOL stateChanged = NO;
        for (UITableViewCell *cell in [self.tableView visibleCells]) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            if(indexPath.row==3){
                
                PZLSettingCell* sCell =  (PZLSettingCell*)cell;
                
                if(sCell.Switch.on!=actualState){
                    _isON = sCell.Switch.on;
                    stateChanged= YES;
                }
            }
            if (stateChanged){
                [self.tableView reloadData];
                break;
            }
        }
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCellUI" forIndexPath:indexPath];
    PZLSettingCell* sCell = (PZLSettingCell*)cell;
    cell.backgroundColor = [UIColor clearColor];
    
    if(indexPath.row==0){
        sCell.Label.text = NSLocalizedString(@"Sounds",@"Sounds");
        sCell.Picker.hidden = true;
    }else if(indexPath.row==1){
        sCell.Label.text = NSLocalizedString(@"Difficulty",@"Difficulty");
        
        sCell.Switch.hidden=true;
        sCell.Picker.delegate  = self;
        sCell.Picker.dataSource = self;
        sCell.Picker.tag=indexPath.row;
    }
    else if(indexPath.row==2){
        sCell.Label.text = NSLocalizedString(@"Show Timer",@"Show Timer");
        sCell.Picker.hidden=true;
    }
    else if(indexPath.row==3){
        sCell.Label.text = NSLocalizedString(@"Enable Hints",@"Enable Hints");
        sCell.Picker.hidden=true;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _enabledKeysChecker = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector: @selector(CheckEnabledKeys:) userInfo:nil repeats:YES];
            [_enabledKeysChecker fire];
        });
        
    }
    else if(indexPath.row==4){
        
        sCell.Label.text = NSLocalizedString(@"Number of Hints",@"Number of Hints");
        sCell.Picker.hidden=true;
        sCell.Switch.hidden=true;
        sCell.Picker.hidden=false;
        sCell.Picker.delegate  = self;
        sCell.Picker.dataSource = self;
        sCell.Picker.tag=indexPath.row;
        
        if(!_isON){
            [sCell.Picker selectRow:0 inComponent:0 animated:NO];
            sCell.userInteractionEnabled =false;
        }
        else{
            if(justAppeared){
                [sCell.Picker selectRow:2 inComponent:0 animated:NO];
                justAppeared = NO;
            };
            sCell.userInteractionEnabled =true;
        }

    }
    else if(indexPath.row==5){
        sCell.Label.text = NSLocalizedString(@"Gameboard",@"Gameboard");
        sCell.Switch.hidden=true;
        sCell.Picker.delegate  = self;
        sCell.Picker.dataSource = self;
        sCell.Picker.tag=indexPath.row;
    }
    [sCell.Label setTextColor:[PZLColorSchemeManager getMainTextColor]];
    sCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



#pragma mark - Picker view delegate and datasource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        
        subview.hidden = (CGRectGetHeight(subview.frame) == 0.5);
    }];
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==difficultyTag){
        return [_arrayOfDifficultyLevels count];
    }else if (pickerView.tag==NumberOfHints){
        return [_arrayOfNumberOfHints count];
    }else if(pickerView.tag==boardSizeTag){
        return [_arrayOfBoardSizes count];
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag==difficultyTag){
        return [_arrayOfDifficultyLevels objectAtIndex:row];
    }else if (pickerView.tag==hintsTag){
        return [_arrayOfNumberOfHints objectAtIndex:row];
    }else if(pickerView.tag==boardSizeTag){
        return [_arrayOfBoardSizes objectAtIndex:row];
        
    }
    return  NULL;
}
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSMutableString *title = NULL;
    if (pickerView.tag==difficultyTag){
        title= [_arrayOfDifficultyLevels objectAtIndex:row];
    }else if (pickerView.tag==hintsTag){
        title= [_arrayOfNumberOfHints objectAtIndex:row];
    }else if(pickerView.tag==boardSizeTag){
        title= [_arrayOfBoardSizes objectAtIndex:row];
        
    }
    
    const CGFloat fontSize = 13;
    NSDictionary *attrs = @{
                            NSFontAttributeName:[UIFont boldSystemFontOfSize:fontSize],
                            NSForegroundColorAttributeName:[PZLColorSchemeManager getPickerColor]
                            };
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:attrs];
    
    return attString;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [pickerView resignFirstResponder];
}

@end
