//
//  BHAlbumPhotoCell.m
//  CollectionViewTutorial
//
//  Created by Bryan Hansen on 11/3/12.
//  Copyright (c) 2012 Bryan Hansen. All rights reserved.
//

#import "PZLAlbumPhotoCell.h"
#import "PZLPhotoAlbumLayout.h"
#import <QuartzCore/QuartzCore.h>

@interface BHAlbumPhotoCell ()

@property (nonatomic, strong, readwrite) UIImageView *imageView;
@property (nonatomic, strong, readwrite) UIImageView *checkView;

@end

@implementation BHAlbumPhotoCell
@synthesize selectedForDeletion=_selectedForDeletion;

#pragma mark - Lifecycle

float TickimageSideLength = 40;
float borderIndent = 2.5;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _selectedForDeletion = false;
        self.backgroundColor = [ UIColor grayColor];
        self.layer.shadowColor = [UIColor grayColor].CGColor;
        self.layer.shadowRadius = 3.0f;
        self.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        self.layer.shadowOpacity = 1.0f;
        // make sure we rasterize nicely for retina
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.layer.shouldRasterize = YES;
        CGRect frameImage = CGRectMake(self.bounds.origin.x  + borderIndent, self.bounds.origin.y + borderIndent, self.bounds.size.width - borderIndent, self.bounds.size.height - borderIndent);
        self.imageView = [[UIImageView alloc] initWithFrame:frameImage];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        CGRect frameTick = CGRectMake(self.bounds.origin.x  + self.bounds.size.width
                                  - TickimageSideLength/2, self.bounds.origin.y - TickimageSideLength/2, TickimageSideLength, TickimageSideLength);
        self.checkView = [[UIImageView alloc] initWithFrame:frameTick];
        self.checkView.contentMode = UIViewContentModeScaleAspectFill;
        self.checkView.clipsToBounds = YES;
        self.checkView.opaque = TRUE;
        self.checkView.layer.zPosition = [BHPhotoAlbumLayout getMaxZIndex];
        
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.checkView];
        self.userInteractionEnabled = TRUE;
    }

    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.checkView.image = nil;
    self.imageView.image = nil;
}
-(void) dealloc{

    [self.contentView willRemoveSubview:self.imageView];
    [self.contentView willRemoveSubview:self.checkView];
    
    self.checkView = nil;
    self.imageView = nil;
}
@end
