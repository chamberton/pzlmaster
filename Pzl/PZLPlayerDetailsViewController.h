//
//  PlayerDetailsViewController.h
//  
//
//  Created by Serge Mbamba on 2016/06/28.
//
//

#import <UIKit/UIKit.h>
#import "GamePickerViewControllerDelegate.h"
#import "PZLGamePickerViewController.h"
#import "PZLTableViewController.h"
@class PlayerDetailsViewController;


@interface PlayerDetailsViewController : PZLTableViewController <GamePickerViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *playerName;
@property (weak, nonatomic) IBOutlet UILabel *detailText;
@property (nonatomic, weak) id <PlayerDetailsViewControllerDelegate> delegate;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end
