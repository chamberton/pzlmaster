//
//  main.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/06/27.
//  Copyright (c) 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PZLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PZLAppDelegate class]));
    }
}
