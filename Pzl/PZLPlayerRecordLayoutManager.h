//
//  PZLPlayerRecordLayoutManager.h
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#ifndef _PZLPlayerRecordLayoutManager_h_
#define _PZLPlayerRecordLayoutManager_h_

#import <UIKit/UIKit.h>

static NSUInteger const RotationCount = 32;
static NSUInteger const RotationStride = 3;

@interface PZLPlayerRecordLayoutManager : UICollectionViewLayout

@property (nonatomic) UIEdgeInsets itemInsets;
@property (nonatomic) CGSize itemSize;
@property (nonatomic) NSInteger oldrow;
@property (nonatomic) CGFloat interItemSpacingY;
@property (nonatomic) NSInteger numberOfColumns;
@property (nonatomic) NSInteger  oldNumberOfColumns;
@property (nonatomic) NSInteger oldSection;
@property (assign) float height;
@property (assign) float oldheight;
@property (nonatomic, strong) NSDictionary *layoutInfo;
@property (nonatomic, strong) NSArray *rotations;

- (CGRect)frameAtIndexPath:(NSIndexPath *)indexPath;

@end

#endif
