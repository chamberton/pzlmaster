//
//  PZLplayerRecordColletionView.m
//
//
//  Created by Serge Mbamba on 2016/07/01.
//
//
//

#import "PZLGameLogic.h"
#import "iToast.h"
#import "PZLCoreDataManager.h"
#import "PZLPlayerRecordColletionViewController.h"
#import "PZLPlayerRecordCell.h"
#import "UIImageView+Text.h"
#import "UITap+Tag.h"
#import "DraggableViewController.h"
#import "SavedGames+CoreDataProperties.h"
#import "BestScore+CoreDataProperties.h"
#import "PZLPlayerSavedRecordCell.h"
#import "PZLUserDefaults.h"
#import "PZLDateUtility.h"
#import "PZLGamesToDeleteVCTableViewController.h"
#import "PZLPlayerBestScore.h"
#import "PZLBestScoreDetailsTableViewController.h"

static NSOrderedSet* _savedGames;
static NSOrderedSet* _bestGames;
static PlayerList* _currentPlayer;
static int _currentIndex;
static NSString* const savedGameRecordCellIdentifier = @"PZLPlayerSavedRecordCell";
static NSString* const  bestGameRecordCellIdentifier =@"PZLPlayerBestScore";

@interface  WithParentCell : UITableView

@property (readwrite,nonatomic) PZLPlayerRecordCell *parentCell;

@end

@implementation WithParentCell

@synthesize  parentCell;

@end

@interface PZLPlayerRecordColletionViewController ()

@property (readwrite,strong) WithParentCell* bestGamesTableView;
@property (readwrite,strong) WithParentCell* savedGamesTableView;

@end


@implementation PZLPlayerRecordColletionViewController

+ (void)load{
    _savedGames = [NSOrderedSet orderedSet];
    _bestGames  = [NSOrderedSet orderedSet];
}

+ (void)setSavedGames:(NSSet *)games {
    
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"dateStopped"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [games
                                 sortedArrayUsingDescriptors:sortDescriptors];
    _savedGames = [NSOrderedSet orderedSetWithArray:sortedEventArray];
}

+ (void)setBestGames:(NSSet *)games {
    
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"score"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [games
                                 sortedArrayUsingDescriptors:sortDescriptors];
    _bestGames = [NSOrderedSet orderedSetWithArray:sortedEventArray];
}

+ (void)setCurrentPlayer:(id)player {
    _currentPlayer = (PlayerList*) player;
}

+ (void)setPlayerIndex:(int)index {
    _currentIndex = index;
}

+ (void)update {
    NSMutableArray* players = [PZLCoreDataManager getPlayers];
    PlayerList *player = players[_currentIndex];
    [PZLPlayerRecordColletionViewController setSavedGames:player.savedgames];
    [PZLPlayerRecordColletionViewController setBestGames:player.bestgames];
    [PZLPlayerRecordColletionViewController setCurrentPlayer:player];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     self.layoutManager = [PZLPlayerRecordLayoutManager new];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.opaque = NO;
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];
    [self.collectionView registerClass:[PZLPlayerRecordCell class] forCellWithReuseIdentifier:PZLPlayerCellIdentifier];
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.25f alpha:1.0f];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = NO;
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.25f alpha:1.0f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshPlayerDetails:)
                                                 name:@"refreshPlayerDetails"
                                               object:nil];
    [PZLPlayerRecordColletionViewController update];
}

- (void)refreshPlayerDetails:(NSNotification *)note {
    
    if([note.name isEqualToString:@"refreshPlayerDetails"]){
        [PZLPlayerRecordColletionViewController update];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [PZLPlayerRecordColletionViewController update];
    [self.savedGamesTableView reloadData];
    [self.bestGamesTableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"deleteGames"]){
        PZLGamesToDeleteVCTableViewController* destinationVC = (PZLGamesToDeleteVCTableViewController*)segue.destinationViewController;
        destinationVC.games  = [NSMutableArray arrayWithArray:_currentPlayer.savedgames.allObjects];
        destinationVC.player = _currentPlayer.name;
    }
    if([segue.identifier isEqualToString:@"showBestScoreDetails"]){
        PZLBestScoreDetailsTableViewController* destinationVC = (PZLBestScoreDetailsTableViewController*)segue.destinationViewController;
        destinationVC.title = NSLocalizedString(@"Game summary", @"Game summary") ;
        destinationVC.bestScore = sender;
    }
}

#pragma mark - UITableView Delegate

-  (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView==self.savedGamesTableView){
        return _savedGames.count;
    }
    if(tableView==self.bestGamesTableView){
        return _bestGames.count<MAX_NUMBER_OF_BEST_GAMES?_bestGames.count:MAX_NUMBER_OF_BEST_GAMES;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView==self.savedGamesTableView){
        SavedGames* game = [_savedGames objectAtIndex:indexPath.row];
        
        if(game!=nil){
            if ([DraggableViewController setSavedGame:game]){
                [self performSegueWithIdentifier:@"startSavedGame" sender:[self.savedGamesTableView parentCell]];
            }
        }
    }
    if(tableView==self.bestGamesTableView){
        
        BestScore* game = [_bestGames objectAtIndex:indexPath.row];
        
        if(game!=nil){
            [self performSegueWithIdentifier:@"showBestScoreDetails" sender:game];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView==self.savedGamesTableView){
        PZLPlayerSavedRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:savedGameRecordCellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        SavedGames* item  = [_savedGames objectAtIndex:indexPath.row];
        cell.titleLabel.text =  NSLocalizedStringFromTable( item.gamePicture, @"ImageNames", nil);
        if([ cell.titleLabel.text isEqualToString:@"letters.jpg"]){
            cell.titleLabel.text = @"Letters";
        }
        else if([cell.titleLabel.text isEqualToString:@"numbers.jpg"]){
            cell.titleLabel.text = @"Numbers";
        }
        cell.dateLabel.text  = [PZLDateUtility stringFromDate:[PZLDateUtility setTimeToCommonFormat:item.dateStopped] withFormat:CommonLocalTime];
        cell.dateLabel.textColor = [PZLColorSchemeManager getDateColor];
        cell.titleLabel.textColor =[PZLColorSchemeManager getMainSubtextColor];
        return cell;
        
    }
    
    if(tableView==self.bestGamesTableView){
        PZLPlayerBestScore *cell = [tableView dequeueReusableCellWithIdentifier:bestGameRecordCellIdentifier];
        BestScore* item  = [_bestGames objectAtIndex:indexPath.row];
        NSLog(@"%@ %@",item.boardName,item.playDate);
        cell.backgroundColor = [UIColor clearColor];
        cell.titleLabel.text =  NSLocalizedStringFromTable(item.boardName, @"ImageNames", nil);
        if([ cell.titleLabel.text isEqualToString:@"letters.jpg"]){
            cell.titleLabel.text = @"Letters";
        }
        else if([cell.titleLabel.text isEqualToString:@"numbers.jpg"]){
            cell.titleLabel.text = @"Numbers";
        }
        
        cell.titleLabel.textColor =[PZLColorSchemeManager getMainSubtextColor];
        cell.scoreLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)item.score.integerValue];
        return cell;
    }
    
    return nil;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return  NUMBER_OF_SECTIONS_PLAYER_RECORD_COLLECTION_VIEW*2;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if((section%2==0 || section==5) && section!=6){
        return 1;
    }else{
        return 5;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath != nil){
        if (indexPath.section==6){
            if (_savedGames.count>0){
                [self performSegueWithIdentifier:@"deleteGames" sender:self];
            }else{
                NSString* toastMessage=[[NSString alloc]initWithFormat:@"%@",NSLocalizedString(@"NoGamesTodelete", @"There are no saved games") ];
               
                    [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
                
            }
        }
        if(indexPath.row==4 && indexPath.section==7){
            UICollectionViewCell* cell = [self.collectionView cellForItemAtIndexPath:indexPath];
            [PZLGameLogic setPlayer:_currentPlayer];
            [self performSegueWithIdentifier:@"startGame" sender:cell];
        }
    }
}

+ (void)saveGame:(id)game {
    [PZLCoreDataManager addGame:game forPlayer:_currentPlayer];
}

+ (void)saveBestScore:(id)game {
    [PZLCoreDataManager addBestScore:game forPlayer:_currentPlayer];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PZLPlayerRecordCell *recordCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:PZLPlayerCellIdentifier
                                              forIndexPath:indexPath];
    
    if (indexPath.section==7){
        
        recordCell.imageView.image = [UIImage imageNamed:@"play button 2"];
    }
    else if(indexPath.section==6){
        recordCell.imageView.image = [UIImage imageNamed:@"delete 1"];
        
    }
    else if (indexPath.section==4){
        recordCell.label.text=NSLocalizedString(@"Delete saved games",@"Delete saved games");
        recordCell.backgroundColor  = [UIColor clearColor];
    }
    else if (indexPath.section==5){
        recordCell.label.text=NSLocalizedString(@"Play New Game",@"Play New Game");
        recordCell.backgroundColor  = [UIColor clearColor];
    }
    else if (indexPath.section==0){
        recordCell.label.text=NSLocalizedString(@"Best Scores",@"Best Scores");
        recordCell.backgroundColor  = [UIColor clearColor];
    }
    else if (indexPath.section==1 || indexPath.section==3){
        
        if(indexPath.row==4){
            if(indexPath.section==3){
                CGRect tableViewFrame = CGRectMake( recordCell.bounds.origin.x+recordCell.bounds.size.width*0.05, recordCell.bounds.origin.y , recordCell.bounds.size.width*0.9, recordCell.bounds.size.height);
                self.savedGamesTableView = [[WithParentCell alloc] initWithFrame:tableViewFrame];
                self.savedGamesTableView.backgroundColor = [UIColor clearColor];
                self.savedGamesTableView.delegate = self;
                self.savedGamesTableView.dataSource = self;
                UINib *cellNib = [UINib nibWithNibName:savedGameRecordCellIdentifier bundle:nil];
                [self.savedGamesTableView registerNib:cellNib forCellReuseIdentifier:savedGameRecordCellIdentifier];
                self.savedGamesTableView.parentCell=recordCell;
                [self.savedGamesTableView  reloadData];
                self.savedGamesTableView.separatorColor = [UIColor clearColor];
                [recordCell addSubview:self.savedGamesTableView];
            }
            if(indexPath.section==1){
                
                CGRect tableViewFrame = CGRectMake( recordCell.bounds.origin.x+recordCell.bounds.size.width*0.05, recordCell.bounds.origin.y , recordCell.bounds.size.width*0.9, recordCell.bounds.size.height);
                self.bestGamesTableView = [[WithParentCell alloc] initWithFrame:tableViewFrame];
                self.bestGamesTableView.backgroundColor = [UIColor clearColor];
                self.bestGamesTableView.delegate = self;
                self.bestGamesTableView.dataSource = self;
                self.bestGamesTableView.separatorColor = [UIColor clearColor];
                UINib *cellNib = [UINib nibWithNibName:bestGameRecordCellIdentifier bundle:nil];
                [self.bestGamesTableView registerNib:cellNib forCellReuseIdentifier:bestGameRecordCellIdentifier];
                [self.bestGamesTableView  reloadData];
                [recordCell addSubview:self.bestGamesTableView];
            }
        }
    }
    
    else if (indexPath.section==2){
        recordCell.label.text=NSLocalizedString(@"Saved Games",@"Saved Games");
        recordCell.backgroundColor  = [UIColor clearColor];
    }
    recordCell.label.textAlignment = NSTextAlignmentCenter;
    recordCell.label.backgroundColor = [UIColor clearColor];
    [recordCell.label  setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:16]];
    [recordCell.label setTextColor:[PZLColorSchemeManager getMainTextColor]];
    
    return recordCell;
}

- (void)myTapMethod:(id)sender {
    UITap_Tag* tap = (UITap_Tag*) sender;
    SavedGames* game = nil;
    for ( SavedGames* gameIn in _currentPlayer.savedgames){
        if ([gameIn.gamePicture isEqualToString:tap.label] && [tap.date isEqualToDate:gameIn.dateStarted]){
            game =gameIn;
        }
    }
    if(game!=nil){
        if (  [DraggableViewController setSavedGame:game])
            [self performSegueWithIdentifier:@"startSavedGame" sender:tap.cell];
        else{
            NSString* toastMessage=[NSString stringWithFormat:@"%@",NSLocalizedString(@"CouldNoLoadSavedGame", @"CouldNoLoadSavedGame")];
            [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
        }
    }
}

#pragma mark <UICollectionViewDelegate>

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
@end
