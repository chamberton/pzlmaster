//
//  PZLImageChangeObserver.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#ifndef PZLImageChangeObserver_h
#define PZLImageChangeObserver_h

@protocol PZLImageChangeObserver <NSObject>

- (void)registerAsImageChangeObserver;
- (void)updateImageList;

@end
#endif /* PZLImageChangeObserver_h */
