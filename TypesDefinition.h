//
//  TypesDefinition.h
//  Pzl
//
//  Created by Serge on 2016/09/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#ifndef TypesDefinition_h
#define TypesDefinition_h

#define MAX_NUMBER_OF_BEST_GAMES 5
#define MAX_RATING 5
#define NUMBER_OF_SECTIONS_PLAYER_RECORD_COLLECTION_VIEW 4
#define NUMBER_OF_ENTRIES_FOR_SIZES 6
#define NUMBER_OF_ITEMS_IN_SECTION 5
#define ASCII_SHIFT_FOR_LETTER 64
#define SECTION_COUNT 1
#define NUMBER_OF_ROWS_IN_SECTION 6
#define DEFAULT_COMPRESSION_RATE   0.5
#define qualityIncrement   0.25

static NSArray* direction; 

typedef enum {
    North,
    South,
    East,
    West
} Location;

typedef enum {
    Beginner,
    Intermediate,
    Advanced,
    Expert
}GameLevel;


typedef struct _moveStruct {
    int source;
    int destinaion;
} moveStruct;

typedef enum _CotinentCode : NSUInteger {
    Continent_NotProvided,
    Continent_Africa,
    Continent_Asia,
    Continent_Australia_Oceania,
    Continent_Europe,
    Continent_Antarctica,
    Continent_America
} CotinentCode;

static NSString* const CURRENT_HINTS=@"NUMBER_OF_HINTS_CURRENT_SELECTED";
static NSString* const CURRENT_LEVEL=@"GAME_LEVEL_CURRENT_SELECTED";
static NSString* const CURRENT_BOARD=@"BOARD_SIZE_CURRENT_SELECTED";
static NSString* const SOUNDS_ENABLED=@"SOUND_IS_ENABLED";
static NSString* const HINTS_ENABLED=@"HINTS_IS_ENABLED";
static NSString* const TIMER_ENABLED=@"TIMER_IS_ENABLED";


#define levelElements NSLocalizedString( @"Beginner",  @"Beginner"),NSLocalizedString( @"Intermediate",  @"Intermediate"),NSLocalizedString( @"Advanced",  @"Advanced"),NSLocalizedString( @"Expert",  @"Expert"),nil

#define ContinentNameArray NSLocalizedString(@"Not provided",@"Not provided"),NSLocalizedString(@"Africa",@"Africa"),NSLocalizedString(@"Asia",@"Asia"), NSLocalizedString(@"Australia/Oceania",@"Australia/Oceania"),NSLocalizedString(@"Europe",@"Europe"), NSLocalizedString(@"Antarctica",@"Antarctica"),NSLocalizedString(@"America",@"America"),nil

extern NSArray* _arrayOfDifficultyLevels;

#endif /* TypesDefinition_h */
