//
//  NSNumber+Move.h
//  Pzl
//
//  Created by Serge on 2016/08/30.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "TypesDefinition.h"

@interface  NSNumber(NSNumber_Move)

-(NSNumber*) movedTo : (Location) directionToMove
           itemPerRow: (int) item_per_row;
@end
