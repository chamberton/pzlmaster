//
//  NSDictionary+KeyForValue.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/15.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(NSDictionary_KeyForValue)
-(id)keyForValue:(id)value;
@end
