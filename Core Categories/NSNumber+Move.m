//
//  NSNumber+Move.m
//  Pzl
//
//  Created by Serge on 2016/08/30.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "NSNumber+Move.h"

@implementation  NSNumber(NSNumber_Move)


-(NSNumber*) movedTo : (Location) directionToMove
           itemPerRow: (int) item_per_row{
    int shift  = 0;
    if (!self)
        return nil;
    switch (directionToMove) {
        case West:
            shift = -1;
            break;
        case East :
            shift = 1;
            break;
        case South:
            shift = item_per_row;
            break;
        case North:
            shift= -1* item_per_row;
        default:
            break;
    }
    return [NSNumber numberWithInteger:self.intValue + shift];
    
}
@end
