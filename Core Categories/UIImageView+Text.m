//
//  UIImageView+Text.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/18.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "UIImageView+Text.h"
#import "PZLColorSchemeManager.h"

@implementation UIImageView(UIImageView_Text)

-(void) drawText:(NSString*) text
                atPoint:(CGPoint)   point
{
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(point.x,point.y , self.frame.size.width*0.80,self.frame.size.height*0.80)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = [UIFont fontWithName:@"Noteworthy-Bold" size:16];
    [lblTitle setTextColor:[PZLColorSchemeManager getMainSubtextColor]];
    lblTitle.text = text;
    [self addSubview:lblTitle];

}

-(void) drawText:(NSString*) text
{
    CGPoint  point= self.frame.origin;
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(point.x,point.y , self.frame.size.width*0.80,self.frame.size.height*0.25)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = [UIFont fontWithName:@"Noteworthy-Bold" size:16];
    [lblTitle setTextColor:[PZLColorSchemeManager getMainTextColor]];
    lblTitle.text = text;
    [self addSubview:lblTitle];
}


-(void) drawText:(NSString*) text
            withfont : (UIFont *)font

{
    CGPoint  point = self.frame.origin;
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(point.x,point.y , self.frame.size.width*0.80,self.frame.size.height*0.25)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = font;
    [lblTitle setTextColor:[PZLColorSchemeManager getMainTextColor]];
    lblTitle.text = text;
    [self addSubview:lblTitle];
    
   }
@end
