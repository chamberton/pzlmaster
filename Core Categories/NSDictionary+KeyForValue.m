//
//  NSDictionary+KeyForValue.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/15.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "NSDictionary+KeyForValue.h"

@implementation NSDictionary(NSDictionary_KeyForValue)

- (id)keyForValue:(id)value {
    for (NSString* key in [self allKeys]){
        if ([[self valueForKey:key] isEqual:value]){
            return key;
        }
    }
    return nil;
}
@end
