//
//  UIImageView+Text.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/18.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface  UIImageView(UIImageView_Text)
-(void) drawText:(NSString*) text
         atPoint:(CGPoint)   point;

-(void) drawText:(NSString*) text;

-(void) drawText:(NSString*) text
       withfont : (UIFont *)font;

@end