//
//  UITap+Tag.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/18.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#import "PZLPlayerRecordCell.h"
#import <UIKit/UIKit.h>

@interface UITap_Tag : UITapGestureRecognizer
@property (readwrite,nonatomic) NSUInteger Tag ;
@property (readwrite,nonatomic) NSString*  label;
@property (readwrite,nonatomic) NSDate*  date;
@property (weak,readwrite,nonatomic)  PZLPlayerRecordCell *cell;
@end
