//
//  PZLTableTableViewController.h
//  iPzl
//
//  Created by Serge Mbamba on 2017/08/07.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PZLTableViewController : UITableViewController
- (void)presentAdd;

@end
