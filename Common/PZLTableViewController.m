//
//  PZLTableTableViewController.m
//  iPzl
//
//  Created by Serge Mbamba on 2017/08/07.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

@import GoogleMobileAds;
#import "PZLTableViewController.h"

@interface PZLTableViewController ()<GADInterstitialDelegate>
@property(nonatomic, strong) GADInterstitial *interstitial;
@end

@implementation PZLTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.interstitial = [self createAndLoadInterstitial];
}

- (void)presentAdd{
    if (self.interstitial.isReady){
        [self.interstitial presentFromRootViewController:self];
    }
}

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-3836110455040592/3557578975"];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

#pragma mark -  GADInterstitialDelegate
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

@end
