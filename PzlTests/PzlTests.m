////
////  PzlTests.m
////  PzlTests
////
////  Created by Serge Mbamba on 2016/06/27.
////  Copyright (c) 2016 Serge Mbamba. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "PZLGameLogic.h"
//#import <XCTest/XCTest.h>
//#import <OCMock/OCMock.h>
//
//@interface PzlTests : XCTestCase
//
//@end
//
//@implementation PzlTests
//
//- (void)setUp {
//    [super setUp];
//    // Put setup code here. This method is called before the invocation of each test method in the class.
//}
//
//- (void)tearDown {
//    // Put teardown code here. This method is called after the invocation of each test method in the class.
//    [super tearDown];
//}
//
//-(void) testComputeDispersion{
//    
//    NSMutableDictionary* _valuesAndPositions = [NSMutableDictionary dictionary];
//    [PZLGameLogic configureVariable:1 numberOfPieces:20];
//    
//    int ITEM_PER_ROW    = [PZLGameLogic numberOfItemPerRow];
//    int ITEM_PER_COLUMN = [PZLGameLogic numberOfItemPerColumn];
//    int ITEM_COUNT      = [PZLGameLogic numberOfPieces];
//    int level = [PZLGameLogic calculateDispersion:_valuesAndPositions];
//    for(int i = 0; i < 20; i++) {
//        [_valuesAndPositions setValue:[NSNumber numberWithInteger:i]  forKey:[NSString stringWithFormat:@"%d",i]];
//    }
//    NSLog(@"Scattering %d", level);
//    int maxdispersion = [PZLGameLogic computeMaxDispersion:Beginner];
//    int mindispersion = [PZLGameLogic computeMinDispersion:Beginner];
//    [PZLGameLogic scatter:_valuesAndPositions gameLevel:Beginner];
//    level = [PZLGameLogic calculateDispersion:_valuesAndPositions];
//    
//    NSLog(@"After scattering %d %d %d", level,mindispersion, maxdispersion);
//    XCTAssert(YES, @"Pass");
//}
//
//- (void)testExample {
//    
//    id tableViewMock = OCMClassMock([UITableView class]);
//    // This is an example of a functional test case.
//    XCTAssert(YES, @"Pass");
//}
//
//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}
//
//@end
