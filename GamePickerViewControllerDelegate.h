//
//  Protocols.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/06/30.
//  Copyright (c) 2016 Serge Mbamba. All rights reserved.
//

#ifndef Pzl_Protocols_h
#define Pzl_Protocols_h
#import <UIKit/UIKit.h>

@protocol GamePickerViewControllerDelegate <NSObject>
- (void)gamePickerViewController:(id)controller didSelectGame:(NSString *)game;
@end

@protocol PlayerDetailsViewControllerDelegate <NSObject>
- (void)playerDetailsViewControllerDidCancel:(id)controller;
- (void)playerDetailsViewController:(id)controller didAddPlayer:(id) player ;
@end

#endif
