//
//  PZLBestScoreDetailsTableViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/24.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLBestScoreDetailsTableViewController.h"
#import "PZLDateUtility.h"
#import "PZLGameLogic.h"
#import "PZLColorSchemeManager.h"

@interface PZLBestScoreDetailsTableViewController ()

@end

@implementation PZLBestScoreDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.allowsSelection = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BestScoreDetailsVC" forIndexPath:indexPath];
    int fullMinutes,remainingSeconds;
    
    switch (indexPath.row) {
        case 0:
            cell.detailTextLabel.text=self.bestScore.boardName;
            cell.textLabel.text = NSLocalizedString(@"Board Name",@"Board Name");
            break;
        case 1:
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%d",[self.bestScore.numberOfTiles intValue]];
            cell.textLabel.text = NSLocalizedString(@"# of pieces",@"# of pieces");
            break;
        case 2:
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%d",[self.bestScore.numberOfMoves intValue]];
            cell.textLabel.text = NSLocalizedString(@"# of moves",@"# of moves");
            break;
        case 3:
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)self.bestScore.score.unsignedIntegerValue];
            cell.textLabel.text = NSLocalizedString(@"Score",@"Score");
            break;
        case 4:
            fullMinutes=floor(((double)self.bestScore.duration.unsignedIntegerValue/60));
            remainingSeconds=(int)(self.bestScore.duration.unsignedIntegerValue % 60);
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%d %@ %d %@",fullMinutes,@"min",remainingSeconds,@"s"];
            cell.textLabel.text = NSLocalizedString(@"Play time",@"Play time");
            break;
        case 5:
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)self.bestScore.usedHints.unsignedIntegerValue];
            cell.textLabel.text = NSLocalizedString(@"Used Hints",@"Use Hints");
            break;
        case 6:
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",_arrayOfDifficultyLevels[self.bestScore.level.unsignedIntegerValue]];
            cell.textLabel.text = NSLocalizedString(@"Level",@"Level");
            break;
        case 7:
            cell.detailTextLabel.text=[PZLDateUtility stringFromDate:[PZLDateUtility setTimeToCommonFormat:self.bestScore.playDate] withFormat:CommonLocalTime];
            cell.textLabel.text = NSLocalizedString(@"Date played", @"Date played") ;
            break;
            
        default:
            break;
    }
    
    cell.textLabel.textColor = [PZLColorSchemeManager getMainTextColor];
    cell.detailTextLabel.textColor = [PZLColorSchemeManager getMainSubtextColor];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Noteworthy-Bold" size:16];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:@"Noteworthy-Bold" size:18];
    cell.opaque = NO;
    
    return cell;
}

@end
