//
//  PZLWinCellTitleArray.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#ifndef PZLWinCellTitleArray_h
#define PZLWinCellTitleArray_h

#define PZLWinCellTitleArray NSLocalizedString(@"Score",@""),NSLocalizedString(@"Moves",@""),NSLocalizedString(@"Duration",@""),NSLocalizedString(@"Used hints",@""),NSLocalizedString(@"Level",@""),NSLocalizedString(@"% completed per move",@""),NSLocalizedString(@"Old rating",@""),NSLocalizedString(@"Current rating",@""),NSLocalizedString(@"Suggested level",@""), nil

#endif /* PZLWinCellTitle_h */
