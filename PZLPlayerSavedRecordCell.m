//
//  PZLPlayerSavedRecordCell.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/14.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLPlayerSavedRecordCell.h"

@implementation PZLPlayerSavedRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
