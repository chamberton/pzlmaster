//
//  PZLSelectPictureSourceViewModel.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLSelectPictureSourceViewModel.h"
#import "PZLSelectPictureSourceRepository.h"

@interface PZLSelectPictureSourceViewModel()

@property  dispatch_queue_t updatingImageQueue;
@property (readwrite,strong) id<PZLSelectPictureSourceViewProtocol> view;
@property (readwrite,strong) PZLSelectPictureSourceRepository* repository;

@end

@implementation PZLSelectPictureSourceViewModel

- (instancetype)initWithView:(id<PZLSelectPictureSourceViewProtocol>)view {
    self= [super init];
    
    if (self){
        self.useCamera = NO;
        self.view = view;
        self.repository = [PZLSelectPictureSourceRepository new];
    }
    
    return self;
}

- (void)trySaveImage:(UIImage*)imageToSave {
    NSData * binaryImageData = UIImagePNGRepresentation(imageToSave);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if([self.repository saveImage:binaryImageData]){
            NSString* toastMessage = [NSString stringWithFormat:@"%@",NSLocalizedString(@"ImageAddedToiPzL", @"ImageAddedToiPzL") ];
            [self.view showToast:toastMessage];
            
        }else{
            NSString* toastMessage=[NSString stringWithFormat:@"%@",NSLocalizedString(@"ImageAlreadyExist", @"ImageAlreadyExist") ];
            [self.view showToast:toastMessage];
        }
        
    });
}

- (UIAlertController *)createSelectionAlertWithTitle:(NSString *)title alertDetails:(NSDictionary *)info {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",@"Ok") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self.view setSelectedSegmentToNone];
        UIImage* imageToSave = info[UIImagePickerControllerOriginalImage];
        [self trySaveImage:imageToSave];

    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self.view setSelectedSegmentToNone];
    }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    return alert;
}

@end
