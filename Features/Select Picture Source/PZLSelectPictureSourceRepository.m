//
//  SelectPictureSourceRepository.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLSelectPictureSourceRepository.h"
#import "PZLCoreDataManager.h"

@implementation PZLSelectPictureSourceRepository

- (NSString *)stringFromNow {
    NSDate* myNSDateInstance =[NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSString *stringFromDate = [formatter stringFromDate:myNSDateInstance];
    stringFromDate=[stringFromDate stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
    stringFromDate=[stringFromDate stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    stringFromDate=[stringFromDate stringByReplacingOccurrencesOfString:@":" withString:@"_"];
    stringFromDate=[stringFromDate stringByAppendingString:@".png"];
    
    return stringFromDate;
}

- (BOOL)saveImage:(NSData*)binaryImageData {
    
    NSString* imageName = [self stringFromNow];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    if([binaryImageData writeToFile:[documentsDirectory stringByAppendingPathComponent:imageName] atomically:YES]){
        
        if([PZLCoreDataManager addImageName:imageName enableErrorNotification:NO]){
            return YES;
        }
    }
    return NO;
}
@end
