//
//  SelectPictureSourceViewController.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PZLViewController.h"

@interface PZLSelectPictureSourceViewController : PZLViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
