//
//  PZLSelectPictureSourceRepository.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZLSelectPictureSourceRepository : NSObject

- (BOOL)saveImage:(NSData*)binaryImageData;

@end
