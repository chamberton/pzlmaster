//
//  PZLSelectPictureSourceViewProtocol.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#ifndef SelectPictureSourceViewProtocol_h
#define SelectPictureSourceViewProtocol_h

@protocol PZLSelectPictureSourceViewProtocol <NSObject>

- (void)setSelectedSegmentToNone;
- (void)showToast:(NSString *)message;

@end

#endif /* SelectPictureSourceViewProtocol_h */
