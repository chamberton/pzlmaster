//
//  PZLSelectPictureSourceViewModel.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PZLSelectPictureSourceViewProtocol.h"

@interface PZLSelectPictureSourceViewModel : NSObject

@property (readwrite) BOOL  useCamera;

- (instancetype)initWithView:(id<PZLSelectPictureSourceViewProtocol>)view;
- (UIAlertController *)createSelectionAlertWithTitle:(NSString *)title alertDetails:(NSDictionary *)info;

@end
