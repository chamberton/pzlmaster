//
//  PZLImageSource.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#ifndef PZLImageSource_h
#define PZLImageSource_h

typedef enum {
    USE_IMAGE_GALERY,
    USE_CAMERA
}PZLImageSource;

#endif /* PZLImageSource_h */
