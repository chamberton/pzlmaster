//
//  PZLSelectPictureSourceViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#import <MobileCoreServices/MobileCoreServices.h>
#import "PZLSelectPictureSourceViewController.h"
#import "PZLColorSchemeManager.h"
#import "PZLSelectPictureSourceViewModel.h"
#import "PZLCoreDataManager.h"
#import "PZLImageSource.h"
#import "iToast.h"

@interface PZLSelectPictureSourceViewController()<PZLSelectPictureSourceViewProtocol>

@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong,readwrite) PZLSelectPictureSourceViewModel* viewModel;

@end

@implementation PZLSelectPictureSourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[PZLSelectPictureSourceViewModel alloc] initWithView:self];
    self.mainview.layer.borderWidth = 8;
    [self.mainview.layer setBorderColor:(__bridge CGColorRef)([PZLColorSchemeManager getBorderColor])];
    self.view.center = CGPointMake(self.parentViewController.view.frame.size.width/2, self.view.frame.origin.y + 0.5 *self.view.frame.size.height);
    self.mainview.layer.cornerRadius = 10;
    [self.segmentedControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
}

- (void)presentImagePicker {
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType =self.viewModel.useCamera? UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)segmentControlValueChanged:(id)sender {
    
    if(self.segmentedControl!=sender){
        return;
    }
    
    self.viewModel.useCamera = self.segmentedControl.selectedSegmentIndex == USE_CAMERA;
    [self presentImagePicker];
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentAdd];
    });
}

# pragma mark - ImagePicker delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.segmentedControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [picker  dismissViewControllerAnimated:NO completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString* title = nil;
    [picker  dismissViewControllerAnimated:NO completion:nil];
    
    if(self.viewModel.useCamera){
        title = [NSString stringWithFormat:@"%@",NSLocalizedString(@"AddTakenPhoto", @"AddTakenPhoto")];
    }else{
        title = [NSString stringWithFormat:@"%@",NSLocalizedString(@"AddImageToiPZL", @"AddImageToiPZL")];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController* alertActionVC = [self.viewModel createSelectionAlertWithTitle:title alertDetails:info];
        
        [self presentViewController:alertActionVC animated:YES completion:nil];
    });
}

- (IBAction)close:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

# pragma  mark - SelectPictureSourceViewProtocol

- (void)setSelectedSegmentToNone {
    [self.segmentedControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
}

- (void)showToast:(NSString *)toastMessage {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:0.f] show:iToastTypeInfo];
    });
}

@end
