//
//  PZLBestScoreFactory.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/24.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BestScore+CoreDataProperties.h"
#import "SavedGames+CoreDataClass.h"

@interface PZLBestScoreFactory : NSObject
+ (BestScore *)buidBestGame:(SavedGames *)game;
@end
