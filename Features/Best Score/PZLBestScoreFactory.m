//
//  PZLBestScoreFactory.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/12/24.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLBestScoreFactory.h"
#import "PZLCoreDataManager.h"
#import "PZLAppDelegate.h"
@implementation PZLBestScoreFactory

+ (BestScore *)buidBestGame:(SavedGames *)game {
    
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    BestScore* bestCore = (BestScore *)[NSEntityDescription insertNewObjectForEntityForName:@"BestScore"inManagedObjectContext:moc];
    bestCore.playDate = [NSDate date];
    bestCore.score = game.gameScore;
    bestCore.duration = game.time;
    bestCore.level = game.gameLevel;
    bestCore.usedHints = game.usedHints;
    bestCore.numberOfMoves = game.numberOfMoves;
    bestCore.numberOfTiles= game.numbeOfPieces;
    bestCore.boardName = game.gamePicture;
    return bestCore;
}
@end
