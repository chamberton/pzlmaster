//
//  AboutPicturesViewProtocol.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#ifndef AboutPicturesViewProtocol_h
#define AboutPicturesViewProtocol_h

@protocol PZLAboutPicturesViewProtocol <NSObject>

@end

#endif /* AboutPicturesViewProtocol_h */
