//
//  AboutPicturesViewModel.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLAboutPicturesViewModel.h"
#import "PZLAboutPicturesRepository.h"
#import "PZLImageFactory.h"
#import "PZLImageChangeObserver.h"
#import "PZLImageObserverRegister.h"

@interface PZLAboutPicturesViewModel()<PZLImageChangeObserver>

@property (strong, nonatomic) PZLAboutPicturesRepository* repository;
@property (strong, nonatomic) id<PZLAboutPicturesViewProtocol> view;

@end

@implementation PZLAboutPicturesViewModel

- (instancetype)initWithView:(id<PZLAboutPicturesViewProtocol>)view {
    self = [super init];
    
    if (self){
        self.view = view;
        self.repository = [PZLAboutPicturesRepository new];
        self.arrayWithImages = [self.repository fecthImages];
    }
    
    return self;
}

- (NSDictionary *)cellInfo:(NSUInteger)row {
    NSMutableDictionary* dictionary = [NSMutableDictionary new];
    NSString* str = (NSString*)[self.arrayWithImages objectAtIndex:row];
    dictionary[@"cellImage"] = [PZLImageFactory imageWithName:str isPlayingMode:NO];
    dictionary[@"pictureTitle"]= NSLocalizedStringFromTable(str, @"ImageNames", nil);;
    dictionary[@"detailsText"]=  NSLocalizedStringFromTable(str, @"Gameimages", nil);
    
    return dictionary;
}

# pragma mark - ImageChangeObserver

- (void)registerAsImageChangeObserver {
    [[PZLImageObserverRegister instance] registerObserver:self];
}

- (void)updateImageList {
    self.arrayWithImages = [self.repository fecthImages];
}

@end
