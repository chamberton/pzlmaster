//
//  AboutPicturesRepository.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLAboutPicturesRepository.h"
#import "PZLCoreDataManager.h"

@interface PZLAboutPicturesRepository()

@end

@implementation PZLAboutPicturesRepository

- (NSMutableArray *)fecthImages {
    
    NSMutableArray *arrayWithImages = [NSMutableArray array];
    arrayWithImages = [NSMutableArray arrayWithArray:[PZLCoreDataManager getImagesNames]];
    
    [arrayWithImages removeObject:@"letters.jpg"];
    [arrayWithImages removeObject:@"numbers.jpg"];
    
    return arrayWithImages;
}


@end
