//
//  AboutPicturesViewModel.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PZLAboutPicturesViewProtocol.h"

@interface PZLAboutPicturesViewModel : NSObject

@property (readwrite) NSMutableArray<NSString*>* arrayWithImages;

- (instancetype)initWithView:(id<PZLAboutPicturesViewProtocol>)view;
- (NSDictionary *)cellInfo:(NSUInteger)row;

@end
