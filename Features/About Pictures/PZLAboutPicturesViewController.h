//
//  AboutPicturesViewController.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/09.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Images+CoreDataProperties.h"
#import "PZLViewController.h"
static NSString* const PictureCellIdentifier = @"PictureCell";

@interface PZLAboutPicturesViewController : PZLViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@end
