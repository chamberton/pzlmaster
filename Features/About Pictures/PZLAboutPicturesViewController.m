//
//  AboutPicturesViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/09.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLAboutPicturesViewController.h"
#import "PictureViewCell.h"
#import "PZLColorSchemeManager.h"
#import "PZLImageFactory.h"
#import "PZLAboutPicturesRepository.h"
#import "PZLAboutPicturesViewModel.h"
#import "PZLAboutPicturesViewProtocol.h"

@interface PZLAboutPicturesViewController ()<PZLAboutPicturesViewProtocol>

@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *pictureTitle;
@property (weak, nonatomic) IBOutlet UITextView *detailsText;
@property (strong, nonatomic) PZLAboutPicturesViewModel* viewModel;

@end

@implementation PZLAboutPicturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[PZLAboutPicturesViewModel alloc] initWithView:self];
    self.mainview.layer.borderWidth = 8;
    [self.mainview.layer setBorderColor:(__bridge CGColorRef)([PZLColorSchemeManager getBorderColor])];
    self.view.center = CGPointMake(self.parentViewController.view.frame.size.width/2, self.view.frame.origin.y + 0.5 *self.view.frame.size.height);
    self.mainview.layer.cornerRadius = 10;
    [self.collectionView registerClass:[PictureViewCell class] forCellWithReuseIdentifier:PictureCellIdentifier];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self presentAdd];
    });
}

- (IBAction)closeTapped:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.arrayWithImages.count;
}


-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PictureViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:PictureCellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    self.pictureTitle.textAlignment = NSTextAlignmentCenter;
    self.pictureTitle.font = [UIFont fontWithName:@"Noteworthy-Bold" size:16];
    
    NSDictionary* cellInfo = [self.viewModel cellInfo:indexPath.row];
    cell.imageView.image  =  cellInfo[@"cellImage"];
    self.pictureTitle.text = cellInfo[@"pictureTitle"];
    self.detailsText.text  = cellInfo[@"detailsText"];
    
    self.detailsText.contentOffset = CGPointMake(0, 0);
    [self.pictureTitle setTextColor:[PZLColorSchemeManager getMainTextColor]];
    cell.backgroundView.layer.cornerRadius = 10;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    PictureViewCell* pictureViewCell = (PictureViewCell*) cell;
    [pictureViewCell willBeRemovedFromCollectionView];
}

@end
