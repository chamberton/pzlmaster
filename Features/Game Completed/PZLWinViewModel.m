//
//  PZLWinViewModel.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLWinViewModel.h"
#import "PZLWinInfoEnumeration.h"
#import "PZLWinCellTitleArray.h"
#import "PZLWinViewProtocol.h"
#import "PZLGameLogic.h"

@interface PZLWinViewModel()

@property (strong,readwrite) NSMutableDictionary<NSString*, NSString*> * values;
@property (strong,readwrite) NSArray *cellTitlesArray;
@property (strong,readwrite) NSArray* keys;
@end

@implementation PZLWinViewModel

- (instancetype)initWithView:(id<PZLWinViewProtocol>)view games:(SavedGames *)savedGames {
    self = [[PZLWinViewModel alloc] initWithView:view];
    
    if (self){
        [self setValuesFromSaveGames:savedGames];
    }
    
    return self;
}

- (instancetype)initWithView:(id<PZLWinViewProtocol>)view {
    self = [super init];
    
    if(self){
        self.cellTitlesArray = [NSArray arrayWithObjects:PZLWinCellTitleArray];
        self.keys = self.cellTitlesArray;
    }
    
    return self;
}

- (void)setValuesFromSaveGames:(SavedGames *)game {
    self.values = [NSMutableDictionary dictionary];
    int seconds = [game.time intValue], numberOfMoves =[game.numberOfMoves intValue],level = [game.gameLevel intValue],
    numberOfHints = [game.usedHints intValue],numberOfTiles = [game.numbeOfPieces intValue];
    self.values[self.cellTitlesArray[Score]] = [NSString stringWithFormat:@"%d",[game.gameScore intValue]];
    self.values[self.cellTitlesArray[Used_hints]] = [NSString stringWithFormat:@"%d",numberOfHints];
    self.values[self.cellTitlesArray[Moves]] = [NSString stringWithFormat:@"%d",numberOfMoves];
    self.values[self.cellTitlesArray[Game_Duration]] = [NSString stringWithFormat:@"%d s",seconds];
    self.values[self.cellTitlesArray[Level]] = [NSString stringWithFormat:@"%@",[NSArray arrayWithObjects:levelElements][[game.gameLevel intValue]]];
    self.values[self.cellTitlesArray[Completed_per_move]] = [NSString stringWithFormat:@"%0.3f",[game.numberOfMoves floatValue]/(100-[game.startingCompletedPercentage floatValue])];
    self.values[self.cellTitlesArray[Old_rating]] = [NSString stringWithFormat:@"%d",[game.usedHints intValue]];
    self.values[self.cellTitlesArray[Current_Rating]] = [NSString stringWithFormat:@"%d",[game.savedFor.rating intValue]];
    self.values[self.cellTitlesArray[Suggested_level]] = [PZLGameLogic suggestLevel:numberOfMoves numberOfSeconds:seconds gameLevel:level numberOfTiles:numberOfTiles useHints:numberOfHints boardName:game.gamePicture];
}

- (void)winViewWasClosed {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"closeGame"
     object:self];
}

@end
