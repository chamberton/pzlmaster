//
//  PZLWinViewModel.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SavedGames+CoreDataClass.h"
#import "PZLWinViewProtocol.h"

@interface PZLWinViewModel : NSObject

@property (readwrite) BOOL soundEnabled;
@property (readonly) NSArray* keys;
@property (readonly) NSMutableDictionary<NSString*, NSString*> * values;

- (instancetype)initWithView:(id<PZLWinViewProtocol>)view games:(SavedGames *)savedGames;
- (void)setValuesFromSaveGames:(SavedGames *)game;
- (void)winViewWasClosed;

@end
