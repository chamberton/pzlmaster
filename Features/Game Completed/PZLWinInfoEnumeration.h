//
//  PZLWinInfoEnumeration.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#ifndef PZLWinInfoEnumeration_h
#define PZLWinInfoEnumeration_h

typedef enum  {
    Score,
    Moves,
    Game_Duration,
    Used_hints,
    Level,
    Completed_per_move,
    Old_rating,
    Current_Rating,
    Suggested_level
} PZLWinInfoEnumeration;

#endif /* PZLWinInfoEnumeration_h */
