//
//  PZLWinVC.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/28.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLWinVC.h"
#import "PZLWinTableViewCell.h"
#import "SoundManager.h"
#import "PZLGameLogic.h"
#import "SavedGames+CoreDataClass.h"
#import "PZLWinInfoEnumeration.h"
#import "PZLWinCellTitleArray.h"
#import "PZLWinViewModel.h"
#import <QuartzCore/QuartzCore.h>

static NSString* const PZLWinTableViewCellIdentifier=@"PZLWinTableViewCell";

@interface PZLWinVC ()<PZLWinViewProtocol>

@property (weak, nonatomic) IBOutlet UIView *statisticsView;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;
@property (readwrite,strong) NSString* mainSoundFileName;
@property (readwrite,strong) NSString* repeatSoundFileName;
@property (readwrite) BOOL viewIsVisible;

@end

@implementation PZLWinVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:( NSBundle *)nibBundleOrNil games:(SavedGames *)games {
    self = [[PZLWinVC alloc] initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self){
        self.viewModel = [[PZLWinViewModel alloc] initWithView:self games:games];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statisticsView.layer.borderWidth = 3;
    self.statisticsView.layer.cornerRadius = 5;
    self.statisticsView.layer.borderColor = [[UIColor whiteColor] CGColor];
    UINib *cellNib = [UINib nibWithNibName:@"PZLWinTableViewCell" bundle:nil];
    [self.resultsTableView registerNib:cellNib forCellReuseIdentifier:PZLWinTableViewCellIdentifier];
    self.viewIsVisible = YES;
    self.resultsTableView.dataSource = self;
    self.resultsTableView.delegate = self;
    self.mainSoundFileName = @"WinFirst.mp3";
    self.repeatSoundFileName = @"WinRepeat.mp3";
}

- (IBAction)close:(id)sender {
    [self.view removeFromSuperview ];
    [self removeFromParentViewController];
    [self.viewModel winViewWasClosed];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.keys.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PZLWinTableViewCell* cell =[self.resultsTableView dequeueReusableCellWithIdentifier:PZLWinTableViewCellIdentifier forIndexPath:indexPath];
    cell.topic.text= self.viewModel.keys[indexPath.row];
    BOOL isSuggestionCell=indexPath.row==Suggested_level;
    
    if (isSuggestionCell){
        cell.topicResult.hidden = YES;
        cell.topic.hidden = YES;
        cell.suggestionCell.hidden = NO;
        cell.suggestionCell.text = self.viewModel.values[cell.topic.text];
    }else{
        cell.topicResult.text = self.viewModel.values[cell.topic.text];
        cell.topicResult.hidden = NO;
        cell.topic.hidden = NO;
        cell.suggestionCell.hidden = YES;
    }
    
    return cell;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(self.viewModel.soundEnabled){
        Sound* mainSound = [Sound soundNamed:self.mainSoundFileName];
        
        [mainSound setCompletionHandler:^(BOOL didFinish){
            if (self.viewIsVisible){
                [[SoundManager sharedManager] playMusic:self.repeatSoundFileName looping:YES];
            }
        }];
        
        [[SoundManager sharedManager] playMusic:mainSound looping:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.viewIsVisible = NO;
    [[SoundManager sharedManager] stopMusic];
}

@end
