//
//  PZLWinVC.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/10/28.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PZLWinViewModel.h"
@class SavedGames;

@interface PZLWinVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (readwrite) PZLWinViewModel* viewModel;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:( NSBundle *)nibBundleOrNil games:(SavedGames *)games;


@end
