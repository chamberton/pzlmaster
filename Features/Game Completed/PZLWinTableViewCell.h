//
//  PZLWinTableViewCell.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/17.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PZLWinTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topicResult;
@property (weak, nonatomic) IBOutlet UILabel *topic;
@property (weak, nonatomic) IBOutlet UILabel *suggestionCell;

@end
