//
//  PZLWinTableViewCell.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/01/17.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLWinTableViewCell.h"

@implementation PZLWinTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor =[UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
