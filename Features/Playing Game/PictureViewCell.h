//
//  CollectionViewCell.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/09.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PictureViewCell : UICollectionViewCell

@property (nonatomic, strong, readwrite) UIImageView *imageView;
-(void) willBeRemovedFromCollectionView;

@end
