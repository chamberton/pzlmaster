//
//  Copyright (c) 2013 Luke Scott
//  https://github.com/lukescott/DraggableCollectionView
//  Distributed under MIT license
//
#import "PZLGameLogic.h"
#import "DraggableCollectionViewFlowLayout.h"
#import "PZLCollectionViewLayoutHelper.h"


static NSString * const CellKind = @"Cell";


@implementation DraggableCollectionViewFlowLayout
@synthesize layoutHelper= _layoutHelper;


- (LSCollectionViewLayoutHelper *)layoutHelper
{
    if(_layoutHelper == nil) {
        _layoutHelper = [[LSCollectionViewLayoutHelper alloc] initWithCollectionViewLayout:self];
    }
    return _layoutHelper;
}



- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}
- (CGSize)collectionViewContentSize
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = MIN(screenRect.size.width, self.collectionView.bounds.size.width);
    CGFloat screenHeight = MIN(screenRect.size.height- 200, self.collectionView.bounds.size.height);
    return CGSizeMake(screenHeight, screenWidth);
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    self.interItemSpacingY = 1.0f;
    self.interItemSpacingX = 1.0f;
    
    self.itemInsets = UIEdgeInsetsMake(22.0f, 22.0f, 13.0f, 22.0f);
    CGFloat width = -0.85 + (self.collectionView.bounds.size.width - ((_interItemSpacingX-1)*[PZLGameLogic numberOfItemPerRow]))/ [PZLGameLogic numberOfItemPerRow];
    CGFloat height= (self.collectionView.bounds.size.height-64*(-1.1*[PZLGameLogic numberOfItemPerRow])* ((_interItemSpacingY-1)*[PZLGameLogic numberOfItemPerColumn]))/ ([PZLGameLogic numberOfItemPerColumn]+1);
    
    self.itemSize = CGSizeMake(width, height);
    
}

#pragma mark - Private

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier,
                                                         NSDictionary *elementsInfo,
                                                         BOOL *stop) {
        [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                          UICollectionViewLayoutAttributes *attributes,
                                                          BOOL *innerStop) {
            if (CGRectIntersectsRect(rect, attributes.frame)) {
                
                [allAttributes addObject:attributes];
            }
        }];
    }];

    return [self.layoutHelper modifiedLayoutAttributesForElements:allAttributes];//[super layoutAttributesForElementsInRect:rect]];//
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.layoutInfo[CellKind][indexPath];
}

- (CGRect) cellAtIndexPath:(NSIndexPath *)indexPath {
 
    if (self.itemSize.height<=0 || self.itemSize.width<=0)
        [self setup];
    
    if (self.itemSize.height<=0 || self.itemSize.width<=0)
        @throw @"wrong item size";
    const int indexpathRow = (int)indexPath.row;
    
    CGFloat originY = indexpathRow<[PZLGameLogic numberOfItemPerRow]?0:ceil((self.itemSize.height +_interItemSpacingY)*([PZLGameLogic columnForItemAtIndex:indexPath]-1));
    
    CGFloat originX = (indexpathRow%[PZLGameLogic numberOfItemPerRow])==0?0:ceil((self.itemSize.width  +_interItemSpacingX)*[PZLGameLogic rowForItemAtIndex:indexPath])-self.itemSize.width-_interItemSpacingX;
    
    return CGRectMake( originX,originY, self.itemSize.width,self.itemSize.height);
   
}

//#pragma mark - Layout

- (void)prepareLayout {
    NSMutableDictionary *newLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    NSInteger sectionCount =  [self.collectionView numberOfSections];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    for (NSInteger section = 0; section < sectionCount; section++) {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++) {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            
            UICollectionViewLayoutAttributes *itemAttributes =
            [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self cellAtIndexPath:indexPath];
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    newLayoutInfo[CellKind] = cellLayoutInfo;
    self.layoutInfo = newLayoutInfo;
    
}

@end
