//
//  Copyright (c) 2013 Luke Scott
//  https://github.com/lukescott/DraggableCollectionView
//  Distributed under MIT license
//


#import <UIKit/UIKit.h>
#import "UICollectionView+Draggable.h"
#import "PlayerList+CoreDataClass.h"
#import "PZLViewController.h"

@interface DraggableViewController : PZLViewController <UICollectionViewDataSource_Draggable, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *soundButton;
@property (weak, nonatomic) IBOutlet UITextField *timeLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, retain) NSTimer *timer;
@property (copy, nonatomic) NSMutableDictionary* texts;
@property (nonatomic) BOOL gamePaused;
@property (strong, nonatomic) NSNumber* zero;
+(BOOL) setFromNull:(BOOL) isImage
          forlevel : (NSNumber*) level
        withPicture: (NSString*) gamePicture
      numberOfHints:  (NSNumber*) hints
    numberOfPieces : (int) numberOfPieces
         enableTmer: (BOOL) enableTimer
        enableSound: (BOOL) enableSound;
+(BOOL) setSavedGame : (id) game ;
@end
