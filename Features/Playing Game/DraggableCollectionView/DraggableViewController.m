#import <Foundation/Foundation.h>
#import "DraggableViewController.h"
#import "UIKit/UIKit.h"
#import "SavedGames+CoreDataProperties.h"
#import "Cell.h"
#import "PZLCoreDataManager.h"
#import "PZLPlayerRecordColletionViewController.h"
#import "PZLGameLogic.h"
#import "PZLUtilities.h"
#import "ShowPictureInGameVC.h"
#import "PZLWinVC.h"
#import "iToast.h"
#import "PZLImageFactory.h"
#import "SoundManager.h"
@import GoogleMobileAds;

static dispatch_queue_t timerQueue ;
static NSDate   *_dateStarted;
static NSNumber *_completedPercentage;
static NSNumber *_gameLevel;
static NSString *_gamePicture;
static NSNumber *_gameScore;
static NSNumber *_hints;
static BOOL _soundEnabled;
static NSNumber *_time;
static BOOL _timerEnabled;
static NSNumber *_numberOfPieces;
static int _secondCounts;
static BOOL _isImage;
static NSMutableDictionary* _valuesAndPositions;
static NSMutableArray* arrayWithImages;
static NSMutableArray* _cells;
static NSMutableArray* _images;
static PlayerList *_player;
static SavedGames* _currentGame;
static BOOL _isNewGame = NO;
static int ITEM_COUNT = 0;
static int ITEM_PER_ROW = 0;
static int ITEM_PER_COLUMN = 0;
static int numberOfMoves = 0;

@interface DraggableViewController ()
{
    NSString* soundFile;
}
@property (weak, nonatomic) IBOutlet UITextField *scoreTextField;
@property (weak, nonatomic) IBOutlet UIButton *pauseANDplayButton;
@property (strong,readwrite) UIImage* imageWhenNotPaused;
@property (strong,readwrite) UIImage* imageWhenPaused;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end


@implementation DraggableViewController

+(void)initialize{
    arrayWithImages= [PZLCoreDataManager getImagesNames];
}

static UIImage *_currentImage =nil;


- (IBAction)muteAndUnmute:(id)sender {
    _soundEnabled = _soundEnabled ? NO : YES;
    if(_soundEnabled){
        [_soundButton setImage: [UIImage imageNamed:@"sound"] forState: UIControlStateNormal];
        
    }else{
        [_soundButton setImage: [UIImage imageNamed:@"mute 2"] forState: UIControlStateNormal];
    }
    
}

+(BOOL) setFromNull:(BOOL) isImage
          forlevel : (NSNumber*) level
        withPicture: (NSString*) gamePicture
      numberOfHints:  (NSNumber*) hints
    numberOfPieces : (int) numberOfPieces
         enableTmer: (BOOL) enableTimer
        enableSound: (BOOL) enableSound{
    
    PZLAppDelegate *ad = (PZLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    _currentGame = (SavedGames *)[NSEntityDescription insertNewObjectForEntityForName:@"SavedGames" inManagedObjectContext:moc];;
    _isNewGame= YES;
    _cells = [NSMutableArray array];
    _currentGame.usedHints = [NSNumber numberWithUnsignedInteger:0];
    _valuesAndPositions = [NSMutableDictionary dictionary];
    _currentGame.hints = hints;
    _dateStarted   = [NSDate date];
    _gameLevel     = level;
    _gamePicture   = gamePicture;
    _gameScore     = [NSNumber numberWithInteger:0];
    _hints= hints;
    _soundEnabled  = enableSound;
    _secondCounts  = 0;
    _player        = [PZLGameLogic getPlayer];
    _isImage       = isImage;
    _timerEnabled  = enableTimer;
    count = 0;
    [PZLGameLogic configureVariable:level.intValue numberOfPieces:numberOfPieces];
    ITEM_PER_ROW    =  [PZLGameLogic numberOfItemPerRow];
    ITEM_PER_COLUMN =  [PZLGameLogic numberOfItemPerColumn];
    ITEM_COUNT      =  ITEM_PER_ROW*ITEM_PER_COLUMN;
    _numberOfPieces = [NSNumber numberWithInteger:ITEM_COUNT];
    
    if (isImage){
        
        UIImage* image =  [PZLImageFactory imageWithName:_gamePicture isPlayingMode:YES];
        if (image){
            _currentImage =image;
            _images = [PZLUtilities getImagesFromImage:_currentImage withRow:ITEM_PER_COLUMN withColumn:ITEM_PER_ROW];
        }
        else
            return NO;
    }
    
    for(int i = 0; i < ITEM_COUNT; i++) {
        gameCell* cell = [[gameCell alloc] init];
        cell.title = ([_gamePicture isEqualToString:@"letters.jpg"])?[NSString stringWithFormat:@"%c", i + ASCII_SHIFT_FOR_LETTER]:
        [NSString stringWithFormat:@"%d", i ];
        if (_isImage)
            cell.image = (UIImage*)[_images objectAtIndex:i];
        
        cell.row = [NSNumber numberWithInteger:i];
        [_cells addObject:cell];
        [_valuesAndPositions setValue:[NSNumber numberWithInteger:i]  forKey:[NSString stringWithFormat:@"%d",i]];
    }
    
    [PZLGameLogic scatter:_valuesAndPositions gameLevel:[level intValue]];
    _currentGame.startingCompletedPercentage=[NSNumber numberWithFloat:[PZLGameLogic isComplete:_valuesAndPositions]];
    numberOfMoves = 0;
    return YES;
}


+(BOOL) setSavedGame : (id) game {
    _currentGame=game;
    _isNewGame= NO;
    SavedGames* _game = (SavedGames*) game;
    _currentGame = (SavedGames*) game;
    _dateStarted = _game.dateStarted;
    _gameLevel = _game.gameLevel;
    _gamePicture = _game.gamePicture;
    _gameScore = _game.gameScore;
    _hints =_game.hints;
    _soundEnabled = [_game.soundEnabled boolValue];
    _secondCounts=[_game.time intValue];
    _player = _game.savedFor;
    _isImage = [_game.isImage boolValue];
    _cells = [NSMutableArray array];
    _valuesAndPositions = nil;
    _valuesAndPositions = [self deserialize:_game.gameboard];
    _timerEnabled   = [_game.timerEnabled boolValue];
    _numberOfPieces = _game.numbeOfPieces;
    count =0;
    [PZLGameLogic configureVariable:_gameLevel.intValue numberOfPieces:_numberOfPieces.intValue] ;
    ITEM_PER_ROW    = [PZLGameLogic numberOfItemPerRow];
    ITEM_PER_COLUMN = [PZLGameLogic numberOfItemPerColumn];
    ITEM_COUNT      = [PZLGameLogic numberOfPieces];
    
    if (ITEM_COUNT!=_numberOfPieces.intValue){
        
        return NO;
    }
    
    if (_isImage){
        
        UIImage* image = [PZLImageFactory imageWithName:_gamePicture isPlayingMode:YES];
        
        if (image){
            _currentImage =[PZLUtilities resized:image];
            _images = [PZLUtilities getImagesFromImage:_currentImage withRow:ITEM_PER_COLUMN withColumn:ITEM_PER_ROW];
        }
        else
            return NO;
    }
    
    
    for(int i = 0; i < ITEM_COUNT; i++) {
        gameCell* cell = [[gameCell alloc] init];
        
        cell.row =  (NSNumber*) [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%d",i]];
        
        if (_isImage)
            cell.image = (UIImage*)[_images objectAtIndex:[cell.row integerValue]];
        else{
            
            cell.title =([_gamePicture isEqualToString:@"letters.jpg"] ||  [_gamePicture isEqualToString:@"Letters"])?[NSString stringWithFormat:@"%c", [cell.row intValue] + ASCII_SHIFT_FOR_LETTER]:
            [NSString stringWithFormat:@"%d", [cell.row intValue] ];
        }
        [_cells addObject:cell];
        
    }
    
    numberOfMoves = [_game.numberOfMoves intValue];
    return YES;
}

- (void)setText {
    _texts = [NSMutableDictionary dictionary];
    for (NSString*  item in [_valuesAndPositions allKeys]){
        NSString* str = [NSString stringWithFormat:@"%ld",(long)((NSNumber*)[_valuesAndPositions valueForKey:item]).integerValue];
        [_texts setValue:[NSNumber numberWithInteger:item.intValue] forKey:str];
    }
}

extern BOOL shouldAppear;

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    shouldAppear = NO;
    [[SoundManager sharedManager] stopMusic];
    [PZLGameLogic reset];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(_timerEnabled==YES){
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self selector:@selector(updateTimeLabel:)
                                                userInfo:nil repeats:YES];
        
    }
    [self.bannerView loadRequest:[GADRequest request]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.bannerView.adUnitID = @"ca-app-pub-3836110455040592/8374117668";
    self.bannerView.rootViewController = self;
    
    self.imageWhenPaused =[UIImage imageNamed:@"play1"];
    self.imageWhenNotPaused =[UIImage imageNamed:@"pause"];
#pragma warning - need to active this after completion
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    if ([_gamePicture isEqualToString:@"letters.jpg"]){
        self.title = NSLocalizedString(@"Latin letters",@"Latin letters");
    }
    else if ([_gamePicture isEqualToString:@"numbers.jpg"]){
        self.title = NSLocalizedString(@"Roman numbers",@"Roman numbers");
    }else{
        self.title = NSLocalizedStringFromTable(_gamePicture, @"ImageNames", nil);
    }
    
    self.mainView.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"WallPaper"]];
    self.view.center = self.parentViewController.view.center;
    _zero = [NSNumber numberWithInteger:0];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBoardCompleted:)
                                                 name:@"closeGame"
                                               object:nil ];
    _scoreTextField.text =[NSString stringWithFormat:@"%lu",(unsigned long)_gameScore.unsignedLongValue];
    _gamePaused=NO;
    
    if(_soundEnabled){
        [_soundButton setImage: [UIImage imageNamed:@"sound"] forState: UIControlStateNormal];
    }else{
        [_soundButton setImage: [UIImage imageNamed:@"mute 2"] forState: UIControlStateNormal];
    }
    
    
}

- (void)handleBoardCompleted:(NSNotification *)note {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:nil];
    });
}


+(NSMutableDictionary *)deserialize:(NSData *)jsonData{
    return [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
}

+ (NSData *)serialize {
    return [NSJSONSerialization dataWithJSONObject:_valuesAndPositions options:NSJSONWritingPrettyPrinted error:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showImage"] && _isImage){
        ShowPictureInGameVC* vc = segue.destinationViewController;
        vc.soundEnabled=_soundEnabled;
        vc.imageToShow =_currentImage;
    }
}

- (IBAction)showHint:(id)sender {
    
    if(_currentGame.usedHints.unsignedIntegerValue==_currentGame.hints.unsignedIntegerValue){
        
        NSString* toastMessage=[[NSString alloc]initWithFormat:@"%@",NSLocalizedString(@"You have used up all your hints", @"You have used up all your hints") ];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeError];
        });
        return;
    }
    if (_soundEnabled){
        soundFile = @"showHint.wav";
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SoundManager sharedManager] playMusic:soundFile looping:NO];
        });
    }
    NSNumber* hints = [NSNumber numberWithUnsignedInteger:_currentGame.usedHints.unsignedIntegerValue + 1];
    _currentGame.usedHints=hints;
    int index = 0;
    NSNumber* correctIndex = nil;
    int attempts =0;
    while (correctIndex==nil ||  [correctIndex integerValue]==0 || [correctIndex integerValue]==index) {
        index = [PZLGameLogic getRandomNumberBetween:0 to:(int)_valuesAndPositions.count];
        correctIndex=  [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%d",index]];
        attempts++;
        if (attempts>_valuesAndPositions.count*2)
            return;
        
    }
    NSIndexPath* indexPath =[NSIndexPath indexPathForRow:index inSection:0];
    [self.collectionView.collectionViewLayout invalidateLayout];
    UICollectionViewCell *__weak cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    // Bounce
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.contentView.layer.borderWidth =8;
        cell.contentView.layer.borderColor =[UIColor greenColor].CGColor;
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.layer.borderColor =(__bridge CGColorRef _Nullable)([UIColor redColor]);
        cell.layer.borderWidth = 8;
        [UIView animateWithDuration:3.0 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveLinear animations:^{
            cell.transform = CGAffineTransformMakeScale(2,2);
            cell.transform = CGAffineTransformMakeScale(1,1);
        } completion:^(BOOL finished) {
            cell.transform = CGAffineTransformMakeScale(1,1);
            cell.contentView.layer.borderWidth =0;
            cell.contentView.layer.borderColor =[UIColor clearColor].CGColor;
        }];
    });
    indexPath =[NSIndexPath indexPathForRow:[correctIndex integerValue] inSection:0];
    cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.contentView.layer.borderWidth =8;
        cell.contentView.layer.borderColor =[UIColor redColor].CGColor;
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.layer.borderColor =(__bridge CGColorRef _Nullable)([UIColor redColor]);
        cell.layer.borderWidth = 8;
        [UIView animateWithDuration:3.0 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveLinear animations:^{
            cell.transform = CGAffineTransformMakeScale(2,2);
            cell.transform = CGAffineTransformMakeScale(1,1);
        } completion:^(BOOL finished) {
            cell.transform = CGAffineTransformMakeScale(1,1);
            cell.contentView.layer.borderWidth =0;
            cell.contentView.layer.borderColor =[UIColor clearColor].CGColor;
            [[SoundManager sharedManager] stopMusic];
        }];
    });
}

- (void)updateTimeLabel:(NSTimer *)sender {
    
    if (_timerEnabled){
        if(!_gamePaused){
            _secondCounts++;
            
            int hours   = (int) floor(_secondCounts/3600);
            int minutes = (int) floor((_secondCounts-hours*3600)/60);
            int seconds = (int) floor(_secondCounts-hours*3600-minutes*60);
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString* sH =@"";
                NSString* sM =@"";
                NSString* sS =@"";
                if(hours<10)
                    sH=@"0";
                if(minutes<10)
                    sM=@"0";
                if(seconds<10)
                    sS=@"0";
                _timeLabel.text = [NSString stringWithFormat:@"%@%d:%@%d:%@%d",sH,hours,sM,minutes, sS,seconds];
            });
        }
    }
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _valuesAndPositions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Cell *cell = (Cell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSNumber* i =[_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    if([i isEqual:_zero]){
        cell.backgroundColor = [UIColor clearColor];
        cell.label.text = @"";
    }else{
        if(!_isImage){
            cell.label.text =((gameCell*)[_cells objectAtIndex:i.integerValue]).title;
        }else{
            cell.label.text = @"";
            cell.backgroundView  = [[UIImageView alloc] initWithImage:((gameCell*)[_cells objectAtIndex:i.integerValue]).image];
        }
    }
    return cell;
}


-(BOOL) isNEIGHBORofZero:(NSInteger)location {
    
    if(location>=_valuesAndPositions.count){
        return NO;
    }
    int locationZero =  -1;
    for(NSNumber* key in [_valuesAndPositions allKeys]){
        if([[_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)key.integerValue]] isEqual:_zero]){
            locationZero = key.intValue;
            break;
        }
    }
    
    int rowZ = (locationZero%[PZLGameLogic numberOfItemPerRow]);
    int rowC = (location%[PZLGameLogic numberOfItemPerRow]);
    int colZ = floor((double)locationZero/[PZLGameLogic numberOfItemPerRow]);
    int colC = floor((double)location/[PZLGameLogic numberOfItemPerRow]);
    
    
    if (rowZ!= rowC && colC!=colZ){
        return false;
    }
    
    if (location-1>=0){
        NSNumber* neigh_1 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)location-1]];
        if([neigh_1 isEqual:_zero]){
            
            return YES;
        }
    }
    if (location+1<_valuesAndPositions.count){
        NSNumber* neigh_2 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)location+1]];
        if([neigh_2 isEqual:_zero]){
            
            return YES;
        }
    }
    if (location-ITEM_PER_ROW>=0){
        NSNumber* neigh_4 =  [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)location-ITEM_PER_ROW]];
        if([neigh_4 isEqual:_zero]){
            return YES;
        }
    }
    if (location+ITEM_PER_ROW<_valuesAndPositions.count){
        
        NSNumber* neigh_3 =  [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)location+ITEM_PER_ROW]];
        if([neigh_3 isEqual:_zero]){
            
            return YES;
        }
    }
    
    return NO;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    int locationZero =  -1;
    for(NSNumber* key in [_valuesAndPositions allKeys]){
        if([[_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)key.integerValue]] isEqual:_zero]){
            locationZero = key.intValue;
            break;
        }
    }
    NSIndexPath* indexOfZero = [NSIndexPath indexPathForRow:locationZero inSection:0];
    if([ self isNEIGHBORofZero:indexPath.row]){
        [self collectionView:self.collectionView moveItemAtIndexPath:indexPath toIndexPath:indexOfZero];
        [self.collectionView performBatchUpdates:^{
            [self.collectionView moveItemAtIndexPath:indexPath toIndexPath:indexOfZero];
            [self.collectionView moveItemAtIndexPath:indexOfZero toIndexPath:indexPath];
            
        } completion:nil];
    }
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber* neigh_1 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    if([neigh_1 isEqual:_zero]){
        return YES;
    }
    if([self isNEIGHBORofZero:indexPath.row]){
        return YES;
    }
    return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    NSNumber* neigh_1 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    if([neigh_1 isEqual:_zero]){
        return YES;
    }
    neigh_1 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)toIndexPath.row]];
    
    if([neigh_1 isEqual:_zero]){
        return YES;
    }
    return NO;
}

-(void) buildGameToSave{
    _currentGame.dateStarted = _dateStarted;
    _currentGame.dateStopped = [NSDate date];
    _currentGame.finished    = [NSNumber numberWithBool:[_completedPercentage isEqual:[NSNumber numberWithInteger:100]]];
    _currentGame.isImage     = [NSNumber numberWithBool:_isImage];
    _currentGame.gameboard   = [DraggableViewController serialize];
    _currentGame.gameLevel   = _gameLevel;
    _currentGame.gamePicture = _gamePicture;
    _currentGame.gameScore   =  [NSNumber numberWithUnsignedInteger:[PZLGameLogic score:_valuesAndPositions time:_secondCounts]];
    _currentGame.hints       = _hints;
    _currentGame.numberOfMoves = [NSNumber numberWithInt:numberOfMoves];
    _currentGame.soundEnabled     =  [NSNumber numberWithBool:_soundEnabled];
    _currentGame.time             =  [NSNumber numberWithInteger:_secondCounts];
    _currentGame.timerEnabled     =  [NSNumber numberWithBool:_timerEnabled];
    _currentGame.numbeOfPieces    = _numberOfPieces;
}

-(void) updateGame{
    _currentGame.dateStopped = [NSDate date];
    _currentGame.finished    = [NSNumber numberWithBool:[_completedPercentage isEqual:[NSNumber numberWithInteger:100]]];
    _currentGame.gameboard   = [DraggableViewController serialize];
    _currentGame.gameScore   = _gameScore;
    _currentGame.hints       = _hints;
    _currentGame.numberOfMoves = [NSNumber numberWithInt:numberOfMoves];
    _currentGame.soundEnabled     =  [NSNumber numberWithBool:_soundEnabled];
    _currentGame.time             =  [NSNumber numberWithInteger:_secondCounts];
}

- (IBAction)backClicked:(id)sender {
    [self buildGameToSave];
    [PZLCoreDataManager updateBestGameForPlayerWithName:_player.name withBestGames:_currentGame];
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Quit Game",@"Quit Game") message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* save       = [UIAlertAction actionWithTitle:NSLocalizedString(@"Save",@"Save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        if(_isNewGame){
            [self buildGameToSave];
        }else{
            [self updateGame];
        }
        
        [PZLPlayerRecordColletionViewController saveGame:_currentGame];
        [alert dismissViewControllerAnimated:NO completion:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"refreshPlayerDetails"
         object:nil];
        
    }];
    UIAlertAction* quit = [UIAlertAction actionWithTitle:NSLocalizedString(@"Quit",@"Quit") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                           {
                               [alert dismissViewControllerAnimated:NO completion:nil];
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:save];
    [alert addAction:cancel];
    [alert addAction:quit];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)pausePlay:(id)sender {
    
    if(_gamePaused) {
        _gamePaused = NO;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimeLabel:) userInfo:nil repeats:YES];
        [self.pauseANDplayButton setImage:self.imageWhenNotPaused forState:UIControlStateNormal];
        [[self.pauseANDplayButton imageView] setImage:self.imageWhenNotPaused];
    }else{
        _gamePaused = YES;
        [_timer invalidate];
        _timer = nil;
        [self.pauseANDplayButton setImage:self.imageWhenPaused forState:UIControlStateNormal];
        [[self.pauseANDplayButton imageView] setImage:self.imageWhenPaused];
    }
}

- (IBAction)showImage:(id)sender {
    if (!_isImage){
        return;
    }
}

int static count = 0;

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    if(!fromIndexPath || !toIndexPath)
        return ;
    if (fromIndexPath.row>=ITEM_COUNT || toIndexPath.row>=ITEM_COUNT)
        return ;
    if (_soundEnabled){
        soundFile = @"tileMoved.wav";
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SoundManager sharedManager] playMusic:soundFile looping:NO];
        });
    }
    if(count==0){
        NSNumber* value1 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)fromIndexPath.row]];
        NSNumber* value2 = [_valuesAndPositions valueForKey:[NSString stringWithFormat:@"%ld",(long)toIndexPath.row]];
        
        [_valuesAndPositions removeObjectsForKeys:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%ld",(long)fromIndexPath.row],[NSString stringWithFormat:@"%ld",(long)toIndexPath.row], nil]];
        [_valuesAndPositions setValue:value2 forKey:[NSString stringWithFormat:@"%ld",(long)fromIndexPath.row]];
        [_valuesAndPositions setValue:value1  forKey:[NSString stringWithFormat:@"%ld",(long)toIndexPath.row]];
        
        gameCell* obj1 = [_cells objectAtIndex:fromIndexPath.row];
        gameCell* obj2=  [_cells objectAtIndex:toIndexPath.row];
        
        obj1.row = [NSNumber numberWithInteger: toIndexPath.row ];
        obj2.row = [NSNumber numberWithInteger:  fromIndexPath.row];
        
        obj1.section = [NSNumber numberWithInteger: toIndexPath.section ];
        obj2.section = [NSNumber numberWithInteger:  fromIndexPath.section];
        count++;
    }
    
    Float32 completedPercentage = [PZLGameLogic isComplete:_valuesAndPositions];
    _completedPercentage = [NSNumber numberWithFloat:completedPercentage];
    
    if (completedPercentage == 100){
        [self showResults];
        
    }else{
        [self updateScore];
    }
    numberOfMoves++;
    if(count==1){
        count =0;
    }
    if (numberOfMoves==50){
        [self presentAdd];
    }
    [[SoundManager sharedManager] stopMusic];
}

- (void)updateScore {
    NSUInteger score =0;
    if (numberOfMoves!=0){
        score = [PZLGameLogic score:_valuesAndPositions time:_secondCounts];
    }
    _currentGame.gameScore =[NSNumber numberWithInteger:score];
    _scoreTextField.text =[NSString stringWithFormat:@"%lu",(unsigned long)score];
}

- (void)showResults {
    [self buildGameToSave];
    NSUInteger points = 0;
    if (numberOfMoves!=0){
        points =[_currentGame.gameScore integerValue] + [PZLGameLogic awardForPicture:_currentGame.gamePicture] + [PZLGameLogic awardForLevel:_currentGame.gameLevel] + [PZLGameLogic awardForNumberOfMoves:_currentGame.numberOfMoves];
    }
    [PZLCoreDataManager updateCompletedPointsForPlayer:_player.name  pointsToAdd:points];
    [PZLCoreDataManager updateBestGameForPlayerWithName:_player.name withBestGames:_currentGame];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        PZLWinVC* destinationViewController =
        [[PZLWinVC alloc] initWithNibName:@"PZLWinVC" bundle:[NSBundle mainBundle] games:_currentGame];
        destinationViewController.viewModel.soundEnabled = _soundEnabled;
        destinationViewController.view.frame = self.view.frame;
        destinationViewController.view.center = self.view.center;
        [self addChildViewController:destinationViewController];
        [self.view addSubview :destinationViewController.view];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        destinationViewController.view.transform = CGAffineTransformMakeTranslation(0, -self.view.frame.size.height);
        
        [UIView animateWithDuration:0.75 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            destinationViewController.view.transform = CGAffineTransformMakeTranslation(0, 0);
        } completion:^(BOOL finished) {
            
        }];
    });
}

-(void) dealloc{
    [_timer invalidate];
    _timer = nil;
}
@end
