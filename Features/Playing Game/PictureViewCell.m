//
//  CollectionViewCell.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/09.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PictureViewCell.h"
#import "PZLCoreDataManager.h"
@implementation PictureViewCell

-(instancetype) init {
    self = [super init ];
    if(self){
     self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];
    }
    return  self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
        
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 3.0f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowRadius = 3.0f;
        self.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        self.layer.shadowOpacity = 0.5f;
        // make sure we rasterize nicely for retina
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.layer.shouldRasterize = YES;
        
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        
        [self.contentView addSubview:self.imageView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.imageView.image = nil;
}
-(void) dealloc{
    self.imageView.image = nil;
}


-(void) willBeRemovedFromCollectionView{
    self.imageView.image =nil;
}
@end
