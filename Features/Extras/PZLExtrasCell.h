//
//  PZLExtrasCell.h
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import <UIKit/UIKit.h>
#import "PZLColorSchemeManager.h"

static NSString * const PZLExtraCellIdentifier = @"PZLExtrasCell";
@interface PZLExtrasCell : UICollectionViewCell
@property (nonatomic,retain) UIButton * button;
@end
