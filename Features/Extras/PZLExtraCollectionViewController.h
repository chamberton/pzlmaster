//
//  PZLExtraCollectionViewController.h
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import <UIKit/UIKit.h>
#import "PZLExtrasLayoutManager.h"
#import "PlayerList+CoreDataProperties.h"

@interface PZLExtraCollectionViewController : UICollectionViewController<UICollectionViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet PZLExtrasLayoutManager *layoutManager;
@end
