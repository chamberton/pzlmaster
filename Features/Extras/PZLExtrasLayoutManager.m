//
//  PZLExtrasLayoutManager.m
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import "PZLExtrasLayoutManager.h"
#import "PZLGameLogic.h"
@implementation PZLExtrasLayoutManager


#pragma mark - Lifecycle

- (id)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    self.itemInsets = UIEdgeInsetsMake(22.0f, 22.0f, 13.0f, 22.0f);
    CGFloat height;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
       height = ([UIScreen mainScreen].bounds.size.height - 250)/3;
    }else{
      height  = [UIScreen mainScreen].bounds.size.height<600?124.0f:152.0f;
    }
    self.itemSize   = CGSizeMake(124.0f, height);
    self.interItemSpacingY = 10.0f;
    self.numberOfColumns = 2;
    [self.collectionView reloadData];

}

#pragma mark - Layout

- (void)prepareLayout {
    NSMutableDictionary *newLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView numberOfSections];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    for (NSInteger section = 0; section < sectionCount; section++) {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++) {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            
            UICollectionViewLayoutAttributes *itemAttributes =
            [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self CellAtIndexPath:indexPath];
            
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    newLayoutInfo[ExtraCellKind] = cellLayoutInfo;
    self.layoutInfo = newLayoutInfo;
}

#pragma mark - Private

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier,
                                                         NSDictionary *elementsInfo,
                                                         BOOL *stop) {
        [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                          UICollectionViewLayoutAttributes *attributes,
                                                          BOOL *innerStop) {
            if (CGRectIntersectsRect(rect, attributes.frame)) {
                [allAttributes addObject:attributes];
            }
        }];
    }];
    
    return allAttributes;
}
- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.layoutInfo[ExtraCellKind][indexPath];
}

- (CGSize)collectionViewContentSize {
    NSInteger rowCount = [self.collectionView numberOfSections] / self.numberOfColumns;
    
    if ([self.collectionView numberOfSections] % self.numberOfColumns){
        rowCount++;
    }
    
    CGFloat height = self.itemInsets.top +
    rowCount * self.itemSize.height + (rowCount - 1) * self.interItemSpacingY +
    self.itemInsets.bottom;
    
    return CGSizeMake(self.collectionView.bounds.size.width, height);
}

float lastX=0;
float lastY=0;

- (CGRect)CellAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.section / self.numberOfColumns;
    NSInteger column = indexPath.section % self.numberOfColumns;
    CGFloat spacingX = self.collectionView.bounds.size.width -
    self.itemInsets.left -
    self.itemInsets.right -
    (self.numberOfColumns * self.itemSize.width);
    
    if (self.numberOfColumns > 1) spacingX = spacingX / (self.numberOfColumns - 1);
    
    CGFloat originX = floorf(self.itemInsets.left + (self.itemSize.width + spacingX) * column);
    
    CGFloat originY = floor(self.itemInsets.top +
                            (self.itemSize.height + self.interItemSpacingY) * row);
    
    if(indexPath.section==6 && lastX==0){
        
      self.itemSize = CGSizeMake(spacingX+ 250.0f,25);
      lastX =originX;
      lastY=originY;
    }
    
    if(indexPath.section==6){
        return CGRectMake(lastX, lastY, self.itemSize.width, self.itemSize.height);
    }
    else
        return CGRectMake(originX, originY, self.itemSize.width, self.itemSize.height);
}
@end
