//
//  PZLExtrasLayoutManager.h
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import <UIKit/UIKit.h>
static NSString * const ExtraCellKind = @"ExtraCell";
@interface PZLExtrasLayoutManager : UICollectionViewLayout

@property (nonatomic, strong) NSDictionary *layoutInfo;
@property (nonatomic) UIEdgeInsets itemInsets;
@property (nonatomic) CGSize itemSize;
@property (nonatomic) CGFloat interItemSpacingY;
@property (nonatomic) NSInteger numberOfColumns;
@end
