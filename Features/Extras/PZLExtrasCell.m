//
//  PZLExtrasCell.m
//  
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import "PZLExtrasCell.h"

@implementation PZLExtrasCell


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.shadowOpacity = 0.5f;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.layer.shouldRasterize = YES;
        self.backgroundColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
    }
    return self;
}

@end
