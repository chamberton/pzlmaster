//
//  PZLExtraCollectionViewController.m
//
//
//  Created by Serge Mbamba on 2016/07/01.
//
//

#import "PZLExtraCollectionViewController.h"
#import "PZLExtrasCell.h"
#import "PZLAboutViewController.h"
#import "PZLHowToPlayViewController.h"
#import "PZLAboutPicturesViewController.h"
#import "PZLCoreDataManager.h"
#import "PZLCollectionViewController.h"
#import "PZLSendFeedBackViewController.h"
#import "PZLSelectPictureSourceViewController.h"
#import "iToast.h"
#import <MobileCoreServices/MobileCoreServices.h>

static dispatch_queue_t _updatingImageQueue ;
@interface PZLExtraCollectionViewController ()

@end

@implementation PZLExtraCollectionViewController{
    NSArray* _menuList;
    BOOL     _useCamera;
}

+(void) load{
    _updatingImageQueue = dispatch_queue_create("updatingImageQueue", NULL);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _menuList= [NSArray arrayWithObjects:@"Add your own pictures",
                NSLocalizedString(@"How To play", @"How To play"),
                NSLocalizedString(@"About Pictures", @"Add your own pictures") ,
                NSLocalizedString( @"Color Schemes", @"Color Schemes") ,
                NSLocalizedString(@"Save Pictures", @"Save Pictures") ,
                NSLocalizedString(@"Support and Feedback", @"Support and Feedback") ,
                NSLocalizedString(@"Add your own pictures", @"Add your own pictures"),
                NSLocalizedString(@"About iPzL", @"About iPzL") ,
                
                nil];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.opaque = NO;
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallPaper"]];
    
    
    [self.collectionView registerClass:[PZLExtrasCell class] forCellWithReuseIdentifier:PZLExtraCellIdentifier];
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.25f alpha:1.0f];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.25f alpha:1.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 7;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0){
        PZLHowToPlayViewController* howToPlayVC = [[PZLHowToPlayViewController alloc] initWithNibName:@"PZLHowToPlayViewController" bundle:nil];
        howToPlayVC.view.frame = self.view.frame;
        [self addChildViewController:howToPlayVC];
        [self.view addSubview:howToPlayVC.view];
    }else  if(indexPath.section==1){
        
        PZLAboutPicturesViewController* aboutPicturesVC = [[PZLAboutPicturesViewController alloc] initWithNibName:@"AboutPicturesViewController" bundle:nil];
         aboutPicturesVC.view.frame = self.view.frame;
        [self addChildViewController:aboutPicturesVC];
        [self.view addSubview:aboutPicturesVC.view];
    }else if(indexPath.section==6){
        AboutViewController* aboutVC = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
         aboutVC.view.frame = self.view.frame;
        [self addChildViewController:aboutVC];
        [self.view addSubview:aboutVC.view];
    }else if(indexPath.section==2){
        if (indexPath != nil){
            UICollectionViewCell* cell =[self.collectionView cellForItemAtIndexPath:indexPath];
            [self performSegueWithIdentifier:@"savePictures" sender:cell];
        }
    }else if(indexPath.section==4){
        if (indexPath != nil){
            PZLSelectPictureSourceViewController* importPictureVC = [[PZLSelectPictureSourceViewController alloc] initWithNibName:@"PZLSelectPictureSourceViewController" bundle:nil];
             importPictureVC.view.frame = self.view.frame;
            [self addChildViewController:importPictureVC];
            [self.view addSubview:importPictureVC.view];
        }
    }else {
        PZLSendFeedBackViewController* sendFeedBack = [[PZLSendFeedBackViewController alloc] initWithNibName:@"SendFeedBackViewController" bundle:nil];
         sendFeedBack.view.frame = self.view.frame;
        [self addChildViewController:sendFeedBack];
        [self.view addSubview:sendFeedBack.view];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    PZLCollectionViewController *navigationController = segue.destinationViewController;
    navigationController.mutipleSelectionAllowed  = YES;
}

- (void) presentImagePicker{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        if(_useCamera) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            
            picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
        
        [self presentViewController:picker animated:YES completion:nil];
    });
}

-(void) getPhoto {
    NSString* text  = NSLocalizedString(@"Select Image Source",@"Select Image Source");
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:text
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert.view setTintColor:[PZLColorSchemeManager getMainSubtextColor]];
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:alert.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.view.frame.size.height*0.8f];
    [alert.view addConstraint:constraint];
    
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : [PZLColorSchemeManager getMainTextColor] };
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"Noteworthy-Bold" size:18]
                  range:NSMakeRange(0, [text length])];
    
    
    [alert setValue:hogan forKey:@"attributedTitle"];
    UIAlertAction* image = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"Camera",@"Camera")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                _useCamera = YES;
                                [alert dismissViewControllerAnimated:YES completion:nil];
                                [self presentImagePicker];
                                
                            }];
    UIAlertAction* galerie = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Galerie",@"Galerie")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  _useCamera = NO;
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self presentImagePicker];
                              }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Close",@"Close")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alert addAction:image];
    [alert addAction:galerie];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = info [UIImagePickerControllerMediaType];
    NSDate* myNSDateInstance =[NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSString *stringFromDate = [formatter stringFromDate:myNSDateInstance];
    stringFromDate=[stringFromDate stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
    stringFromDate=[stringFromDate stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    stringFromDate=[stringFromDate stringByReplacingOccurrencesOfString:@":" withString:@"_"];
    stringFromDate=[stringFromDate stringByAppendingString:@".png"];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSString* title = nil;
    if(_useCamera){
        title=  [NSString stringWithFormat:@"%@",NSLocalizedString(@"AddTakenPhoto", @"AddTakenPhoto")];
    }else{
        title=  [NSString stringWithFormat:@"%@",NSLocalizedString(@"AddImageToiPZL", @"AddImageToiPZL")];
    }
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"OK",@"OK")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             dispatch_async(_updatingImageQueue, ^{
                                 if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
                                 {
                                     
                                     NSURL *imagePickerURL = [info objectForKey:UIImagePickerControllerMediaURL];
                                     if(imagePickerURL){
                                         NSURL *fileUrl;
                                         fileUrl = [NSURL fileURLWithPath:[imagePickerURL path]];
                                     }else{
                                         UIImage* imageToSave = info[UIImagePickerControllerOriginalImage];
                                         NSData * binaryImageData = UIImagePNGRepresentation(imageToSave);
                                         if(![PZLCoreDataManager imageWithDataExist:binaryImageData]){
                                             NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                             if([binaryImageData writeToFile:[documentsDirectory stringByAppendingPathComponent:stringFromDate] atomically:YES]){
                                                 if([PZLCoreDataManager addImageName:stringFromDate enableErrorNotification:NO]){
                                                     NSString* toastMessage=[NSString stringWithFormat:@"%@",NSLocalizedString(@"ImageAddedToiPzL", @"ImageAddedToiPzL") ];
                                                     
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:0.f] show:iToastTypeInfo];
                                                     });
                                                     
                                                 }
                                             }
                                             
                                         }else{
                                             
                                             NSString* toastMessage=[NSString stringWithFormat:@"%@",NSLocalizedString(@"ImageAlreadyExist", @"ImageAlreadyExist") ];
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 [[[[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] setUseShadow:YES] setBgRed:255.f] show:iToastTypeInfo];
                                             });
                                             
                                             
                                         }
                                     }
                                     
                                 }
                                 else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
                                 {
                                     
                                     NSURL *imagePickerURL = [info objectForKey:UIImagePickerControllerMediaURL];
                                     NSURL *fileUrl;
                                     fileUrl = [NSURL fileURLWithPath:[imagePickerURL path]];
                                 }
                             });
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PZLExtrasCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PZLExtraCellIdentifier forIndexPath:indexPath];
    cell.layer.cornerRadius = 10;
    UIImage *bckImage = nil;
    CGRect frame =  CGRectMake(cell.bounds.origin.x, cell.bounds.origin.x, cell.frame.size.height*0.75, cell.frame.size.width*0.75);
    UIImageView *imageView = nil;
    
    switch (indexPath.section) {
            case 0:
            bckImage = [UIImage imageNamed:@"how to play"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
            case 1:
            bckImage = [UIImage imageNamed:@"images"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
            case 2:
            bckImage = [UIImage imageNamed:@"import"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
            case 3:
            bckImage = [UIImage imageNamed:@"feedback"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
            case 4:
            bckImage = [UIImage imageNamed:@"export"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
            case 5:
            bckImage = [UIImage imageNamed:@"feedback"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
            case 6:
            bckImage = [UIImage imageNamed:@"feedback"];
            imageView = [[UIImageView alloc] initWithFrame:frame];
            imageView.image = bckImage;
            [cell setBackgroundView:imageView];
            break;
        default:
            break;
    }
    if(indexPath.section==6){
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:cell.frame];
        nameLabel.text = NSLocalizedString(@"About iPzL",@"About iPzL");
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [nameLabel  setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:18]];
        [nameLabel setTextColor:[PZLColorSchemeManager getMainTextColor]];
        [cell setBackgroundView:nameLabel];
    }
    
    if(indexPath.section==3){
        cell.hidden = YES;
        cell.userInteractionEnabled = NO;
    }
    
    if(indexPath.section==2){
        cell.center = CGPointMake(self.view.frame.size.width/2,cell.center.y);
    }
    if(indexPath.section!=6){
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
