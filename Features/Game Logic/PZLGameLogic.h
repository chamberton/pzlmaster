//
//  PZLGameLogic.h
//  Pzl
//
//  Created by Serge on 2016/08/08.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSNumber+Move.h"
#import "PlayerList+CoreDataProperties.h"

@interface gameCell : NSObject

@property (strong)  NSString *title;
@property (strong)  UIImage  *image;
@property (strong)  NSNumber *row;
@property (strong)  NSNumber *section;

@end

@interface Position : NSObject

@property (nonatomic) int key;
@property (nonatomic) int value;

@end


@interface PZLGameLogic : NSObject


+ (int)numberOfContinentItems;
+ (int)getRandomNumberBetween:(int)from to:(int)to ;
+ (void)scatter:(NSMutableDictionary *) array gameLevel:(int)level;

+ (void) configureVariable: (int)level numberOfPieces: (int)number;

+ (int) numberOfItemPerRow ;

+ (void) reset;

+ (int)getCompletedGamesPoints:(NSUInteger)numberOfPieces level:(GameLevel)level timeTake:(NSUInteger)time;

+ (int)numberOfItemPerColumn;

+ (int)numberOfPieces;
+ (int)calculateDispersion: (NSDictionary*) array;

+ (int) computeMinDispersion: (int) level;
+ (int) computeMaxDispersion: (int) level;
+ (int) rowForItemAtIndex : (NSIndexPath*) path;
+ (int) columnForItemAtIndex : (NSIndexPath*) path;
+ (int) rowForPosition : (int) path;
+ (int) columnForPosition : (int) path;
+ (NSArray *) boardSizes;
+ (Float32)isComplete:(NSDictionary *)dictionary ;
+ (NSUInteger) score:(NSDictionary *)dictionary time:(NSUInteger)elapsedSeconds;
+ (void) setPlayer:(PlayerList *)player;
+ (PlayerList *)getPlayer;
+ (NSString*)suggestLevel:(NSInteger)numberOfMoves numberOfSeconds:(NSInteger)seconds gameLevel:(NSInteger)level numberOfTiles:(NSInteger)numberOfTiles useHints:(NSInteger)useHints boardName:(NSString *)gameName;
+ (NSInteger)awardForPicture:(NSString *)gamePicture;
+ (NSUInteger)awardForLevel:(NSNumber *)gameLevel;
+ (NSUInteger)awardForNumberOfMoves:(NSNumber *)numberOfMoves;
@end
