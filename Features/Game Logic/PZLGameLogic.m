#import <Foundation/Foundation.h>
#import "PZLGameLogic.h"
#import "PlayerList+CoreDataClass.h"

@implementation gameCell

@synthesize title=_title, image=_image;

@end

@implementation Position : NSObject

@synthesize key=_key, value=_value;

@end

@implementation PZLGameLogic


int    NUMBER_OF_VALID_NUMBER_OF_PIECES = 0;
static int* valideNumberOfPieces;
static  int maxNumberOfItemPerRow =  0;
static  int maxNumberOfItemPerColumn =0;
static int arrayOfItemsPerRow []= {4,4,5,5,6,6};
static int arrayOfItemsPerColumn[] ={4,5,5,6,6,7};
static BOOL first  = true;
static int* northLimits = NULL ;
static int* westLimits = NULL;
static int*  eastLimits = NULL ;
static int* southLimits = NULL;
static int item_per_row=0;
static int item_per_column= 0;
static int  number_of_items=0;
static Location lastMove = East;
static NSNumber * _emptyNow;
static Location  _directionToMove;
static NSArray* _objectsToMove;
static int _numberOfObjectsToMove;
static NSArray*  _arrayOfMinDispersion ;
static NSArray* _arrayOfMaxDispersion ;
static NSArray* _arrayOfMinRounds ;
static NSArray* _arrayOfMaxRounds ;
static PlayerList* currentPlayer;

const int dispersionLevelMinbeginner =  200;
const int dispersionLevelMaxbeginner =  400;
const int numberOfRoundMinbeginner   =   10;
const int numberOfRoundMaxbeginner   =   20;

const int dispersionLevelMinIntermediate =  400;
const int dispersionLevelMaxIntermediate =  800;
const int numberOfRoundMinIntermediate   =   15;
const int numberOfRoundMaxIntermediate   =   25;

const int dispersionLevelMinAdvanced =    800;
const int dispersionLevelMaxAdvanced =   1200;
const int numberOfRoundMinAdvanced   =     20;
const int numberOfRoundMaxAdvanced   =     35;

const int dispersionLevelMinExpert =   2000;
const int dispersionLevelMaxExpert =    200;
const int numberOfRoundMinExpert   =     40;
const int numberOfRoundMaxExpert   =    100;


static unsigned long savedSeconds=0;

+ (int)numberOfContinentItems {
    return (int)[NSArray arrayWithObjects:ContinentNameArray].count;
}

+ (void)load {
    direction = @[@"North",@"South",@"East",@"West"];
    
    CGRect  screenRect   = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth  = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    
    if (screenWidth>=375){
        maxNumberOfItemPerRow = 6;
    }else{
        maxNumberOfItemPerRow  = 5;
    }
    
    if (screenHeight>=660){
        maxNumberOfItemPerColumn = 7;
    }else{
        maxNumberOfItemPerColumn  = 6;
    }
    
    valideNumberOfPieces=(int*) malloc(sizeof(int)*NUMBER_OF_ENTRIES_FOR_SIZES);
    
    for (int i=0; i<NUMBER_OF_ENTRIES_FOR_SIZES; i++){
        valideNumberOfPieces[i]=arrayOfItemsPerRow[i]*arrayOfItemsPerColumn[i];
    }
    float multiplier = 1;
    if(number_of_items>30){
        multiplier = 1.75;
    }
    
    _arrayOfMinDispersion = [NSArray arrayWithObjects:[NSNumber numberWithInteger:dispersionLevelMinbeginner*multiplier],
                             [NSNumber numberWithInteger:dispersionLevelMinIntermediate],
                             [NSNumber numberWithInteger:dispersionLevelMinAdvanced],
                             [NSNumber numberWithInteger:dispersionLevelMinExpert],
                             nil];
    
    _arrayOfMaxDispersion = [NSArray arrayWithObjects:[NSNumber numberWithInteger:dispersionLevelMaxbeginner*multiplier],
                             [NSNumber numberWithInteger:dispersionLevelMaxIntermediate],
                             [NSNumber numberWithInteger:dispersionLevelMaxAdvanced],
                             [NSNumber numberWithInteger:dispersionLevelMaxExpert],
                             nil];
    
    _arrayOfMinRounds = [NSArray arrayWithObjects:[NSNumber numberWithInteger:numberOfRoundMinbeginner*multiplier],
                            [NSNumber numberWithInteger:numberOfRoundMinIntermediate],
                            [NSNumber numberWithInteger:numberOfRoundMinAdvanced],
                            [NSNumber numberWithInteger:numberOfRoundMinExpert],
                            nil];
    
    _arrayOfMaxRounds =    [NSArray arrayWithObjects:[NSNumber numberWithInteger:numberOfRoundMaxbeginner*multiplier],
                            [NSNumber numberWithInteger:numberOfRoundMaxIntermediate],
                            [NSNumber numberWithInteger:numberOfRoundMaxAdvanced],
                            [NSNumber numberWithInteger:numberOfRoundMaxExpert],
                            nil];
    first = YES;
}

int highestIndexOf(int value,int* array,int size){
    int index = -1;
    for (int i=0; i<size; i++){
        if (array[i]==value)
            index=i;
    }
    return index;
}

+ (NSArray *)boardSizes {
    int maxIndex = MAX(highestIndexOf(maxNumberOfItemPerRow,arrayOfItemsPerRow,NUMBER_OF_ENTRIES_FOR_SIZES),
                       highestIndexOf(maxNumberOfItemPerColumn,arrayOfItemsPerColumn,NUMBER_OF_ENTRIES_FOR_SIZES));
    
    NSMutableArray* array =[NSMutableArray array];
    int index=0;
    while(index<=maxIndex){
        [array addObject:[NSString stringWithFormat:@"%d",arrayOfItemsPerRow[index]*arrayOfItemsPerColumn[index]]];
        index++;
    }
    return array ;
}

+ (NSArray *)getObjectsAt:(Location) directionGoTo  of:(NSNumber *) reference In:(NSDictionary *)arrayOfObjects NumberOfObjectToGet:(int)count {
    
    NSMutableArray* array  = [NSMutableArray array];
    int referenceIntVal = [reference intValue];
    int index  = 0;
restart:
    if (directionGoTo==East ||directionGoTo==West){
        index = [PZLGameLogic columnForPosition:referenceIntVal];
        
    }
    if (directionGoTo==North || directionGoTo==South){
        index = [PZLGameLogic rowForPosition:referenceIntVal];
        
    }
    int limit = 0;
    
    
    if (((directionGoTo==East || directionGoTo==West) && index>=item_per_column )
        || ((directionGoTo==North || directionGoTo==South) && index>=item_per_row ) ){
        @throw [NSException exceptionWithName:@"index exceed rows or columns" reason:@"Over boundaries" userInfo:nil];
    }
    
    while (referenceIntVal>=0 && referenceIntVal<arrayOfObjects.count){
        switch (directionGoTo) {
            case East:
                limit  = eastLimits[index];
                referenceIntVal++;
                break;
            case West:
                limit  = westLimits[index];
                referenceIntVal--;
                break;
            case North:
                limit  = northLimits[index];
                referenceIntVal-=item_per_row;
                break;
            case South:
                limit  = southLimits[index];
                referenceIntVal+=item_per_row;
                break;
            default:
                @throw [NSException exceptionWithName:@"Direction not known" reason:@"A direction to move must be known" userInfo:nil];
                break;
                
        }
        if (directionGoTo==East || directionGoTo==South){
            if(referenceIntVal>limit){
                return [array copy];
                ;
            }
        }
        if (directionGoTo==West ||  directionGoTo==North){
            if(referenceIntVal<limit){
                return [array copy];
                ;
            }
        }
        if(referenceIntVal<0 || referenceIntVal>=arrayOfObjects.count){
            return [array copy];
            ;
        }
        Position *obj = [[Position alloc] init];
        obj.key   =[(NSNumber*)[arrayOfObjects objectForKey:[NSString stringWithFormat:@"%d",referenceIntVal]] intValue];
        obj.value =referenceIntVal ;
        [array addObject:obj];
        if(array.count==count){
            break;
        }
        
    }
    if (array.count==0)
    {
        goto restart;
    }
    return [array copy];
    
    
}

+ (int)computeMinDispersion:(int)level {
    return [(NSNumber*)[_arrayOfMinDispersion objectAtIndex:level] intValue] *((double)number_of_items/16);
}

+ (int)computeMaxDispersion:(int)level {
    return  [(NSNumber*)[_arrayOfMaxDispersion objectAtIndex:level] intValue]*((double)number_of_items/16);
}

+ (int)computeMinMoves:(int)level{
    return  [(NSNumber*)[_arrayOfMinRounds objectAtIndex:level] intValue]*((double)number_of_items/16);
}

+ (int)computeMaxMoves:(int)level{
    return  [(NSNumber*)[_arrayOfMaxRounds objectAtIndex:level] intValue]*((double)number_of_items/16);
}

+ (Location)oppositeDirection:(Location)direction {
    switch (direction) {
        case East:
            return West;
            break;
        case West:
            return East;
        case North:
            return South;
        case South:
            return  North;
            break;
        default:
            @throw @"Wrong direction";
            break;
    }
}

+ (void)scatter:(NSMutableDictionary *)array gameLevel:(int)level {
    
    if(array==nil || array.count<3)
        return ;
    
    const int levelMinDispersion     = [PZLGameLogic computeMinDispersion:level];
    const int levelMaxDispersion     = [PZLGameLogic computeMaxDispersion:level];
    const int levelMaxNumberOfRounds = [ PZLGameLogic computeMaxMoves:level];
    const int levelMinNumberOfRounds = [ PZLGameLogic computeMinMoves:level];
    
    int roundNumber =0;
    int dispersionlevel =[PZLGameLogic calculateDispersion:array];
Disperse:
    while(dispersionlevel<levelMinDispersion || roundNumber<levelMinNumberOfRounds){
        roundNumber++;
        
        [PZLGameLogic buildMoves:array];
        
        if ([_objectsToMove count]>0){
            
            [array removeObjectForKey:[NSString stringWithFormat:@"%d",_emptyNow.intValue]];
            for (Position* pos in _objectsToMove){
                [array removeObjectForKey:[NSString stringWithFormat:@"%d",pos.value]];
            }
            
            for (Position* pos in _objectsToMove){
                NSNumber* obj = [NSNumber numberWithInt:pos.value];
                Location opposite =[PZLGameLogic oppositeDirection:_directionToMove];
                NSNumber * newPos = [obj movedTo:opposite  itemPerRow:item_per_row];
                if (newPos!=nil){
                    _emptyNow = [_emptyNow movedTo:_directionToMove  itemPerRow:item_per_row];
                    [array setValue:[NSNumber numberWithInteger:pos.key] forKey:[NSString stringWithFormat:@"%d", newPos.intValue ]];
                }
                
            }
            [array setValue:[NSNumber numberWithInt:0] forKey:[NSString stringWithFormat:@"%d",_emptyNow.intValue]];
            
            
            dispersionlevel = [PZLGameLogic calculateDispersion:array];
        }
        if(dispersionlevel>levelMaxDispersion || roundNumber>levelMaxNumberOfRounds){
            return;
        }
    }
    
}

+ (int)getRandomNumberBetween:(int)from to:(int)to {
    usleep(500); // increase randomness
    return (int)from + arc4random() % (to-from);
}

+ (int)getCompletedGamesPoints:(NSUInteger)numberOfPieces level:(GameLevel)level timeTake:(NSUInteger)numberOfSeconds {
    double time = 0.2*numberOfSeconds<1?1:0.2*numberOfSeconds;
    
    return (level+1)*numberOfPieces/time*0.5;
}

+ (int)getDirectionToMove {
    float low_bound  = 0;
    float high_bound = 4;
    usleep(250);
    return  [PZLGameLogic getRandomNumberBetween:low_bound to:high_bound];
}

+ (int)getNumberOfCellToMove:(int)maxNumber {
    return [PZLGameLogic getRandomNumberBetween:1 to:maxNumber];
}

+ (void)buildMoves:(NSDictionary *)array {
    
    BOOL found = NO;
    NSArray* keys = [array allKeys];
    
    for (NSString* item in keys){
        if(((NSNumber*)[array valueForKey:item]).intValue==0){
            _emptyNow = [NSNumber numberWithInteger:item.intValue];
            found = YES;
            break;
        }
    }
    
    if (!found){
        _objectsToMove = nil;
        _numberOfObjectsToMove= 0;
        @throw [NSException exceptionWithName:@"Zero not found" reason:@"Cannot miss zero" userInfo:nil];
    }
    if (item_per_row==0)
        @throw[NSException exceptionWithName:@"Number of item per row is invalid" reason:@"Cannot miss zero" userInfo:nil];  ;
    
    int neighbourCount=0;
    NSSet* setDirection = [NSSet set];
    int location = [_emptyNow intValue] ;
    _objectsToMove = [NSArray array];
    int row  = [PZLGameLogic rowForPosition:location];
    int column = [PZLGameLogic columnForPosition:location];
    
    if (westLimits[column]<location){
        neighbourCount++;
        setDirection = [setDirection setByAddingObject:[NSNumber numberWithInteger:West]];
    }
    if (eastLimits[column]>location){
        neighbourCount++;
        setDirection = [setDirection setByAddingObject:[NSNumber numberWithInteger:East]];
    }
    if (southLimits[row]>location){
        neighbourCount++;
        setDirection = [setDirection setByAddingObject:[NSNumber numberWithInteger:South]];
    }
    if (northLimits[row]<location){
        neighbourCount++;
        setDirection = [setDirection setByAddingObject:[NSNumber numberWithInteger:North]];
    }
    
    if (first){
        while (![setDirection containsObject:[NSNumber numberWithInteger:_directionToMove]] ||
               (first && (_directionToMove==lastMove)) ||
               _directionToMove==[PZLGameLogic oppositeDirection:lastMove]) {
            _directionToMove= [PZLGameLogic getDirectionToMove];
            first = NO;
        }
    }else{
        while (![setDirection containsObject:[NSNumber numberWithInteger:_directionToMove]] ||
               _directionToMove==lastMove) {
            _directionToMove= [PZLGameLogic getDirectionToMove];
            first = NO;
        }
    }
    lastMove  = _directionToMove;
    while(_numberOfObjectsToMove<=0){
        _numberOfObjectsToMove= (int)[PZLGameLogic getNumberOfCellToMove:MAX(item_per_column, item_per_row)] ;
    }
    
    while(_objectsToMove.count<=0){
        
        _objectsToMove = [PZLGameLogic getObjectsAt:_directionToMove of:_emptyNow In:array NumberOfObjectToGet:_numberOfObjectsToMove];
        
        if(_objectsToMove==nil  || _objectsToMove.count==0){
            @throw [NSException exceptionWithName:@"Could not find neighbour"
                                           reason:@"The neight must always be present"
                                         userInfo:nil];
        }
    }
    _numberOfObjectsToMove = 0;
    
}

+ (Float32)isComplete:(NSDictionary *)dictionary {
    
    NSArray* keys = [dictionary allKeys];
    Float32 unordered = 0;
    for (NSString* item in keys){
        if (item.integerValue!=[(NSNumber*)[dictionary valueForKey:item] integerValue]){
            unordered++;
        }
    }
    return (unordered==0)?100:(100 - unordered*100/dictionary.count);
}


+ (int)calculateDispersion:(NSDictionary*)array {
    
    if (array==nil || array.count==0)
        return 0;
    
    double dispersionLevel = 0;
    NSArray* keys = [array allKeys];
    for (NSString*  item in keys){
        
        int location = [(NSNumber*)[array valueForKey:item] intValue] ;
        if (location-1>=0){
            NSNumber* neigh_1 = [array valueForKey:[NSString stringWithFormat:@"%ld",(long)location-1]];
            dispersionLevel+= 0.5*(double)pow(abs( location - [neigh_1 intValue])-1,2);
        }
        if (location+1<array.count){
            NSNumber* neigh_2 = [array valueForKey:[NSString stringWithFormat:@"%ld",(long)location+1]];
            dispersionLevel+= 0.5*(double)pow(abs( location - [neigh_2 intValue])-1,2);
            
        }
        if (location-item_per_row>=0){
            NSNumber* neigh_4 =  [array valueForKey:[NSString stringWithFormat:@"%ld",(long)location-item_per_row]];
            dispersionLevel+= 0.5*(int)pow((fabs( (double)location - [neigh_4 intValue])-item_per_row)/item_per_row,2);
        }
        
        if (location+item_per_row<array.count){
            NSNumber* neigh_3 =  [array valueForKey:[NSString stringWithFormat:@"%ld",(long)location+item_per_row]];
            dispersionLevel+= 0.5*(double)pow((fabs( (double)location - [neigh_3 intValue])-item_per_row)/item_per_row,2);
        }
        
    }
    return dispersionLevel;
}

bool validateNumberOfItems(int number, int* index){
    
    unsigned short i = 0;
    for ( i = 0 ; i < NUMBER_OF_VALID_NUMBER_OF_PIECES; i++){
        if (valideNumberOfPieces[i]==number){
            *index =i;
            return true;
        }
    }
    return false;
}


+ (double)percentageCompleted:(NSDictionary *)dictionary {
    double total = dictionary.count;
    double completd = 0;
    
    for (NSString* item in [dictionary allKeys]){
        if (item.integerValue==[(NSNumber*)[dictionary valueForKey:item] integerValue]){
            completd++;
        }
    }
    return completd/total;
}

+ (void)setPlayer:(PlayerList *)player {
    currentPlayer = player;
}

+ (PlayerList *)getPlayer {
    return currentPlayer;
}

+ (NSUInteger)score:(NSDictionary *)dictionary time:(NSUInteger)elapsedSeconds {
    
    double percentageCompleted = [self percentageCompleted:dictionary];
    double diffSeconds = elapsedSeconds;
    savedSeconds = diffSeconds;
    if(savedSeconds>500){
        diffSeconds = 500;
    }
    NSInteger rawResult = percentageCompleted*350 - (diffSeconds) + (dictionary.count*5);
    if (rawResult<0){
        return 0;
    }
    return rawResult;
}

+ (void)configureVariable:(int)level numberOfPieces:(int)number{
    savedSeconds =0;
    int index = 0;
    NUMBER_OF_VALID_NUMBER_OF_PIECES = number;
    
    if (validateNumberOfItems(number,&index)){
        number_of_items = number;
        item_per_row    = arrayOfItemsPerRow[index];
        item_per_column = arrayOfItemsPerColumn[index];
    }else{
        @throw [NSException exceptionWithName:@"Invalid number of pieces" reason:@"Need to validate number of piece" userInfo:nil];
    }
    northLimits = (int*) malloc(item_per_row*sizeof(int));
    southLimits = (int*) malloc(item_per_row*sizeof(int));
    eastLimits  = (int*) malloc(item_per_column*sizeof(int));
    westLimits  = (int*) malloc(item_per_column*sizeof(int));
    
    int i =0;
    for (i=0; i<item_per_row;i++){
        northLimits[i] = i;
        southLimits[i] = northLimits[i] + item_per_row*(item_per_column-1);
    }
    for (i=0; i<item_per_column;i++){
        eastLimits[i] = (i+1)*item_per_row-1;
        westLimits[i] = eastLimits[i]-item_per_row+1;
    }
    number_of_items=item_per_row*item_per_column;
}

+ (int) numberOfItemPerRow {
    return item_per_row;
}

+ (int) numberOfItemPerColumn{
    return item_per_column;
}

+ (int) numberOfPieces{
    return number_of_items;
}

+ (void)reset {
    first = YES;
}

+ (int)rowForItemAtIndex:(NSIndexPath *)path {
    return abs((int)path.row % item_per_row)+1;
}

+ (int)columnForItemAtIndex:(NSIndexPath *)path {
    return ceil(fabs((float)(path.row+1)/item_per_row));
}

+ (int)rowForPosition:(int)path {
    return abs(path % item_per_row);
}

+ (int)columnForPosition:(int)path {
    return floor(fabs((float)(path)/item_per_row));
}

+ (NSString*)suggestLevel:(NSInteger)numberOfMoves numberOfSeconds:(NSInteger)seconds gameLevel:(NSInteger)level numberOfTiles:(NSInteger)numberOfTiles useHints:(NSInteger)useHints boardName:(NSString *)gameName {
    
    if([gameName containsString:@"letters"] || [gameName containsString:@"numbers"] ){
        return NSLocalizedString(@"Reduce the number of hints", @"Increase number of tiles");
    }    
    if(useHints>40){
        return NSLocalizedString(@"Reduce the number of hints", @"Increase number of tiles");
    }else{
        if(useHints>10){
            return NSLocalizedString(@"Disable hints", @"Disable hints");
        }
    }
    
    if (numberOfTiles<36){
        return NSLocalizedString(@"Increase number of tiles", @"Increase number of tiles");
    }
    if (seconds>1200){
        return NSLocalizedString(@"Enable timer", @"Enable timer");
    }
    
    switch (level) {
        case Expert:
            return NSLocalizedString(@"Use a harder picture", @"Use a harder picture");
        case Advanced:
            if(useHints>10){
                return NSLocalizedString(@"Disable hints", @"Increase number of tiles");
            }
            return NSLocalizedString(@"Try Expert level", @"Try Expert level");
        case Intermediate:
            if (seconds>500){
                return NSLocalizedString(@"Enable timer", @"Enable timer");
            }else{
                if(useHints>10){
                    return NSLocalizedString(@"Disable hints", @"Increase number of tiles");
                }
                return NSLocalizedString(@"Try Advanced level", @"Try Advanced level");
            }
        case Beginner:
            if (seconds>500){
                return NSLocalizedString(@"Enable timer", @"Enable timer");
            }else{
                if(useHints!=0){
                    return NSLocalizedString(@"Disable hints", @"Increase number of tiles");
                }
                return NSLocalizedString(@"Try Intermediate level", @"Try Intermediate level");
            }
            
    }
    return  NSLocalizedString(@"Use a harder picture", @"Use a harder picture");
}
+ (NSUInteger)awardForLevel:(NSNumber *)gameLevel {
    return [gameLevel unsignedIntegerValue]*5;
}
+ (NSUInteger)awardForNumberOfMoves:(NSNumber *)numberOfMoves {
    if (numberOfMoves==0){
        return 0;
    }
    else{
        return MAX(200 - [numberOfMoves integerValue],0);
    }
}


+ (NSInteger)awardForPicture:(NSString *)gamePicture {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Images" withExtension:@"plist"];
    NSDictionary *arrayOfImagesDescription = [[NSDictionary alloc ] initWithContentsOfURL:url];
    
    NSDictionary* objectInfo = arrayOfImagesDescription[gamePicture];
    if(objectInfo && ![objectInfo isKindOfClass:[NSNull class]]){
        NSNumber* awards=[objectInfo objectForKey:@"AwardOnCompletion"];
        if(awards && ![awards isKindOfClass:[NSNull class]]){
            return awards.integerValue;
        }
    }
    return 0;
}
@end
