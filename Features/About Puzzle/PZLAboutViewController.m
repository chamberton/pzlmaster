//
//  AboutViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/08.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLAboutViewController.h"
#import "PZLColorSchemeManager.h"

@interface AboutViewController ()

@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UILabel *version;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation AboutViewController

- (IBAction)OkTapped:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.center = CGPointMake(self.parentViewController.view.frame.size.width/2, self.view.frame.origin.y + 0.5 *self.view.frame.size.height);
    self.mainview.layer.cornerRadius = 10;
    self.mainview.layer.borderWidth = 2;
    self.mainview.layer.borderColor= (__bridge CGColorRef)([PZLColorSchemeManager getMainTextColor]);
    self.textView.text = NSLocalizedString(@"About", @"About");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *buildVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.version.text =[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Version", @"Version"),buildVersion];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.textView.contentOffset = CGPointZero;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentAdd];
    });
}
@end
