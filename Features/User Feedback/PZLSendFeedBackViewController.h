//
//  PZLSendFeedBackViewController.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/16.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PZLColorSchemeManager.h"
#import "PZLViewController.h"

@interface PZLSendFeedBackViewController : PZLViewController<UITextFieldDelegate, UITextViewDelegate>

@end
