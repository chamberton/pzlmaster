//
//  PZLSendMailFlowAction.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLSendMailFlowAction.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface PZLSendMailFlowAction()<MFMailComposeViewControllerDelegate>

@property(weak,readwrite)UIViewController *rootViewController;
@property (strong, nonatomic) MFMailComposeViewController *mailCont;

@end

@implementation PZLSendMailFlowAction

- (instancetype)init{
    [self doesNotRecognizeSelector:_cmd];
    return self;
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super init];
    
    if(self){
        self.rootViewController = rootViewController;
        self.mailCont = [[MFMailComposeViewController alloc] init];
        self.mailCont.mailComposeDelegate = self;
    }
    return self;
}

- (void)tryStartMailFlowWith:(NSString *)emailAddress subject:(NSString *)subject messageBody:(NSString *)messageBody {
    if([MFMailComposeViewController canSendMail]) {
        [self.mailCont setToRecipients:@[@"chamberton@hotmail.com"]];
        [self.mailCont setSubject:subject];
        [self.mailCont setMessageBody:messageBody isHTML:NO];
        [self.rootViewController presentViewController:_mailCont animated:YES completion:nil];
    }
}

# pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
    [self.rootViewController.view removeFromSuperview];
    [self.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
