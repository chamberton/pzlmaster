//
//  PZLSendFeedBackViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/16.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "PZLSendFeedBackViewController.h"
#import "PZLSendFeedBackViewProtocol.h"
#import "PZLSendFeedBackViewModel.h"
#import "PZLSendMailFlowAction.h"

@interface PZLSendFeedBackViewController ()<PZLSendFeedBackViewProtocol>

@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UITextField *senderAliasTextField;

@property (weak, nonatomic) IBOutlet UITextView *message;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (readwrite) CGFloat viewOffSet;
@property (readwrite,strong) PZLSendMailFlowAction* sendMailFlowAction;
@end

static float kOFFSET_FOR_KEYBOARD = 80.0;

@implementation PZLSendFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sendMailFlowAction = [[PZLSendMailFlowAction alloc] initWithRootViewController:self];
    self.viewOffSet = 0;
    self.mainView.layer.borderWidth = 8;
    [self.mainView.layer setBorderColor:(__bridge CGColorRef)([PZLColorSchemeManager getBorderColor])];
    self.view.center = CGPointMake(self.parentViewController.view.frame.size.width/2, self.view.frame.origin.y + 0.5 *self.view.frame.size.height);
    self.mainView.layer.cornerRadius = 10;
    _message.delegate = self;
    
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        if (screenHeight <= 568) {
            self.subjectTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            self.senderAliasTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            self.message.autocorrectionType = UITextAutocorrectionTypeNo;
        }
    }
    
}


-(void)keyboardWillShow:(NSNotification*) note{
    if([UIScreen mainScreen].bounds.size.height<600 && self.viewOffSet<=0){
        NSDictionary * Dictionnary=[note userInfo];
        
        NSValue* value = (NSValue*)[Dictionnary valueForKey:UIKeyboardFrameEndUserInfoKey ];
        CGRect rawFrame      = [value CGRectValue];
        CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
        
        kOFFSET_FOR_KEYBOARD =  keyboardFrame.size.height - (
                                                             [UIScreen mainScreen].bounds.size.height - self.mainView.frame.size.height - self.mainView.frame.origin.y);
        if (self.viewOffSet<= 0)
        {
            self.viewOffSet = kOFFSET_FOR_KEYBOARD;
            [self setViewMovedUp:YES];
        }
        
        
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentAdd];
    });
}
-(void)keyboardWillHide : (NSNotification*) note{
    if([UIScreen mainScreen].bounds.size.height<600 && self.viewOffSet>0){
        if( self.viewOffSet >0){
            [self setViewMovedUp:NO];
            self.viewOffSet = 0;
        }
    }
}

-(void)setViewMovedUp:(BOOL)movedUp {
    
    if([UIScreen mainScreen].bounds.size.height<600){
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3]; // if you want to slide up the view
        CGFloat offSet;
        if (movedUp)
        {
            offSet = -1*self.viewOffSet;
        }
        else
        {
            offSet = +1*self.viewOffSet;
        }
        self.view.frame = CGRectOffset(self.view.frame, 0, offSet);
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView commitAnimations];
        });
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (IBAction)sendText:(id)sender {
    NSString* subject = self.subjectTextField.text;
    NSString* messageBody = self.message.text;
    [self.sendMailFlowAction tryStartMailFlowWith:@"chamberton@hotmail.com" subject:subject messageBody:messageBody];
}

- (IBAction)discardMessage:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
