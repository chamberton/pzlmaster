//
//  PZLSendMailFlowAction.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/05.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZLSendMailFlowAction : NSObject

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController;
- (void)tryStartMailFlowWith:(NSString *)emailAddress subject:(NSString *)subject messageBody:(NSString *)messageBody;

@end
