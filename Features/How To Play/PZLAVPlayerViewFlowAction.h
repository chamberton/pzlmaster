//
//  PZLAVPlayerViewFlowAction.h
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVKit/AVKit.h>

@interface PZLAVPlayerViewFlowAction : NSObject

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController;
- (void)startShowVideoFlowAction:(NSURL *)movieURL;

@end
