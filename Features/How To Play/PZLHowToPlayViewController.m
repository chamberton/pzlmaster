//
//  PZLHowToPlayViewController.m
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/08.
//  Copyright © 2016 Serge Mbamba. All rights reserved.

#import "PZLHowToPlayViewController.h"
#import "PZLColorSchemeManager.h"
#import "PZLHowToPlayViewModel.h"
#import "PZLHowToPlayViewProtocol.h"
#import "PZLAVPlayerViewFlowAction.h"
#import <AVKit/AVKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
//

@interface PZLHowToPlayViewController()<PZLHowToPlayViewProtocol>

@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITextView *textInstructions;
@property (readwrite,strong) PZLHowToPlayViewModel* viewModel;
@property (readwrite,strong) PZLAVPlayerViewFlowAction* videoPlayerFlowAction;

@end

@implementation PZLHowToPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[PZLHowToPlayViewModel alloc] initWithView:self];
    self.videoPlayerFlowAction =[[PZLAVPlayerViewFlowAction alloc] initWithRootViewController:self];
    self.mainview.layer.borderWidth = 8;
    [self.mainview.layer setBorderColor:(__bridge CGColorRef)([PZLColorSchemeManager getBorderColor])];
    self.view.center = CGPointMake(self.parentViewController.view.frame.size.width/2, self.view.frame.origin.y + 0.75 *self.view.frame.size.height);
    self.mainview.layer.cornerRadius = 10;
    self.textInstructions.text = NSLocalizedString(@"InstructionsText1", @"InstructionsText1");
    self.textInstructions.contentOffset = CGPointZero;
}

- (IBAction)ShowDemoVideo:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentAdd];
    });
    [self.videoPlayerFlowAction startShowVideoFlowAction:[self.viewModel movieFileURL]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.textInstructions.contentOffset = CGPointZero;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentAdd];
    });
}

- (IBAction)okTapped:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)segmentControlValueChanged:(id)sender {
    self.textInstructions.contentOffset = CGPointZero;
    if(self.segmentedControl!=sender){
        return;
    }
    [self presentAdd];
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.textInstructions.text= NSLocalizedString(@"InstructionsText1", @"InstructionsText1");
    } else if(self.segmentedControl.selectedSegmentIndex == 1) {
        self.textInstructions.text=NSLocalizedString(@"InstructionsText2", @"InstructionsText1");
    } else if(self.segmentedControl.selectedSegmentIndex == 2) {
        self.textInstructions.text=NSLocalizedString(@"InstructionsText3", @"InstructionsText1");
    }
    else if(self.segmentedControl.selectedSegmentIndex == 3) {
        self.textInstructions.text=NSLocalizedString(@"InstructionsText4", @"InstructionsText1");
    }
}

@end
