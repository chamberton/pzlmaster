//
//  HowToPlayViewModel.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLHowToPlayViewModel.h"

@interface PZLHowToPlayViewModel()

@property (strong,readwrite) id<PZLHowToPlayViewProtocol>view;

@end

static NSString* const movieFileName = @"OutputMovie.mov";

@implementation PZLHowToPlayViewModel

- (instancetype)initWithView:(id<PZLHowToPlayViewProtocol>)view {
    self = [super init];
    
    if (self){
        self.view = view;
    }
    
    return self;
}

- (NSURL *)movieFileURL{

    NSString* movieFilePath = [[NSBundle mainBundle] pathForResource:movieFileName ofType:nil];
    NSAssert(movieFilePath, @"movieFilePath is nil");
    
    return [NSURL fileURLWithPath:movieFilePath];
}

@end
