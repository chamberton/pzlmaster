//
//  PZLHowToPlayViewController.h
//  Pzl
//
//  Created by Serge Mbamba on 2016/07/08.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PZLViewController.h"

@interface PZLHowToPlayViewController : PZLViewController<AVPlayerViewControllerDelegate>

@end
