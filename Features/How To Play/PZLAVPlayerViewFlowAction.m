//
//  PZLAVPlayerViewFlowAction.m
//  Pzl
//
//  Created by Serge Mbamba on 2017/04/06.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "PZLAVPlayerViewFlowAction.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PZLAVPlayerViewFlowAction()

@property (weak, readwrite) UIViewController *rootViewController;
@property (nonatomic, retain) AVPlayerViewController *avPlayerViewcontroller;

@end

@implementation PZLAVPlayerViewFlowAction

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    
    self = [super init];
    
    if(self){
        self.rootViewController = rootViewController;
    }
    
    return self;
    
}

- (void)playerDidFinishPlaying:(NSNotification *)note {
    
}

- (void)startShowVideoFlowAction:(NSURL *)movieURL {
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.player = [AVPlayer playerWithURL:movieURL];
    self.avPlayerViewcontroller = playerViewController;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidFinishPlaying:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayerViewcontroller.player currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidFinishPlaying:)
                                                 name:AVPlayerItemFailedToPlayToEndTimeNotification
                                               object:[self.avPlayerViewcontroller.player currentItem] ];
    
    [self resizePlayerToViewSize];
    [self.rootViewController presentViewController:self.avPlayerViewcontroller animated:YES completion:nil];
    [self.rootViewController.view addSubview:playerViewController.view];
    self.rootViewController.view.autoresizesSubviews = TRUE;

}

- (void) resizePlayerToViewSize {
    self.avPlayerViewcontroller.view.frame =  self.rootViewController.view.frame;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
